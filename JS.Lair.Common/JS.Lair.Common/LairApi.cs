﻿using JS.Common;
using System.Threading.Tasks;
using req = JS.Lair.Common.Protocol.Request;
using res = JS.Lair.Common.Protocol.Response;

namespace JS.Lair.Common
{
    public class LairApi : WebApi
    {
        public LairApi(string url, Serializer serializer) : base(url, serializer)
        {
        }

        public async Task<res.Join> Join(req.Join req)
        {
            return await SendRequest<req.Join, res.Join>($"{_url}/api/Join", req);
        }

        public async Task<res.Login> Login(req.Login req)
        {
            return await SendRequest<req.Login, res.Login>($"{_url}/api/Login", req);
        }

        public async Task<res.GetAccountKey> GetAccountKey(req.GetAccountKey req)
        {
            return await SendRequest<req.GetAccountKey, res.GetAccountKey>($"{_url}/api/GetAccountKey", req);
        }
    }
}
