﻿namespace JS.Lair.Common.Protocol.Response
{
    public class GetAccountKey : Response
    {
        public long AccountKey;
    }
}
