﻿namespace JS.Lair.Common.Protocol.Response
{
    public enum ResponseCode
    {
        Success,

        UndefinedFailure,

        AlreadyExistID,

        InvalidID,
        InvalidPassword
    }
}
