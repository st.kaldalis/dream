﻿using ud = JS.MyGladiator.Common.UserData;
using gd = JS.MyGladiator.Common.GameData;

namespace JS.MyGladiator.Common.Model.World
{
    public class Unit
    {
        private gd.Unit _gdUnit;

        private ud.Unit _udUnit;

        public void Init(gd.Unit gdUnit, ud.Unit udUnit)
        {
            _gdUnit = gdUnit;

            _udUnit = udUnit;
        }

        public gd.Unit GetUnitData()
        {
            return _gdUnit;
        }

        public ud.Unit GetUnitUserData()
        {
            return _udUnit;
        }
    }
}
