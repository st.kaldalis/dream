﻿namespace JS.MyGladiator.Common.GameData
{
    public class PrefabInfo
    {
        public string ObjectType;

        public string BundleName;

        public string Path;
    }
}
