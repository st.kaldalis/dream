﻿using JS.Common.Csv;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace JS.MyGladiator.Common.GameData
{
    public enum LANGUAGE_CODE
    {
        en,
        ko
    }

    public class GameDataService : IGameDataService
    {
        private Dictionary<string, Text> _texts;

        private Dictionary<string, Status> _status;

        private Dictionary<string, Skeleton> _skeleton;

        private List<Unit> _unit;

        private List<Attachment> _attachment;

        private List<PrefabInfo> _prefabInfo;

        private List<PooledObjectInfo> _pooledObjectInfo;

        public delegate IEnumerator ReadFile(string path, Action<string> callback);

        public event EventHandler OnLoadFinished;

        public GameDataService()
        {
            _texts = new Dictionary<string, Text>();

            _status = new Dictionary<string, Status>();

            _skeleton = new Dictionary<string, Skeleton>();

            _unit = new List<Unit>();

            _attachment = new List<Attachment>();

            _prefabInfo = new List<PrefabInfo>();

            _pooledObjectInfo = new List<PooledObjectInfo>();

            OnLoadFinished += (sender, args) => { };
        }

        private void LoadPrefabInfo(string csv)
        {
            foreach (var line in CsvReader.ReadFromText(csv))
            {
                _prefabInfo.Add(new PrefabInfo
                {
                    ObjectType = line["objectType"],
                    BundleName = line["bundleName"],
                    Path = line["path"]
                });
            }
        }

        private void LoadPooledObjectInfo(string csv)
        {
            foreach (var line in CsvReader.ReadFromText(csv))
            {
                _pooledObjectInfo.Add(new PooledObjectInfo
                {
                    ObjectType = line["objectType"],
                    ExpireTime = Convert.ToDouble(line["expireTime"])
                });
            }
        }

        private void LoadAttachment(string csv)
        {
            foreach (var line in CsvReader.ReadFromText(csv))
            {
                _attachment.Add(new Attachment
                {
                    SkeletonID = line["skeletonID"],
                    Tier = Convert.ToInt32(line["tier"]),
                    AttachmentSetType = line["attachmentSetType"],
                    AttachmentSetID = line["attachmentSetID"],
                    SlotName = line["slotName"],
                    AttachmentName = line["attachmentName"],
                    SpritePath = line["spritePath"]
                });
            }
        }

        private void LoadUnit(string csv)
        {
            foreach (var line in CsvReader.ReadFromText(csv))
            {
                var id = line["id"];
                _unit.Add(new Unit
                {
                    ID = id,
                    Skeleton = _skeleton[line["skeletonID"]],
                    SkinID = line["skinID"],
                    Status = _status[line["statusID"]]
                });
            }
        }

        private void LoadSkeleton(string csv)
        {
            foreach (var line in CsvReader.ReadFromText(csv))
            {
                var id = line["id"];
                _skeleton[id] = new Skeleton
                {
                    ID = id,
                    ObjectType = line["objectType"],
                    UIObjectType = line["uiObjectType"]
                };
            }
        }

        private void LoadStatus(string csv)
        {
            foreach (var line in CsvReader.ReadFromText(csv))
            {
                var id = line["id"];
                _status[id] = new Status
                {
                    ID = id,
                    Strength = Convert.ToInt32(line["strength"]),
                    Dexterity = Convert.ToInt32(line["dexterity"]),
                    Vitality = Convert.ToInt32(line["vitality"]),
                    Luck = Convert.ToInt32(line["luck"])
                };
            }
        }

        private void LoadText(string csv)
        {
            foreach (var line in CsvReader.ReadFromText(csv))
            {
                var id = line["id"];
                _texts[id] = new Text
                {
                    ID = id,
                    Multilingual = new Dictionary<LANGUAGE_CODE, string>()
                    {
                        [LANGUAGE_CODE.en] = line["en"],
                        [LANGUAGE_CODE.ko] = line["ko"]
                    }
                };
            }
        }

        public void Load(string rootPath)
        {
            void ReadFile(string path, Action<string> callback)
            {
                if (!File.Exists(path))
                {
                    return;
                }

                callback(File.ReadAllText(path));
            }

            ReadFile(Path.Combine(rootPath, "Text.csv"), LoadText);

            ReadFile(Path.Combine(rootPath, "Status.csv"), LoadStatus);

            ReadFile(Path.Combine(rootPath, "Skeleton.csv"), LoadSkeleton);

            ReadFile(Path.Combine(rootPath, "Unit.csv"), LoadUnit);

            ReadFile(Path.Combine(rootPath, "Attachment.csv"), LoadAttachment);

            ReadFile(Path.Combine(rootPath, "PrefabInfo.csv"), LoadPrefabInfo);

            ReadFile(Path.Combine(rootPath, "PooledObjectInfo.csv"), LoadPooledObjectInfo);
        }

        public IEnumerator LoadAsync(string rootPath, ReadFile readFile)
        {
            yield return readFile(Path.Combine(rootPath, "Text.csv"), LoadText);

            yield return readFile(Path.Combine(rootPath, "Status.csv"), LoadStatus);

            yield return readFile(Path.Combine(rootPath, "Skeleton.csv"), LoadSkeleton);

            yield return readFile(Path.Combine(rootPath, "Unit.csv"), LoadUnit);

            yield return readFile(Path.Combine(rootPath, "Attachment.csv"), LoadAttachment);

            yield return readFile(Path.Combine(rootPath, "PrefabInfo.csv"), LoadPrefabInfo);

            yield return readFile(Path.Combine(rootPath, "PooledObjectInfo.csv"), LoadPooledObjectInfo);

            OnLoadFinished(this, EventArgs.Empty);
        }

        public bool HasText(string id, LANGUAGE_CODE languageCode)
        {
            if (!_texts.ContainsKey(id))
            {
                return false;
            }

            if (!_texts[id].Multilingual.ContainsKey(languageCode))
            {
                return false;
            }

            return true;
        }

        public string GetText(string id, LANGUAGE_CODE languageCode)
        {
            return _texts[id].Multilingual[languageCode];
        }

        public Attachment GetAttachment(Predicate<Attachment> match)
        {
            return _attachment.Find(match);
        }

        public List<Attachment> GetAllAttachment(Predicate<Attachment> match)
        {
            return _attachment.FindAll(match);
        }

        public Unit GetUnit(Predicate<Unit> match)
        {
            return _unit.Find(match);
        }

        public PrefabInfo GetPrefabInfo(Predicate<PrefabInfo> match)
        {
            return _prefabInfo.Find(match);
        }

        public PooledObjectInfo GetPooledObjectInfo(Predicate<PooledObjectInfo> match)
        {
            return _pooledObjectInfo.Find(match);
        }
    }
}
