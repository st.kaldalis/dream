﻿namespace JS.MyGladiator.Common.GameData
{
    public class Attachment
    {
        public string SkeletonID;

        public int Tier;

        public string AttachmentSetType;

        public string AttachmentSetID;

        public string SlotName;

        public string AttachmentName;

        public string SpritePath;
    }
}
