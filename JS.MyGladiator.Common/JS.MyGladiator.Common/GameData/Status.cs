﻿namespace JS.MyGladiator.Common.GameData
{
    public class Status
    {
        public string ID;

        public int Strength;

        public int Dexterity;

        public int Vitality;

        public int Luck;
    }
}
