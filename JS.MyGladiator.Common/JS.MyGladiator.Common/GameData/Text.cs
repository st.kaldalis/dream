﻿using System.Collections.Generic;

namespace JS.MyGladiator.Common.GameData
{
    public class Text
    {
        public string ID;

        public Dictionary<LANGUAGE_CODE, string> Multilingual;
    }
}
