﻿namespace JS.MyGladiator.Common.GameData
{
    public class Unit
    {
        public string ID;

        public Skeleton Skeleton;

        public string SkinID;

        public Status Status;
    }
}
