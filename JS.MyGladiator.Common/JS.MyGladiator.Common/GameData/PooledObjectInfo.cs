﻿namespace JS.MyGladiator.Common.GameData
{
    public class PooledObjectInfo
    {
        public string ObjectType;

        public double ExpireTime;
    }
}
