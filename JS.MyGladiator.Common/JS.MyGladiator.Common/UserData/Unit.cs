﻿using System.Collections.Generic;

namespace JS.MyGladiator.Common.UserData
{
    public class Unit
    {
        public long Key;

        public string UnitID;

        public string Name;

        public int Level;

        public Status Status;

        public List<Attachment> Attachments;
    }
}
