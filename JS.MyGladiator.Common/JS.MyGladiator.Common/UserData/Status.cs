﻿namespace JS.MyGladiator.Common.UserData
{
    public class Status
    {
        public int Strength;

        public int Dexterity;

        public int Vitality;

        public int Luck;
    }
}