﻿using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    /// <summary>
    /// 유니티의 코루틴이 필요한 MonoBehaviour 가 아닌 오브젝트에서 사용하기 위한 오브젝트
    /// </summary>
    public class CoroutineRunner : MonoBehaviour
    {
        public void Run(IEnumerator routine)
        {
            StartCoroutine(routine);
        }
    }
}