﻿using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class BaseObject
    {
        protected Context _context;

        public virtual void Init(Context context)
        {
            _context = context;
        }

        public virtual void CleanUp()
        {
            _context = default;
        }

        protected void RunCoroutine(IEnumerator routine)
        {
            _context.CoroutineRunner.Run(routine);
        }

        protected static GameObject Instantiate(GameObject original)
        {
            return Object.Instantiate(original);
        }

        protected static void Destroy(GameObject obj)
        {
            Object.Destroy(obj);
        }
    }
}
