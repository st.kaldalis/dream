﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class AssetBundleManager : MonoBehaviour, IAssetBundleManager
    {
        public string AssetBundleRootPath;

        public JS.Common.ILogger Logger;

        private Dictionary<string, AssetBundle> _bundles;

        private HashSet<string> _bundleNamesOnLoading;

        private void Awake()
        {
            _bundles = new Dictionary<string, AssetBundle>();

            _bundleNamesOnLoading = new HashSet<string>();
        }

        public AssetBundle LoadBundle(string bundleName)
        {
            if (_bundles.ContainsKey(bundleName))
            {
                return _bundles[bundleName];
            }

            var bundle = AssetBundle.LoadFromFile(System.IO.Path.Combine($"{Application.streamingAssetsPath}/AssetBundles", bundleName));
            if (bundle == null)
            {
                Logger.Fatal($"Failed to load bundle. Bundle name : {bundleName}");
            }

            _bundles[bundleName] = bundle;

            return bundle;
        }

        public IEnumerator LoadBundleAsync(string bundleName)
        {
            while (_bundleNamesOnLoading.Contains(bundleName))
            {
                yield return null;
            }

            if (_bundles.ContainsKey(bundleName))
            {
                yield break;
            }

            _bundleNamesOnLoading.Add(bundleName);

            var abcr = AssetBundle.LoadFromFileAsync(System.IO.Path.Combine($"{Application.streamingAssetsPath}/AssetBundles", bundleName));
            yield return abcr;

            if (abcr.assetBundle == null)
            {
                Logger.Fatal($"Failed to load bundle. Bundle name : {bundleName}");
            }
            else
            {
                _bundles[bundleName] = abcr.assetBundle;
            }

            _bundleNamesOnLoading.Remove(bundleName);
        }

        public T Load<T>(string bundleName, string path) where T : UnityEngine.Object
        {
            var bundle = LoadBundle(bundleName);

            T asset = bundle.LoadAsset<T>(AssetBundleRootPath + path);
            if (asset == null)
            {
                Logger.Fatal($"Failed to load asset. Bundle name : {bundleName}, Path : {path}");
            }

            return asset;
        }

        public IEnumerator LoadAsync<T>(string bundleName, string path, Action<T> callback) where T : UnityEngine.Object
        {
            yield return LoadBundleAsync(bundleName);

            var bundle = _bundles[bundleName];

            var abr = bundle.LoadAssetAsync<T>(AssetBundleRootPath + path);
            yield return abr;

            T asset = abr.asset as T;
            if (asset == null)
            {
                Logger.Fatal($"Failed to load asset. Bundle name : {bundleName}, Path : {path}");
            }

            callback(asset);
        }
    }
}