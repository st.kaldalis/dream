﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class Logger : JS.Common.ILogger
    {
        public void Fatal(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Debug.LogError($"[FATAL] {msg}\nfilePath : {filePath}\nlineNumber : {lineNumber}");
        }

        public void Info(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Debug.Log($"[INFO] {msg}\nfilePath : {filePath}\nlineNumber : {lineNumber}");
        }

        public void Warn(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Debug.LogWarning($"[WARN] {msg}\nfilePath : {filePath}\nlineNumber : {lineNumber}");
        }
    }
}
