﻿using FlatBuffers;
using JS.Common;
using JS.MyGladiator.ChattingServer.Common.Protocol;
using JS.MyGladiator.Net;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using csProto = JS.MyGladiator.ChattingServer.Common.Protocol;

namespace JS.MyGladiator.Client.ChattingUtility
{
    public class ChattingConsole : BaseObject, IReceiveHandler, IConnectHandler, IDisconnectHandler
    {
        private enum EventType
        {
            ConnectSuccess,
            ConnectFailed,
            LoginSuccess,
            LoginFailed,
            Disconnected,
            MessageReceived
        }

        private class Event
        {
            public EventType Type { get; private set; }

            public EventArgs Args { get; private set; }

            public Event(EventType type, EventArgs args)
            {
                Type = type;

                Args = args;
            }
        }

        private ConcurrentQueue<Event> _eventQueue;

        private readonly int MAX_MESSAGE_COUNT = 15;

        public Queue<Message> MessageList { private set; get; }

        public ChattingConsoleState State { private set; get; }

        private ILogger _logger;

        private IPacketService _packetService;

        private IPool<SocketAsyncEventArgs> _saePool;

        private IPool<Token> _tokenPool;

        private Session _session;

        public event EventHandler<LoginResultReceivedEventArgs> LoginResultReceivedEvent;

        public event EventHandler<MessageReceivedEventArgs> MessageReceivedEvent;

        public event EventHandler DisconnectedEvent;

        public override void Init(Context context)
        {
            base.Init(context);

            State = ChattingConsoleState.Idle;

            _logger = context.Logger;

            _packetService = new PacketService();

            _saePool = new SaePool();

            _tokenPool = new Pool<Token>(new TokenFactory());

            _session = default;

            _eventQueue = new ConcurrentQueue<Event>();

            MessageList = new Queue<Message>();

            LoginResultReceivedEvent = default;
            LoginResultReceivedEvent += (sender, args) => { };

            MessageReceivedEvent = default;
            MessageReceivedEvent += (sender, args) => { };

            DisconnectedEvent = default;
            DisconnectedEvent += (sender, args) => { };
        }

        public void Login(string hostAddress, int port, string serverSessionID, float timeLimit)
        {
            Debug.Assert(State == ChattingConsoleState.Idle);

            State = ChattingConsoleState.PendingLogin;

            RunCoroutine(LoginAsync(hostAddress, port, serverSessionID, timeLimit));
        }

        private IEnumerator LoginAsync(string hostAddress, int port, string serverSessionID, float timeLimit)
        {
            // Connect
            {
                Connect(hostAddress, port);

                Event evt = default;
                yield return ReceiveEventAsync(timeLimit, (usedTime, receivedEvent) =>
                {
                    timeLimit -= usedTime;
                    evt = receivedEvent;
                });

                if (timeLimit <= 0.0f)
                {
                    State = ChattingConsoleState.Idle;
                    LoginResultReceivedEvent(this, new LoginResultReceivedEventArgs(false, LoginFailedReson.Timeout));
                    yield break;
                }

                if (evt == default || evt.Type != EventType.ConnectSuccess)
                {
                    State = ChattingConsoleState.Idle;
                    LoginResultReceivedEvent(this, new LoginResultReceivedEventArgs(false, LoginFailedReson.ConnectFailed));
                    yield break;
                }
            }

            // Login
            {
                Login(serverSessionID);

                Event evt = default;
                yield return ReceiveEventAsync(timeLimit, (usedTime, receivedEvent) =>
                {
                    timeLimit -= usedTime;
                    evt = receivedEvent;
                });

                if (timeLimit <= 0.0f)
                {
                    State = ChattingConsoleState.Idle;
                    LoginResultReceivedEvent(this, new LoginResultReceivedEventArgs(false, LoginFailedReson.Timeout));
                    yield break;
                }

                if (evt == default || evt.Type != EventType.LoginSuccess)
                {
                    State = ChattingConsoleState.Idle;
                    LoginResultReceivedEvent(this, new LoginResultReceivedEventArgs(false, LoginFailedReson.AuthenticationFailed));
                    yield break;
                }
            }

            State = ChattingConsoleState.LoggedIn;
            LoginResultReceivedEvent(this, new LoginResultReceivedEventArgs(true, LoginFailedReson.None));

            RunCoroutine(HandleEventAsync());
        }

        private IEnumerator ReceiveEventAsync(float timeLimit, Action<float, Event> callback)
        {
            var remainTime = timeLimit;

            while (true)
            {
                remainTime -= UnityEngine.Time.deltaTime;
                if (remainTime <= 0.0f)
                {
                    callback(timeLimit, default);
                    yield break;
                }

                if (_eventQueue.Count == 0)
                {
                    yield return null;
                    continue;
                }

                if (!_eventQueue.TryDequeue(out Event evt))
                {
                    yield return null;
                    continue;
                }

                callback(timeLimit - remainTime, evt);
                yield break;
            }
        }

        private IEnumerator HandleEventAsync()
        {
            while (true)
            {
                if (State != ChattingConsoleState.LoggedIn)
                {
                    yield break;
                }

                if (_eventQueue.Count == 0)
                {
                    yield return null;
                    continue;
                }

                if (!_eventQueue.TryDequeue(out Event evt))
                {
                    yield return null;
                    continue;
                }

                switch (evt.Type)
                {
                    case EventType.MessageReceived:
                        {
                            var args = evt.Args as MessageReceivedEventArgs;
                            AddMessage(args.Message);

                            MessageReceivedEvent(this, args);
                        }
                        break;
                    case EventType.Disconnected:
                        State = ChattingConsoleState.Idle;
                        DisconnectedEvent(this, EventArgs.Empty);
                        yield break;
                    default:
                        CloseSession();

                        State = ChattingConsoleState.Idle;
                        DisconnectedEvent(this, EventArgs.Empty);
                        yield break;
                }
            }
        }

        private void Connect(string hostAddress, int port)
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(hostAddress);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

            var socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            _session = new Session(_logger, _packetService, _saePool, _tokenPool, socket, this, this, this);
            _session.BeginConnect(remoteEP);
        }

        private void Login(string serverSessionID)
        {
            var builder = new FlatBufferBuilder(1);
            var sessionID = builder.CreateString(serverSessionID);

            RequestConnection.StartRequestConnection(builder);
            RequestConnection.AddSessionID(builder, sessionID);

            var requestConnection = RequestConnection.EndRequestConnection(builder);

            builder.Finish(requestConnection.Value);

            var protocolType = (int)csProto.Type.REQUEST_CONNECTION;
            var protocol = builder.DataBuffer.ToSizedArray();

            var data = new byte[sizeof(int) + protocol.Length];
            Buffer.BlockCopy(BitConverter.GetBytes(protocolType), 0, data, 0, sizeof(int));
            Buffer.BlockCopy(protocol, 0, data, sizeof(int), protocol.Length);

            _session.Send(data);
        }

        public void SendMessage(string value)
        {
            Debug.Assert(State == ChattingConsoleState.LoggedIn);

            var builder = new FlatBufferBuilder(1);

            var msg = builder.CreateString(value);
            RequestSendMessage.StartRequestSendMessage(builder);
            RequestSendMessage.AddBody(builder, msg);

            var Message = RequestSendMessage.EndRequestSendMessage(builder);

            builder.Finish(Message.Value);

            var protocolType = (int)csProto.Type.SEND_MESSAGE;
            var protocol = builder.DataBuffer.ToSizedArray();

            var data = new byte[sizeof(int) + protocol.Length];
            Buffer.BlockCopy(BitConverter.GetBytes(protocolType), 0, data, 0, sizeof(int));
            Buffer.BlockCopy(protocol, 0, data, sizeof(int), protocol.Length);

            _session.Send(data);
        }

        public void OnConnect(Session session)
        {
            if (session.Socket.Connected)
            {
                session.BeginReceive();
                _eventQueue.Enqueue(new Event(EventType.ConnectSuccess, EventArgs.Empty));
            }
            else
            {
                _eventQueue.Enqueue(new Event(EventType.ConnectFailed, EventArgs.Empty));
            }
        }

        public void OnDisconnect(Session session)
        {
            CloseSession();

            _eventQueue.Enqueue(new Event(EventType.Disconnected, EventArgs.Empty));
        }

        private void CloseSession()
        {
            _session.Close();
            _session = default;
        }

        public void OnReceive(Session session, byte[] data)
        {
            var protocolType = (csProto.Type)BitConverter.ToInt32(data, 0);
            switch (protocolType)
            {
                case csProto.Type.ALLOW_CONNECTION:
                    _eventQueue.Enqueue(new Event(EventType.LoginSuccess, EventArgs.Empty));
                    break;
                case csProto.Type.REJECT_CONNECTION:
                    CloseSession();

                    _eventQueue.Enqueue(new Event(EventType.LoginFailed, EventArgs.Empty));
                    break;
                case csProto.Type.SEND_MESSAGE:
                    {
                        var byteBuffer = new ByteBuffer(data, 4);
                        var proto = RequestSendMessage.GetRootAsRequestSendMessage(byteBuffer);
                        _logger.Info($"{proto.SenderName} : {proto.Body}");

                        _eventQueue.Enqueue(new Event(EventType.MessageReceived, new MessageReceivedEventArgs(new Message
                        {
                            SenderName = proto.SenderName,
                            Body = proto.Body
                        })));
                    }
                    break;
                default:
                    _context.Logger.Fatal($"Invalid protocol type received. Protocol type : {protocolType}");
                    break;
            }
        }

        private void AddMessage(Message value)
        {
            MessageList.Enqueue(value);
            if (MessageList.Count > MAX_MESSAGE_COUNT)
            {
                MessageList.Dequeue();
            }
        }
    }
}
