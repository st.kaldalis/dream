﻿using System;

namespace JS.MyGladiator.Client.ChattingUtility
{
    public class MessageReceivedEventArgs : EventArgs
    {
        public MessageReceivedEventArgs(Message message)
        {
            Message = message;
        }

        public Message Message { protected set; get; }
    }
}
