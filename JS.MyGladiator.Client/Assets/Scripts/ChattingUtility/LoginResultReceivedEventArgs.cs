﻿using System;

namespace JS.MyGladiator.Client.ChattingUtility
{
    public enum LoginFailedReson
    {
        None,
        Timeout,
        ConnectFailed,
        AuthenticationFailed
    }

    public class LoginResultReceivedEventArgs : EventArgs
    {
        public LoginResultReceivedEventArgs(bool isLoginSuccess, LoginFailedReson loginFailedResonCode)
        {
            IsLoginSuccess = isLoginSuccess;

            LoginFailedReson = loginFailedResonCode;
        }

        public bool IsLoginSuccess { private set; get; }

        public LoginFailedReson LoginFailedReson { protected set; get; }
    }
}
