﻿namespace JS.MyGladiator.Client.ChattingUtility
{
    public enum ChattingConsoleState
    {
        Idle,
        PendingLogin,
        LoggedIn
    }
}
