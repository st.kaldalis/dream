﻿using System;

namespace JS.MyGladiator.Client
{
    public class ObservableValue<T>
    {
        public class SetValueEventArgs : EventArgs
        {
            public T Value;
        }

        private T _value;

        public event EventHandler<SetValueEventArgs> SetValueEvent;

        public T Value
        {
            get { return _value; }

            set
            {
                _value = value;

                SetValueEvent(this, new SetValueEventArgs
                {
                    Value = _value
                });
            }
        }

        public ObservableValue()
        {
            CleanUp();
        }

        public void CleanUp()
        {
            _value = default;

            SetValueEvent = default;
            SetValueEvent += (sender, args) => { };
        }
    }
}