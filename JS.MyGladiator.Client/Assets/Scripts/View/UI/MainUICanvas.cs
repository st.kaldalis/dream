﻿using UnityEngine;

namespace JS.MyGladiator.Client.View.UI
{
    public class MainUICanvas : MonoBehaviour
    {
        private Window _window;

        public Window GetWindow()
        {
            return _window;
        }

        public void UnsetWindow()
        {
            _window = default;
        }

        public void SetWindow(Window value)
        {
            _window = value;
            _window.transform.SetParent(transform, false);
            _window.transform.SetAsFirstSibling();
        }
    }
}
