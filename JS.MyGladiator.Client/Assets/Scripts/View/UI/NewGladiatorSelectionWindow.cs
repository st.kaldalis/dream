﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using mw = JS.MyGladiator.Common.Model.World;

namespace JS.MyGladiator.Client.View.UI
{
    public class NewGladiatorSelectionWindow : MainWindow
    {
        public class GladiatorCellClickEventArgs : EventArgs
        {
            public GladiatorCell Cell;
        }

        public class GladiatorNameInputFieldValuChangedEventArgs : EventArgs
        {
            public string Value;
        }

        protected bool _isGladiatorListOnUpdate;

        public mw.Unit Gladiator { protected set; get; }

        public string GladiatorName { protected set; get; }

        public List<mw.Unit> _gladiators;

        public Transform GladiatorList;

        protected List<GladiatorCell> _gladiatorCells;

        public InputField GladiatorNameInputField;

        public Button StartButton;

        public event EventHandler StartButtonClickEvent;

        public event EventHandler BackButtonClickEvent;

        public event EventHandler<GladiatorCellClickEventArgs> GliadatorCellClickEvent;

        public event EventHandler<GladiatorNameInputFieldValuChangedEventArgs> GladiatorNameInputFieldValueChangedEvent;

        public override void CleanUp()
        {
            base.CleanUp();

            GliadatorCellClickEvent = default;
            GliadatorCellClickEvent += (sender, args) => { };

            StartButtonClickEvent = default;
            StartButtonClickEvent += (sender, args) => { };

            BackButtonClickEvent = default;
            BackButtonClickEvent += (sender, args) => { };

            GladiatorNameInputFieldValueChangedEvent = default;
            GladiatorNameInputFieldValueChangedEvent += (sender, args) => { };

            _isGladiatorListOnUpdate = false;

            Gladiator = default;

            GladiatorName = default;

            if (_gladiatorCells != default)
            {
                foreach (var cell in _gladiatorCells) { cell.Terminate(); }
            }
            _gladiatorCells = new List<GladiatorCell>();

            GladiatorNameInputField.gameObject.SetActive(false);
            GladiatorNameInputField.text = string.Empty;
        }

        public void Init(Context context, List<mw.Unit> units)
        {
            base.Init(context);

            SetNewGladiators(units);
        }

        public void SetGladiator(mw.Unit unit)
        {
            if (Gladiator == unit)
            {
                return;
            }

            Gladiator = unit;
            GladiatorNameInputField.gameObject.SetActive(Gladiator != null);
            GladiatorNameInputField.text = string.Empty;

            UpdateStartButtonState();
        }

        public void SetNewGladiators(List<mw.Unit> units)
        {
            _gladiators = units;

            RunCoroutine(UpdateGladiatorListAsync());
        }

        public void SetGladiatorName(string name)
        {
            if (Gladiator == default) { return; }

            GladiatorName = name;

            UpdateStartButtonState();
        }

        protected IEnumerator UpdateGladiatorListAsync()
        {
            var guid = GetGuid();

            while (true)
            {
                if (!_isGladiatorListOnUpdate) { break; }
                if (!GetGuid().Equals(guid)) { yield break; }
                else { yield return null; }
            }
            _isGladiatorListOnUpdate = true;

            foreach (var cell in _gladiatorCells)
            {
                cell.Terminate();
            }
            _gladiatorCells.Clear();

            var newGladiators = _gladiators;
            var newGladiatorCells = new List<GladiatorCell>();
            for (var i = 0; i < newGladiators.Count; i++)
            {
                GladiatorCell cell = default;
                yield return _context.Pool.GetAsync("GladiatorCell", (bo) => { cell = bo as GladiatorCell; });

                if (!GetGuid().Equals(guid))
                {
                    cell.Terminate();
                    yield break;
                }

                cell.transform.SetParent(GladiatorList, false);
                _gladiatorCells.Add(cell);

                cell.Init(_context, newGladiators[i]);
                cell.ClickEvent += OnClickGladiatorCell;
            }

            _isGladiatorListOnUpdate = false;
        }

        private void UpdateStartButtonState()
        {
            StartButton.interactable = Gladiator != default && !string.IsNullOrEmpty(GladiatorName);
        }

        public void OnValuChangedGladiatorNameInputField(string value)
        {
            GladiatorNameInputFieldValueChangedEvent(this, new GladiatorNameInputFieldValuChangedEventArgs { Value = value });
        }

        public void OnClickBackButton()
        {
            BackButtonClickEvent(this, EventArgs.Empty);
        }

        public void OnClickStartButton()
        {
            StartButtonClickEvent(this, EventArgs.Empty);
        }

        private void OnClickGladiatorCell(object sender, EventArgs args)
        {
            GliadatorCellClickEvent(this, new GladiatorCellClickEventArgs { Cell = sender as GladiatorCell });
        }
    }
}
