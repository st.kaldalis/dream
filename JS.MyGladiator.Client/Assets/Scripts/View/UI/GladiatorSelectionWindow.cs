﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using mw = JS.MyGladiator.Common.Model.World;

namespace JS.MyGladiator.Client.View.UI
{
    public class GladiatorSelectionWindow : MainWindow
    {
        public class GladiatorCellClickEventArgs : EventArgs
        {
            public GladiatorCell Cell;
        }

        public mw.Unit MainGladiator { protected set; get; }

        protected bool _isGladiatorListOnUpdate;

        protected List<GladiatorCell> _gladiatorCells;

        public List<mw.Unit> _gladiators;

        public Transform GladiatorList;

        public Button StartButton;

        public event EventHandler CreateNewGladiatorButtonEvent;

        public event EventHandler<GladiatorCellClickEventArgs> GliadatorCellClickEvent;

        public event EventHandler StartButtonClickEvent;

        protected IEnumerator UpdateGladiatorListAsync()
        {
            var guid = GetGuid();

            while (true)
            {
                if (!GetGuid().Equals(guid)) { yield break; }

                if (!_isGladiatorListOnUpdate) { break; }
                else { yield return null; }
            }
            _isGladiatorListOnUpdate = true;

            foreach (var cell in _gladiatorCells)
            {
                cell.Terminate();
            }
            _gladiatorCells.Clear();

            for (var i = 0; i < _gladiators.Count; i++)
            {
                GladiatorCell cell = default;
                yield return _context.Pool.GetAsync("GladiatorCell", (bo) =>
                {
                    cell = bo as GladiatorCell;
                });

                if (!GetGuid().Equals(guid))
                {
                    cell.Terminate();
                    yield break;
                }

                cell.transform.SetParent(GladiatorList, false);
                cell.transform.SetAsFirstSibling();

                _gladiatorCells.Add(cell);

                cell.Init(_context, _gladiators[i]);
                cell.ClickEvent += OnClickGladiatorCell;
            }

            _isGladiatorListOnUpdate = false;
        }

        public override void CleanUp()
        {
            base.CleanUp();

            _isGladiatorListOnUpdate = false;

            MainGladiator = default;

            if (_gladiatorCells != default)
            {
                foreach (var cell in _gladiatorCells) { cell.Terminate(); }
            }
            _gladiatorCells = new List<GladiatorCell>();

            CreateNewGladiatorButtonEvent = default;
            CreateNewGladiatorButtonEvent += (sender, args) => { };

            GliadatorCellClickEvent = default;
            GliadatorCellClickEvent += (sender, args) => { };

            StartButtonClickEvent = default;
            StartButtonClickEvent += (sender, args) => { };
        }

        public void SetMainGladiator(mw.Unit unit)
        {
            if (MainGladiator == unit)
            {
                return;
            }

            MainGladiator = unit;

            UpdateStartButtonState();
        }

        private void UpdateStartButtonState()
        {
            StartButton.interactable = MainGladiator != default;
        }

        public void Init(Context context, List<mw.Unit> units, mw.Unit unit)
        {
            base.Init(context);

            SetGladiators(units);

            SetMainGladiator(unit);

            UpdateStartButtonState();
        }

        public void SetGladiators(List<mw.Unit> units)
        {
            _gladiators = units;

            RunCoroutine(UpdateGladiatorListAsync());
        }

        public void OnClickCreateNewGladiatorButton()
        {
            CreateNewGladiatorButtonEvent(this, EventArgs.Empty);
        }

        private void OnClickGladiatorCell(object sender, EventArgs args)
        {
            GliadatorCellClickEvent(this, new GladiatorCellClickEventArgs { Cell = sender as GladiatorCell });
        }

        public void OnClickStartButton()
        {
            StartButtonClickEvent(this, EventArgs.Empty);
        }
    }
}
