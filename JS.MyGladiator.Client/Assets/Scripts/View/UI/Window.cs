﻿using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client.View.UI
{
    [DisallowMultipleComponent]
    public class Window : BaseMonoBehaviour
    {
        // 제거 처리 할 것
        public virtual IEnumerator InitAsync(Context context)
        {
            Init(context);

            _context = context;

            yield return null;
        }
    }
}
