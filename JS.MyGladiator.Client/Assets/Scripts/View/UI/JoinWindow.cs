﻿using System;
using UnityEngine.UI;

namespace JS.MyGladiator.Client.View.UI
{
    public class JoinWindow : Window
    {
        public string ID { protected set; get; }
        public string Password { protected set; get; }
        public string ConfirmPassword { protected set; get; }
        public string Email { protected set; get; }

        public InputField IDInputField;
        public InputField PasswordInputField;
        public InputField ConfirmPasswordInputField;
        public InputField EmailInputField;

        public event EventHandler CancelButtonClickEvent;
        public event EventHandler SubmitButtonClickEvent;

        public override void CleanUp()
        {
            base.CleanUp();

            ID = string.Empty;
            Password = string.Empty;
            ConfirmPassword = string.Empty;
            Email = string.Empty;

            IDInputField.text = string.Empty;
            PasswordInputField.text = string.Empty;
            ConfirmPasswordInputField.text = string.Empty;
            EmailInputField.text = string.Empty;

            CancelButtonClickEvent = default;
            CancelButtonClickEvent += (sender, args) => { };

            SubmitButtonClickEvent = default;
            SubmitButtonClickEvent += (sender, args) => { };
        }

        public void OnEndEditIDInputField(string value)
        {
            ID = value;
        }

        public void OnEndEditPasswordInputField(string value)
        {
            Password = value;
        }

        public void OnEndEditConfirmPasswordInputField(string value)
        {
            ConfirmPassword = value;
        }

        public void OnEndEditEmailInputField(string value)
        {
            Email = value;
        }

        public void OnClickCancelButton()
        {
            CancelButtonClickEvent(this, EventArgs.Empty);
        }

        public void OnClickSubmitButton()
        {
            SubmitButtonClickEvent(this, EventArgs.Empty);
        }
    }
}
