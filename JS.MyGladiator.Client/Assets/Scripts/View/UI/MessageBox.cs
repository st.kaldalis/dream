﻿using System;
using UnityEngine.UI;

namespace JS.MyGladiator.Client.View.UI
{
    public class MessageBox : ModalWindow
    {
        public Text Message;
         
        public event EventHandler OKButtonClickEvent;

        public override void CleanUp()
        {
            base.CleanUp();

            OKButtonClickEvent = default;
            OKButtonClickEvent += (sender, args) => { };
        }

        public void OnClickOKButtonEvent()
        {
            OKButtonClickEvent(this, EventArgs.Empty);
        }
    }
}
