﻿using JS.MyGladiator.Client.Controller.Event;
using System;
using UnityEngine;

namespace JS.MyGladiator.Client.View.UI
{
    public class Curtain : Window
    {
        public enum State
        {
            Init,
            Dropping,
            Dropped,
            Lifting,
            Lifted
        }

        private State _state;

        public Animator Animator;

        public event EventHandler<DropCurtainFinishedEventArgs> DropCurtainFinishedEvent;

        public event EventHandler<LiftCurtainFinishedEventArgs> LiftCurtainFinishedEvent;

        public State GetState()
        {
            return _state;
        }

        private void OnEnable()
        {
            switch (_state)
            {
                case State.Lifting:
                    _state = State.Lifted;
                    UpdateAnimator();
                    break;
                case State.Dropping:
                    _state = State.Dropped;
                    UpdateAnimator();
                    break;
                default:
                    UpdateAnimator();
                    break;
            }
        }

        public override void Init(Context context)
        {
            base.Init(context);

            _state = State.Init;
            UpdateAnimator();
        }

        public override void CleanUp()
        {
            base.CleanUp();

            _state = State.Init;

            DropCurtainFinishedEvent = default;
            DropCurtainFinishedEvent += (sender, args) => { };

            LiftCurtainFinishedEvent = default;
            LiftCurtainFinishedEvent += (sender, args) => { };
        }

        public void Drop()
        {
            _state = State.Dropping;
            UpdateAnimator();
        }

        public void Lift()
        {
            _state = State.Lifting;
            UpdateAnimator();
        }

        public void OnDropFinishedEvent()
        {
            _state = State.Dropped;
            UpdateAnimator();
            DropCurtainFinishedEvent(this, new DropCurtainFinishedEventArgs { Curtain = this });
        }

        public void OnLiftFinishedEvent()
        {
            _state = State.Lifted;
            UpdateAnimator();
            LiftCurtainFinishedEvent(this, new LiftCurtainFinishedEventArgs { Curtain = this });

            Terminate();
        }

        private void UpdateAnimator()
        {
            foreach (var param in Animator.parameters)
            {
                Animator.ResetTrigger(param.name);
            }

            switch (_state)
            {
                case State.Lifted:
                case State.Init:
                    Animator.SetTrigger("Init");
                    break;
                case State.Dropping:
                    Animator.SetTrigger("Drop");
                    break;
                case State.Dropped:
                    Animator.SetTrigger("Dropped");
                    break;
                case State.Lifting:
                    Animator.SetTrigger("Lift");
                    break;
            }
        }
    }
}
