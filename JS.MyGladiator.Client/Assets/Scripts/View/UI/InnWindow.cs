﻿using Boo.Lang;
using JS.MyGladiator.Client.ChattingUtility;
using JS.MyGladiator.Client.View.UI.ChattingConsoleComponent;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JS.MyGladiator.Client.View.UI
{
    public class InnWindow : MainWindow
    {
        public class MessageInputFieldEndEditEventArgs : EventArgs
        {
            public string Message;
        }

        public int MAX_MESSAGE_ROW_COUNT;

        private Queue<MessageRow> _messageRows;

        public Transform ChattingConsoleContent;

        public InputField MessageInputField;

        public SceneNavigationPanel SceneNavigationPanel;

        public event EventHandler<MessageInputFieldEndEditEventArgs> MessageInputFieldEndEditEvent;

        public override void Init(Context context)
        {
            base.Init(context);

            _messageRows = new Queue<MessageRow>();
        }

        public override void CleanUp()
        {
            base.CleanUp();

            MessageInputFieldEndEditEvent = default;
            MessageInputFieldEndEditEvent += (sender, args) => { };

            if (_messageRows != default)
            {
                foreach (var row in _messageRows) { row.Terminate(); }
                _messageRows = default;
            }
        }

        public void OnEndEditMessageInputField(string value)
        {
            MessageInputField.text = string.Empty;

            MessageInputFieldEndEditEvent(this, new MessageInputFieldEndEditEventArgs { Message = value });
        }

        public void AddMessageRow(Message message)
        {
            StartCoroutine(AddMessageRowAsync(message));
        }

        private IEnumerator AddMessageRowAsync(Message message)
        {
            var guid = GetGuid();

            MessageRow row = default;
            yield return _context.Pool.GetAsync("ChattingConsoleComponent.MessageRow", (bo) =>
            {
                row = bo as MessageRow;
            });

            if (!guid.Equals(GetGuid())) { row.Terminate(); }

            row.transform.SetParent(ChattingConsoleContent, false);
            row.MessageText.text = $"{message.SenderName} : {message.Body}";
            _messageRows.Enqueue(row);

            if (_messageRows.Count > MAX_MESSAGE_ROW_COUNT)
            {
                var oldRow = _messageRows.Dequeue();
                oldRow.Terminate();
            }
        }
    }
}
