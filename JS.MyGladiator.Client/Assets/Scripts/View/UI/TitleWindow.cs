﻿using System;
using UnityEngine.UI;

namespace JS.MyGladiator.Client.View.UI
{
    public class TitleWindow : MainWindow
    {
        public Button GameStartButton;

        public event EventHandler GameStartButtonClickEvent;

        public override void CleanUp()
        {
            base.CleanUp();

            GameStartButtonClickEvent = default;
            GameStartButtonClickEvent += (sender, args) => { };
        }

        public void OnClickGameStartButton()
        {
            GameStartButtonClickEvent(this, EventArgs.Empty);
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
