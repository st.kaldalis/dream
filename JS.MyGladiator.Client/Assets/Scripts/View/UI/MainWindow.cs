﻿using UnityEngine;

namespace JS.MyGladiator.Client.View.UI
{
    [DisallowMultipleComponent]
    public class MainWindow : Window
    {
        public WindowStack WindowStack;

        public ModalWindowStack ModalWindowStack;
    }
}
