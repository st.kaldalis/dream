﻿using System;

namespace JS.MyGladiator.Client.View.UI
{
    public class LogoWindow : MainWindow
    {
        public event EventHandler StartEvent;

        public void Start()
        {
            StartEvent(this, EventArgs.Empty);
        }

        public override void CleanUp()
        {
            base.CleanUp();

            StartEvent = default;
            StartEvent += (sender, args) => { };
        }
    }
}