﻿using System;
using System.Collections;
using UnityEngine.UI;

namespace JS.MyGladiator.Client.View.UI
{
    public class LoginWindow : Window
    {
        public string ID { protected set; get; }
        public string Password { protected set; get; }

        public InputField IDInputField;
        public InputField PasswordInputField;

        public event EventHandler JoinButtonClickEvent;
        public event EventHandler LoginButtonClickEvent;

        public override void CleanUp()
        {
            base.CleanUp();

            ID = string.Empty;
            Password = string.Empty;

            IDInputField.text = string.Empty;
            PasswordInputField.text = string.Empty;

            JoinButtonClickEvent = default;
            JoinButtonClickEvent += (sender, args) => { };

            LoginButtonClickEvent = default;
            LoginButtonClickEvent += (sender, args) => { };
        }

        public void OnEndEditIDInputField(string value)
        {
            ID = value;
        }

        public void OnEndEditPasswordInputField(string value)
        {
            Password = value;
        }

        public void OnClickJoinButton()
        {
            JoinButtonClickEvent(this, EventArgs.Empty);
        }

        public void OnClickLoginButton()
        {
            LoginButtonClickEvent(this, EventArgs.Empty);
        }
    }
}
