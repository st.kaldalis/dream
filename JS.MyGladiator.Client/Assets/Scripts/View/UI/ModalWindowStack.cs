﻿using System;
using System.Linq;
using UnityEngine;

namespace JS.MyGladiator.Client.View.UI
{
    public class ModalWindowStack : WindowStack
    {
        public GameObject ModalWindowBackground;

        public override void Add(Window window)
        {
            base.Add(window);

            UpdateModalWindowBackground();
        }

        public override Window Remove(Guid guid)
        {
            var window = base.Remove(guid);
            if (window == null)
            {
                return null;
            }

            UpdateModalWindowBackground();

            return window;
        }

        private void UpdateModalWindowBackground()
        {
            Debug.Assert(ModalWindowBackground != null, "Modal window background is null. Make sure that modal window stack initialized.");

            if (_stack.Count > 0)
            {
                ModalWindowBackground.SetActive(true);
                ModalWindowBackground.transform.SetAsLastSibling();

                var topWindow = _stack.LastOrDefault();
                topWindow.transform.SetAsLastSibling();
            }
            else
            {
                ModalWindowBackground.SetActive(false);
            }
        }
    }
}
