﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace JS.MyGladiator.Client.View.UI
{
    public class WindowStack : MonoBehaviour
    {
        protected List<Window> _stack;

        private void Awake()
        {
            _stack = new List<Window>();
        }

        public int Count()
        {
            return _stack.Count;
        }

        public virtual void Add(Window window)
        {
            window.CleanUpEvent += OnWindowCleanUpEvent;

            window.gameObject.transform.SetParent(transform, false);
            window.gameObject.transform.SetAsLastSibling();

            _stack.Add(window);
        }

        protected void OnWindowCleanUpEvent(object sender, EventArgs args)
        {
            var window = sender as Window;

            Remove(window.GetGuid());
        }

        public virtual Window Remove(Guid guid)
        {
            var index = _stack.FindIndex((item) => { return item.GetGuid() == guid; });
            if (index < 0)
            {
                return null;
            }

            var window = _stack[index];
            window.CleanUpEvent -= OnWindowCleanUpEvent;

            _stack.RemoveAt(index);

            return window;
        }

        public Window Find(Predicate<Window> match)
        {
            return _stack.Find(match);
        }

        public bool Exists(Predicate<Window> match)
        {
            return _stack.Exists(match);
        }
    }
}
