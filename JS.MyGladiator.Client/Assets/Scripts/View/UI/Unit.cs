﻿using Spine;
using Spine.Unity;
using Spine.Unity.AttachmentTools;
using System.Collections;
using UnityEngine;
using mw = JS.MyGladiator.Common.Model.World;

namespace JS.MyGladiator.Client.View.UI
{
    public class Unit : BaseMonoBehaviour
    {
        public SkeletonGraphic SkeletonGraphic;

        public Material SourceMaterial; // This will be used as the basis for shader and material property settings.

        private Material _runtimeMaterial;

        private Texture2D _runtimeAtlas;

        private mw.Unit _mwUnit;

        public void Init(Context context, mw.Unit mwUnit)
        {
            base.Init(context);

            _mwUnit = mwUnit;

            Apply();
        }

        protected void Apply()
        {
            var skeleton = SkeletonGraphic.Skeleton;

            // STEP 0: PREPARE SKINS
            // Let's prepare a new skin to be our custom skin with equips/customizations. We get a clone so our original skins are unaffected.
            //customSkin = customSkin ?? new Skin("custom skin"); // This requires that all customizations are done with skin placeholders defined in Spine.
            //                                                    //customSkin = customSkin ?? skeleton.UnshareSkin(true, false, skeletonAnimation.AnimationState); 
            //                                                    // use this if you are not customizing on the default skin.

            //TODO 주석 풀고 밑에꺼 쓰게 해줘야 함 그리고 코드 정리 해줘야 됨
            //var templateSkin = skeleton.Data.FindSkin(_mwUnit.GetUnitData().SkinID);
            var templateSkin = skeleton.Data.FindSkin("base");

            // STEP 1: "EQUIP" ITEMS USING SPRITES
            // STEP 1.1 Find the original/template attachment.
            // Step 1.2 Get a clone of the original/template attachment.
            // Step 1.3 Apply the Sprite image to the clone.
            // Step 1.4 Add the remapped clone to the new custom skin.

            //// Let's do this for the visor.
            //int visorSlotIndex = skeleton.FindSlotIndex(visorSlot); // You can access GetAttachment and SetAttachment via string, but caching the slotIndex is faster.
            //Attachment templateAttachment = templateSkin.GetAttachment(visorSlotIndex, visorKey);  // STEP 1.1
            //Attachment newAttachment = templateAttachment.GetRemappedClone(visorSprite, sourceMaterial); // STEP 1.2 - 1.3
            //customSkin.SetAttachment(visorSlotIndex, visorKey, newAttachment); // STEP 1.4

            //// And now for the gun.
            //int gunSlotIndex = skeleton.FindSlotIndex(gunSlot);
            //Attachment templateGun = templateSkin.GetAttachment(gunSlotIndex, gunKey); // STEP 1.1
            //Attachment newGun = templateGun.GetRemappedClone(gunSprite, sourceMaterial); // STEP 1.2 - 1.3
            //if (newGun != null) customSkin.SetAttachment(gunSlotIndex, gunKey, newGun); // STEP 1.4

            // customSkin.RemoveAttachment(gunSlotIndex, gunKey); // To remove an item.
            // customSkin.Clear()
            // Use skin.Clear() To remove all customizations.
            // Customizations will fall back to the value in the default skin if it was defined there.
            // To prevent fallback from happening, make sure the key is not defined in the default skin.

            // STEP 3: APPLY AND CLEAN UP.
            // Recommended, preferably at level-load-time: REPACK THE CUSTOM SKIN TO MINIMIZE DRAW CALLS
            // 				IMPORTANT NOTE: the GetRepackedSkin() operation is expensive - if multiple characters
            // 				need to call it every few seconds the overhead will outweigh the draw call benefits.
            //
            // 				Repacking requires that you set all source textures/sprites/atlases to be Read/Write enabled in the inspector.
            // 				Combine all the attachment sources into one skin. Usually this means the default skin and the custom skin.
            // 				call Skin.GetRepackedSkin to get a cloned skin with cloned attachments that all use one texture.

            var skinName = _mwUnit.GetUnitData().ID;
            var skeletonID = _mwUnit.GetUnitData().Skeleton.ID;

            var customSkin = new Skin(skinName);

            customSkin.AddAttachments(skeleton.Data.DefaultSkin); // Include the "default" skin. (everything outside of skin placeholders)

            var sUnit = _mwUnit.GetUnitUserData();
            if (sUnit.Attachments != null)
            {
                foreach (var attachment in sUnit.Attachments)
                {
                    var gdAttachments = _context.GameDataService.GetAllAttachment((item) =>
                    {
                        return item.SkeletonID == skeletonID && item.AttachmentSetType == attachment.Type && item.AttachmentSetID == attachment.ID;
                    });
                    foreach (var gdAttachment in gdAttachments)
                    {
                        int slotIndex = skeleton.FindSlotIndex(gdAttachment.SlotName); // You can access GetAttachment and SetAttachment via string, but caching the slotIndex is faster.
                        Attachment templateAttachment = templateSkin.GetAttachment(slotIndex, gdAttachment.AttachmentName);  // STEP 1.1
                        var sprite = _context.AssetBundleManager.Load<Sprite>("view", gdAttachment.SpritePath);
                        Attachment newAttachment = templateAttachment.GetRemappedClone(sprite, SourceMaterial); // STEP 1.2 - 1.3
                        customSkin.SetAttachment(slotIndex, gdAttachment.AttachmentName, newAttachment); // STEP 1.4
                    }
                }
            }

            // TODO 착용 중인 장비의 어태치먼트도 넣어 주어야 함 당장은 실행하지 않는다.

            //repackedSkin.AddAttachments(customSkin); // Include your new custom skin.

            customSkin = customSkin.GetRepackedSkin(skinName, SourceMaterial, out _runtimeMaterial, out _runtimeAtlas); // Pack all the items in the skin.
            skeleton.SetSkin(customSkin); // Assign the repacked skin to your Skeleton.

            //if (bbFollower != null) bbFollower.Initialize(true);

            skeleton.SetSlotsToSetupPose(); // Use the pose from setup pose.
            SkeletonGraphic.Update(0); // Use the pose in the currently active animation.
            SkeletonGraphic.OverrideTexture = _runtimeAtlas;

            // WARNING 초기화가 끝나고 나면 반드시 수행 해주어야 함
            AtlasUtilities.ClearCache(); // http://ko.esotericsoftware.com/forum/Attachment-GetRemappedClone-Leak-10183
            Resources.UnloadUnusedAssets();
        }

        public override void CleanUp()
        {
            base.CleanUp();

            if (_runtimeMaterial != default)
            {
                Destroy(_runtimeMaterial);
            }

            if (_runtimeAtlas != default)
            {
                Destroy(_runtimeAtlas);
            }
        }
    }
}
