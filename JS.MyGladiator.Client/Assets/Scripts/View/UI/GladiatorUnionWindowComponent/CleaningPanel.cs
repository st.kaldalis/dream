﻿using System;
using UnityEngine;

namespace JS.MyGladiator.Client.View.UI.GladiatorUnionWindowComponent
{
    public class CleaningPanel : MonoBehaviour
    {
        public event EventHandler GoButtonClickEvent;

        public void OnClickGoButton()
        {
            GoButtonClickEvent(this, EventArgs.Empty);
        }
    }
}
