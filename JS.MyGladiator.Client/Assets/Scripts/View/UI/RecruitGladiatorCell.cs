﻿using System;

namespace JS.MyGladiator.Client.View.UI
{
    public class RecruitGladiatorCell : BaseMonoBehaviour
    {
        public event EventHandler ClickEvent;

        public override void CleanUp()
        {
            base.CleanUp();

            ClickEvent = default;
            ClickEvent += (sender, args) => { };
        }

        public void OnClickEvent()
        {
            ClickEvent(this, EventArgs.Empty);
        }
    }
}
