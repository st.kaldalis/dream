﻿using System;
using UnityEngine;

namespace JS.MyGladiator.Client.View.UI
{
    public class SceneNavigationPanel : MonoBehaviour
    {
        public event EventHandler InnButtonClickEvent;

        public event EventHandler GladiatorUnionButtonClickEvent;

        public event EventHandler BlacksmithButtonClickEvent;

        public event EventHandler MagicianHouseButtonClickEvent;

        public event EventHandler AlchemistHouseButtonClickEvent;

        public event EventHandler WeirdStoreButtonClickEvent;

        public void OnClickInnButton()
        {
            InnButtonClickEvent(this, EventArgs.Empty);
        }

        public void OnClickGladiatrUnionButton()
        {
            GladiatorUnionButtonClickEvent(this, EventArgs.Empty);
        }

        public void OnClickBlacksmithButton()
        {
            BlacksmithButtonClickEvent(this, EventArgs.Empty);
        }

        public void OnClickMagicianHouseButton()
        {
            MagicianHouseButtonClickEvent(this, EventArgs.Empty);
        }

        public void OnClickAlchemistHouseButton()
        {
            AlchemistHouseButtonClickEvent(this, EventArgs.Empty);
        }

        public void OnClickWeirdStoreButton()
        {
            WeirdStoreButtonClickEvent(this, EventArgs.Empty);
        }
    }
}
