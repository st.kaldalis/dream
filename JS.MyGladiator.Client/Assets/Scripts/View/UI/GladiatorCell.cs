﻿using System;
using System.Collections;
using UnityEngine;
using mw = JS.MyGladiator.Common.Model.World;

namespace JS.MyGladiator.Client.View.UI
{
    public class GladiatorCell : BaseMonoBehaviour
    {
        private mw.Unit _mwUnit;

        private Unit _unit;

        private bool _isUnitPanelOnUpdate;

        public GameObject UnitPanel;

        public event EventHandler ClickEvent;

        public void Init(Context context, mw.Unit mwUnit)
        {
            base.Init(context);

            _mwUnit = mwUnit;

            RunCoroutine(UpdateUnitPanelAsync());
        }

        protected IEnumerator UpdateUnitPanelAsync()
        {
            var guid = GetGuid();

            while (true)
            {
                if (!GetGuid().Equals(guid)) { yield break; }
                if (!_isUnitPanelOnUpdate) { break; }
                else { yield return null; }
            }
            _isUnitPanelOnUpdate = true;

            Unit unit = default;
            yield return _context.Pool.GetAsync(_mwUnit.GetUnitData().Skeleton.UIObjectType, (bo) =>
            {
                unit = bo as Unit;
            });

            if (!GetGuid().Equals(guid))
            {
                unit.Terminate();
                yield break;
            }

            unit.transform.SetParent(UnitPanel.transform, false);
            _unit = unit;
            unit.Init(_context, _mwUnit);

            _isUnitPanelOnUpdate = false;
        }

        public override void CleanUp()
        {
            base.CleanUp();

            _isUnitPanelOnUpdate = false;

            _mwUnit = default;

            if (_unit != default)
            {
                _unit.Terminate();
            }
            _unit = default;

            ClickEvent = default;
            ClickEvent += (sender, args) => { };
        }

        public mw.Unit GetUnitModel()
        {
            return _mwUnit;
        }

        public void OnClickEvent()
        {
            ClickEvent.Invoke(this, EventArgs.Empty);
        }
    }
}
