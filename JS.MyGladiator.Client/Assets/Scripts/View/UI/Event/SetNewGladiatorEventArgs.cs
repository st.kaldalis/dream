﻿using System;
using mw = JS.MyGladiator.Common.Model.World;

namespace JS.MyGladiator.Client.View.UI.Event
{
    public class SetNewGladiatorEventArgs : EventArgs
    {
        public mw.Unit Unit;
    }
}
