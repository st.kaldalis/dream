﻿using System;
using mw = JS.MyGladiator.Common.Model.World;

namespace JS.MyGladiator.Client.View.UI.Event
{
    public class SetNewMainGladiatorEventArgs : EventArgs
    {
        public mw.Unit Unit;
    }
}
