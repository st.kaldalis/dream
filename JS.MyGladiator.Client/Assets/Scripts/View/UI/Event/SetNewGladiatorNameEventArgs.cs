﻿using System;

namespace JS.MyGladiator.Client.View.UI.Event
{
    public class SetNewGladiatorNameEventArgs : EventArgs
    {
        public string Name;
    }
}
