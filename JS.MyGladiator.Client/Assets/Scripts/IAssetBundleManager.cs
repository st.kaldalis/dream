﻿using System;
using System.Collections;

namespace JS.MyGladiator.Client
{
    public interface IAssetBundleManager
    {
        T Load<T>(string bundleName, string path) where T : UnityEngine.Object;
        IEnumerator LoadAsync<T>(string bundleName, string path, Action<T> callback) where T : UnityEngine.Object;
    }
}