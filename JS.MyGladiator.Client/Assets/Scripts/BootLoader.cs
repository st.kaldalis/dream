﻿using JS.MyGladiator.Client.ChattingUtility;
using JS.Lair.Common;
using JS.MyGladiator.Client.GameDataService;
using JS.MyGladiator.Client.Model.Lair;
using JS.MyGladiator.Client.Model.Server;
using JS.MyGladiator.Client.Model.World;
using JS.MyGladiator.Client.View.UI;
using JS.MyGladiator.Net;
using JS.MyGladiator.Server.Common;
using Model.ChattingServer;
using JS.MyGladiator.Client.View;

namespace JS.MyGladiator.Client
{
    public class BootLoader : BaseMonoBehaviour
    {
        public AssetBundleManager AssetBundleManager;

        public DummyAssetBundleManager DummyAssetBundleManager;

        public Pool Pool;

        public World World;

        public MainUICanvas MainUICanvas;

        public CurtainRail CurtainRail;

        public CoroutineRunner CoroutineRunner;

        public void Start()
        {
            var logger = new Logger();

#if UNITY_EDITOR
            var abm = DummyAssetBundleManager;
            Destroy(AssetBundleManager.gameObject);
#else
            var abm = AssetBundleManager;
            Destroy(DummyAssetBundleManager.gameObject);
#endif
            abm.Logger = logger;

            var frontDesk = new FrontDesk();

            var fileService = new FileService();

            var serializeService = new SerializeService();

            var gameDataService = new Common.GameData.GameDataService();

            var textService = new TextService(gameDataService)
            {
                Logger = logger
            };

            var lairApi = new LairApi("http://localhost:61120", new JS.Common.Serializer());
            var serverApi = new ServerApi("http://localhost:61438", new JS.Common.Serializer());

            var lairModel = new LairModel
            {
                Session = new Model.Lair.Session(),
                UserInfo = new UserInfo()
            };

            var serverModel = new ServerModel
            {
                Session = new Model.Server.Session(),
                WritePermissionCode = new WritePermissionCode()
            };

            var chattingServerModel = new ChattingServerModel
            {
                PacketService = new PacketService(),
                SaePool = new SaePool(),
                TokenPool = new Pool<Token>(new TokenFactory()),
                Session = default
            };

            var worldModel = new WorldModel();

            var chattingConsole = new ChattingConsole();

            var screen = new Screen();

            var context = new Context
            {
                AssetBundleManager = abm,
                World = World,
                MainUICanvas = MainUICanvas,
                CurtainRail = CurtainRail,
                Logger = logger,
                Screen = screen,
                FrontDesk = frontDesk,
                FileService = fileService,
                SerializeService = serializeService,
                GameDataService = gameDataService,
                TextService = textService,
                Pool = Pool,
                LairApi = lairApi,
                ServerApi = serverApi,
                LairModel = lairModel,
                WorldModel = worldModel,
                ServerModel = serverModel,
                ChattingServerModel = chattingServerModel,
                CoroutineRunner = CoroutineRunner,
                ChattingConsole = chattingConsole
            };

            context.Pool.Init(context);
            context.WorldModel.Init(context);
            context.Screen.Init(context);
            context.CurtainRail.Init(context);

            chattingConsole.Init(context);
            frontDesk.Init(context);

            context.Screen.LoadLogoScene();

            Destroy(gameObject);
        }
    }
}
