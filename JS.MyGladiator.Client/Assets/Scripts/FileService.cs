﻿using System;
using System.Collections;
using System.IO;

namespace JS.MyGladiator.Client
{
    public class FileService
    {
        public IEnumerator ReadFile(string path, Action<string> callback)
        {
            string data;
            if (path.Contains("://") || path.Contains(":///"))
            {
                UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get(path);
                yield return www.SendWebRequest();
                data = www.downloadHandler.text;
            }
            else
            {
                data = File.ReadAllText(path);
            }

            callback(data);
        }
    }
}
