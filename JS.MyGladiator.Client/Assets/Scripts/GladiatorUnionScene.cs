﻿using JS.MyGladiator.Client.Controller.UI;
using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class GladiatorUnionScene : Scene
    {
        public GladiatorUnionScene() : base()
        {
        }

        public override void Load()
        {
            RunCoroutine(LoadAsync());
        }

        private IEnumerator LoadAsync()
        {
            Debug.Assert(MainWindow == default);

            Debug.Assert(_context.MainUICanvas.GetWindow() == default);

            GladiatorUnionWindow win = default;
            yield return _context.Pool.GetAsync("GladiatorUnionWindow", (bo) =>
            {
                MainWindow = win = bo as GladiatorUnionWindow;
            });

            var winCtrl = new GladiatorUnionWindowController();

            win.Init(_context);

            winCtrl.Init(_context, win);

            MainWindow = win;

            _context.MainUICanvas.SetWindow(win);

            TriggerLoadFinishedEvent(this, EventArgs.Empty);
        }

        public override void Unload()
        {
            Debug.Assert(MainWindow != default);

            Debug.Assert(_context.MainUICanvas.GetWindow() != default);

            Debug.Assert(_context.MainUICanvas.GetWindow().GetGuid() == MainWindow.GetGuid());

            MainWindow.Terminate();
            MainWindow = default;

            _context.MainUICanvas.UnsetWindow();
        }
    }
}
