﻿using System;

namespace JS.MyGladiator.Client.Controller.Event
{
    public class SetMainStageEventArgs : EventArgs
    {
        public string StageID;
    }
}
