﻿using System;
using JS.MyGladiator.Client.View.UI;

namespace JS.MyGladiator.Client.Controller.Event
{
    public class LiftCurtainFinishedEventArgs : EventArgs
    {
        public Curtain Curtain;
    }
}