﻿using System;

namespace JS.MyGladiator.Client.Controller.Event
{
    public class DropCurtainEventArgs : EventArgs
    {
        public string CurtainType;

        public EventHandler<DropCurtainFinishedEventArgs> DropCurtainFinishedEventHandler;
    }
}
