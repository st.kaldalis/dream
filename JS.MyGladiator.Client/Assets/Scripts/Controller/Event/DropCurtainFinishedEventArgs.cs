﻿using System;
using JS.MyGladiator.Client.View.UI;

namespace JS.MyGladiator.Client.Controller.Event
{
    public class DropCurtainFinishedEventArgs : EventArgs
    {
        public Curtain Curtain;
    }
}
