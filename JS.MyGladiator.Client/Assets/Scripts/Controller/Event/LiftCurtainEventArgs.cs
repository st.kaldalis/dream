﻿using System;

namespace JS.MyGladiator.Client.Controller.Event
{
    public class LiftCurtainEventArgs : EventArgs
    {
        public Guid CurtainGuid;
    }
}
