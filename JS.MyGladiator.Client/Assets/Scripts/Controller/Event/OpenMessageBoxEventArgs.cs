﻿using System;

namespace JS.MyGladiator.Client.Controller.Event
{
    public class OpenMessageBoxEventArgs : EventArgs
    {
        public string Message;

        public EventHandler OkButtonClickEventHandler;

        public OpenMessageBoxEventArgs()
        {
            Message = string.Empty;

            OkButtonClickEventHandler = (sender, args) => { };
        }
    }
}
