﻿using JS.MyGladiator.Client.View.UI;
using static JS.MyGladiator.Client.View.UI.InnWindow;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class InnWindowController : WindowController<InnWindow>
    {
        private SceneNavigationPanelController _sceneNavigationPanelController;

        private ChattingUtility.ChattingConsole _chattingConsole;

        public override void Init(Context context, InnWindow window)
        {
            base.Init(context, window);

            _window.MessageInputFieldEndEditEvent += OnMessageInputFieldEndEditEvent;

            _chattingConsole = _context.ChattingConsole;
            _chattingConsole.MessageReceivedEvent += OnMessageReceivedEvent;

            _sceneNavigationPanelController = new SceneNavigationPanelController();
            _sceneNavigationPanelController.Init(context, window.SceneNavigationPanel);
        }

        public void OnMessageInputFieldEndEditEvent(object sender, MessageInputFieldEndEditEventArgs args)
        {
            if (_chattingConsole.State != ChattingUtility.ChattingConsoleState.LoggedIn)
            {
                return;
            }

            _chattingConsole.SendMessage(args.Message);
        }

        public void OnMessageReceivedEvent(object sender, ChattingUtility.MessageReceivedEventArgs args)
        {
            _window.AddMessageRow(args.Message);
        }

        public override void CleanUp()
        {
            base.CleanUp();

            if (_chattingConsole != default)
            {
                _chattingConsole.MessageReceivedEvent -= OnMessageReceivedEvent;
            }

            if (_sceneNavigationPanelController != default)
            {
                _sceneNavigationPanelController.CleanUp();
                _sceneNavigationPanelController = default;
            }
        }
    }
}
