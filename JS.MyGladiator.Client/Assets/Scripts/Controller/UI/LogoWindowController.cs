﻿using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class LogoWindowController : WindowController<LogoWindow>
    {
        public override void Init(Context context, LogoWindow window)
        {
            base.Init(context, window);

            window.StartEvent += OnStartEvent;
        }

        private void OnStartEvent(object sender, EventArgs e)
        {
            RunCoroutine(StartAsync());
        }

        private IEnumerator StartAsync()
        {
            // TODO 리소스 업데이트

            yield return _context.GameDataService.LoadAsync($"{Application.streamingAssetsPath}/GameData", _context.FileService.ReadFile);

            _context.Screen.LoadTitleScene();
        }
    }
}
