﻿using JS.MyGladiator.Client.View.UI.GladiatorUnionWindowComponent;
using System;

namespace JS.MyGladiator.Client.Controller.UI.GladiatorUnionWindowComponent
{
    public class CleaningPanelController : BaseObject
    {
        private CleaningPanel _cleaningPanel;

        public void Init(Context context, CleaningPanel panel)
        {
            base.Init(context);

            _cleaningPanel = panel;
            _cleaningPanel.GoButtonClickEvent += OnGoButtonClickEvent;
        }

        private void OnGoButtonClickEvent(object sender, EventArgs e)
        {
            _context.Screen.LoadDungeonScene();
        }

        public override void CleanUp()
        {
            base.CleanUp();

            if (_cleaningPanel != default)
            {
                _cleaningPanel.GoButtonClickEvent -= OnGoButtonClickEvent;
            }
        }
    }
}
