﻿using JS.MyGladiator.Client.Controller.UI.GladiatorUnionWindowComponent;
using JS.MyGladiator.Client.View.UI;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class GladiatorUnionWindowController : WindowController<GladiatorUnionWindow>
    {
        private SceneNavigationPanelController _sceneNavigationPanelController;

        private CleaningPanelController _cleaningPanelController;

        public override void Init(Context context, GladiatorUnionWindow window)
        {
            base.Init(context, window);

            _sceneNavigationPanelController = new SceneNavigationPanelController();
            _sceneNavigationPanelController.Init(context, window.SceneNavigationPanel);

            _cleaningPanelController = new CleaningPanelController();
            _cleaningPanelController.Init(context, window.CleaningPanel);
        }

        public override void CleanUp()
        {
            base.CleanUp();

            if (_sceneNavigationPanelController != default)
            {
                _sceneNavigationPanelController.CleanUp();
                _sceneNavigationPanelController = default;
            }

            if (_cleaningPanelController != default)
            {
                _cleaningPanelController.CleanUp();
                _cleaningPanelController = default;
            }
        }
    }
}
