﻿using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using UnityEngine;
using lp = JS.Lair.Common.Protocol;
using sp = JS.MyGladiator.Server.Common.Protocol;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class LoginWindowController : WindowController<LoginWindow>
    {
        public event EventHandler LoginSuccessEvent;

        public event EventHandler OpenJoinWindowEvent;

        public override void Init(Context context, LoginWindow window)
        {
            base.Init(context, window);

            _window.LoginButtonClickEvent += OnClickLoginButtonEvent;
            _window.JoinButtonClickEvent += OnClickJoinButtonEvent;
        }

        public override void CleanUp()
        {
            base.CleanUp();

            LoginSuccessEvent = default;
            LoginSuccessEvent += (sender, args) => { };

            OpenJoinWindowEvent = default;
            OpenJoinWindowEvent += (sender, args) => { };
        }

        protected void OnClickJoinButtonEvent(object sender, EventArgs args)
        {
            OpenJoinWindowEvent(this, EventArgs.Empty);
        }

        protected void OnClickLoginButtonEvent(object sender, EventArgs args)
        {
            var window = sender as LoginWindow;

            var id = window.ID;
            var pw = window.Password;

            if (string.IsNullOrEmpty(id))
            {
                OpenMessageBox((box) =>
                {
                    box.Init(_context);
                    box.Message.text = _context.TextService.GetText("INPUT_ID");
                });
                return;
            }

            if (string.IsNullOrEmpty(pw))
            {
                OpenMessageBox((box) =>
                {
                    box.Init(_context);
                    box.Message.text = _context.TextService.GetText("INPUT_PASSWORD");
                });
                return;
            }

            RunCoroutine(LoginAsync(id, pw));
        }

        private IEnumerator LoginAsync(string id, string pw)
        {
            Curtain curtain = default;
            _context.CurtainRail.DropCurtain("PendingWebResponseCurtain", (ret) => { curtain = ret; });
            yield return new WaitUntil(() => curtain != default && curtain.GetState() == Curtain.State.Dropped);

            lp.Response.Login resLairLogin = default;
            yield return LairLoginAsync(id, pw, (ret) => { resLairLogin = ret; });

            switch (resLairLogin.ResponseCode)
            {
                case lp.Response.ResponseCode.Success:
                    {
                        var sessionID = resLairLogin.SessionID;
                        _context.LairModel.Session = new Model.Lair.Session
                        {
                            ID = sessionID
                        };

                        yield return ServerLoginAsync(sessionID);
                    }
                    break;
                case lp.Response.ResponseCode.InvalidID:
                    OpenMessageBox((box) =>
                    {
                        box.Init(_context);
                        box.Message.text = _context.TextService.GetText("INVALID_ID_MESSAGE");
                    });
                    break;
                case lp.Response.ResponseCode.InvalidPassword:
                    OpenMessageBox((box) =>
                    {
                        box.Init(_context);
                        box.Message.text = _context.TextService.GetText("INVALID_PASSWORD_MESSAGE");
                    });
                    break;
                default:
                    OpenMessageBox((box) =>
                    {
                        box.Init(_context);
                        box.Message.text = _context.TextService.GetText("UNKNOWN_NETWORK_ERROR");
                    });
                    break;
            }

            curtain.Lift();
        }

        private IEnumerator ServerLoginAsync(string sessionID)
        {
            sp.Response.Login resServerLogin = default;
            yield return ServerLoginAsync(sessionID, (ret) => { resServerLogin = ret; });

            switch (resServerLogin.ResponseCode)
            {
                case sp.Response.ResponseCode.Success:
                    {
                        _context.ServerModel.Session.SessionID = resServerLogin.SessionID;
                        _context.ServerModel.WritePermissionCode.Code = resServerLogin.WritePermissionCode;

                        LoginSuccessEvent(this, EventArgs.Empty);
                    }
                    break;
                case sp.Response.ResponseCode.NotFoundAccount:
                    {
                        sp.Response.Join resServerJoin = default;
                        yield return ServerJoinAsync(sessionID, (ret) => { resServerJoin = ret; });

                        switch (resServerJoin.ResponseCode)
                        {
                            case sp.Response.ResponseCode.Success:
                                yield return ServerLoginAsync(sessionID);
                                break;
                            case sp.Response.ResponseCode.InvalidLairSessionID:
                                OpenMessageBox((box) =>
                                {
                                    box.Init(_context);
                                    box.Message.text = _context.TextService.GetText("INVALID_PASSWORD_MESSAGE");
                                });
                                break;
                            default:
                                OpenMessageBox((box) =>
                                {
                                    box.Init(_context);
                                    box.Message.text = _context.TextService.GetText("NETWORK_ERROR_MESSAGE");
                                });
                                break;
                        }
                    }
                    break;
                case sp.Response.ResponseCode.InvalidLairSessionID:
                    OpenMessageBox(_context.TextService.GetText("INVALID_PASSWORD_MESSAGE"));
                    break;
                default:
                    OpenMessageBox(_context.TextService.GetText("NETWORK_ERROR_MESSAGE"));
                    break;
            }
        }

        private IEnumerator LairLoginAsync(string id, string pw, Action<lp.Response.Login> callback)
        {
            lp.Response.Login response = default;
            _context.LairApi.Login(new lp.Request.Login() { ID = id, Password = pw }).ContinueWith((ret) =>
                {
                    if (ret.IsCompleted && !ret.IsFaulted) { response = ret.Result; }
                    else { response = new lp.Response.Login { ResponseCode = lp.Response.ResponseCode.UndefinedFailure }; }
                });

            yield return new WaitUntil(() => response != default);

            callback(response);
        }

        private IEnumerator ServerLoginAsync(string sessionID, Action<sp.Response.Login> callback)
        {
            sp.Response.Login response = default;
            _context.ServerApi.Login(new sp.Request.Login { SessionID = sessionID }).ContinueWith((ret) =>
            {
                if (ret.IsCompleted && !ret.IsFaulted) { response = ret.Result; }
                else { response = new sp.Response.Login { ResponseCode = sp.Response.ResponseCode.UndefinedFailure }; }
            });

            yield return new WaitUntil(() => response != default);

            callback(response);
        }

        private IEnumerator ServerJoinAsync(string sessionID, Action<sp.Response.Join> callback)
        {
            sp.Response.Join response = default;
            _context.ServerApi.Join(new sp.Request.Join { SessionID = sessionID }).ContinueWith((ret) =>
             {
                 if (ret.IsCompleted && !ret.IsFaulted) { response = ret.Result; }
                 else { response = new sp.Response.Join { ResponseCode = sp.Response.ResponseCode.UndefinedFailure }; }
             });

            yield return new WaitUntil(() => response != default);

            callback(response);
        }
    }
}
