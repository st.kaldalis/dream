﻿using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class TitleWindowController : WindowController<TitleWindow>
    {
        public override void Init(Context context, TitleWindow window)
        {
            base.Init(context, window);

            _window.GameStartButton.gameObject.SetActive(false);

            _window.GameStartButtonClickEvent += OnGameStartButtonClickEvent;

            OpenLoginWindow();
        }

        private void OnGameStartButtonClickEvent(object sender, EventArgs e)
        {
            RunCoroutine(StartAsync());
        }

        protected IEnumerator StartAsync()
        {
            Curtain curtain = default;
            _context.CurtainRail.DropCurtain("PendingWebResponseCurtain", (ret) => { curtain = ret; });
            yield return new WaitUntil(() => curtain != default && curtain.GetState() == Curtain.State.Dropped);

            var isLoadFinished = false;
            var isSuccess = true;
            _context.WorldModel.Load((ret) =>
            {
                isLoadFinished = true;
                isSuccess = ret;
            });
            yield return new WaitUntil(() => isLoadFinished);

            if (!isSuccess)
            {
                OpenMessageBox(_context.TextService.GetText("FAILED_TO_LOAD_WORLDMODEL"));
                yield break;
            }

            var isLIftCurtainFinished = false;
            curtain.LiftCurtainFinishedEvent += (sender, args) => { isLIftCurtainFinished = true; };

            curtain.Lift();

            yield return new WaitUntil(() => isLIftCurtainFinished);

            _context.Screen.LoadGladiatorSelectionScene();
        }

        private void OpenLoginWindow()
        {
            RunCoroutine(OpenLoginWindowAsync());
        }

        private IEnumerator OpenLoginWindowAsync()
        {
            LoginWindow win = default;

            yield return _context.Pool.GetAsync("LoginWindow", (bo) =>
            {
                win = bo as LoginWindow;
            });

            var winCtrl = new LoginWindowController();

            win.Init(_context);

            winCtrl.Init(_context, win);

            winCtrl.LoginSuccessEvent += (sender, args) =>
            {
                var ctrl = sender as LoginWindowController;

                ctrl.GetWindow().Terminate();

                _window.GameStartButton.gameObject.SetActive(true);
            };

            winCtrl.OpenJoinWindowEvent += (sender, args) =>
            {
                var ctrl = sender as LoginWindowController;

                ctrl.GetWindow().Terminate();

                OpenJoinWindow();
            };

            _window.WindowStack.Add(win);
        }

        private void OpenJoinWindow()
        {
            RunCoroutine(OpenJoinWindowAsync());
        }

        private IEnumerator OpenJoinWindowAsync()
        {
            JoinWindow win = default;

            yield return _context.Pool.GetAsync("JoinWindow", (bo) =>
            {
                win = bo as JoinWindow;
            });

            var winCtrl = new JoinWindowController();

            win.Init(_context);

            winCtrl.Init(_context, win);

            winCtrl.CloseWindowEvent += (sender, args) =>
            {
                var ctrl = sender as JoinWindowController;

                ctrl.GetWindow().Terminate();

                OpenLoginWindow();
            };

            winCtrl.JoinCompleteEvent += (sender, args) =>
            {
                OpenMessageBox((box) =>
                {
                    box.Init(_context);
                    box.Message.text = _context.TextService.GetText("THANK_YOU_FOR_JOIN_US");
                    box.OKButtonClickEvent += (s, a) =>
                    {
                        box.Terminate();

                        win.Terminate();

                        OpenLoginWindow();
                    };
                });
            };

            _window.WindowStack.Add(win);
        }

        private void OnCloseJoinWindowEvent(object sender, EventArgs e)
        {
            var winCtrl = sender as JoinWindowController;
            var win = winCtrl.GetWindow();
            win.Terminate();

            OpenLoginWindow();
        }
    }
}