﻿using System;
using System.Collections;
using JS.MyGladiator.Client.View.UI;
using UnityEngine;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class JoinWindowController : WindowController<JoinWindow>
    {
        public event EventHandler JoinCompleteEvent;

        public event EventHandler CloseWindowEvent;

        public override void Init(Context context, JoinWindow window)
        {
            base.Init(context, window);

            _window.CancelButtonClickEvent += OnCancelButtonClickEvent;
            _window.SubmitButtonClickEvent += OnSubmitButtonClickEvent;
        }

        public override void CleanUp()
        {
            base.CleanUp();

            JoinCompleteEvent = default;
            JoinCompleteEvent += (sender, args) => { };

            CloseWindowEvent = default;
            CloseWindowEvent += (sender, args) => { };
        }

        protected void OnCancelButtonClickEvent(object sender, EventArgs args)
        {
            CloseWindowEvent(this, EventArgs.Empty);
        }

        protected void OnSubmitButtonClickEvent(object sender, EventArgs args)
        {
            if (string.IsNullOrEmpty(_window.ID))
            {
                OpenMessageBox((box) =>
                {
                    box.Init(_context);
                    box.Message.text = _context.TextService.GetText("INPUT_ID");
                });
                return;
            }

            if (string.IsNullOrEmpty(_window.Password))
            {
                OpenMessageBox((box) =>
                {
                    box.Init(_context);
                    box.Message.text = _context.TextService.GetText("INPUT_PASSWORD");
                });
                return;
            }

            if (!_window.ConfirmPassword.Equals(_window.Password))
            {
                OpenMessageBox((box) =>
                {
                    box.Init(_context);
                    box.Message.text = _context.TextService.GetText("MISMATCH_CONFIRM_PASSWORD");
                });
                return;
            }

            if (string.IsNullOrEmpty(_window.Email))
            {
                OpenMessageBox((box) =>
                {
                    box.Init(_context);
                    box.Message.text = _context.TextService.GetText("INPUT_EMAIL");
                });
                return;
            }

            RunCoroutine(JoinAsync(_window.ID, _window.Password, _window.Email));
        }

        private IEnumerator JoinAsync(string id, string pw, string email)
        {
            Curtain curtain = default;
            _context.CurtainRail.DropCurtain("PendingWebResponseCurtain", (ret) => { curtain = ret; });
            yield return new WaitUntil(() => curtain != default && curtain.GetState() == Curtain.State.Dropped);

            Lair.Common.Protocol.Response.Join response = default;
            _context.LairApi.Join(new Lair.Common.Protocol.Request.Join
            {
                ID = id,
                Password = pw,
                Email = email
            }).ContinueWith((ret) =>
            {
                if (ret.IsCompleted && !ret.IsFaulted) { response = ret.Result; }
                else { response = new Lair.Common.Protocol.Response.Join { ResponseCode = Lair.Common.Protocol.Response.ResponseCode.UndefinedFailure }; }
            });

            yield return new WaitUntil(() => response != default);

            switch (response.ResponseCode)
            {
                case Lair.Common.Protocol.Response.ResponseCode.Success:
                    JoinCompleteEvent(this, EventArgs.Empty);
                    break;
                case Lair.Common.Protocol.Response.ResponseCode.AlreadyExistID:
                    OpenMessageBox((box) =>
                    {
                        box.Init(_context);
                        box.Message.text = _context.TextService.GetText("NETWORK_ERROR_MESSAGE");
                    });
                    break;
                default:
                    OpenMessageBox((box) =>
                    {
                        box.Init(_context);
                        box.Message.text = _context.TextService.GetText("UNKNOWN_NETWORK_ERROR");
                    });
                    break;
            }

            curtain.Lift();
        }
    }
}
