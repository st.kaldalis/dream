﻿using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using UnityEngine;
using mw = JS.MyGladiator.Common.Model.World;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class GladiatorSelectionWindowController : WindowController<GladiatorSelectionWindow>
    {
        public override void Init(Context context, GladiatorSelectionWindow window)
        {
            base.Init(context, window);

            _window.StartButtonClickEvent += OnClickStartButton;
            _window.CreateNewGladiatorButtonEvent += OnCreateNewGladiatorButtonEvent;

            _window.GliadatorCellClickEvent += OnClickGladiatorCell;
        }

        private void OnClickGladiatorCell(object sender, GladiatorSelectionWindow.GladiatorCellClickEventArgs e)
        {
            var cell = e.Cell;
            _window.SetMainGladiator(cell.GetUnitModel());
        }

        private void OnCreateNewGladiatorButtonEvent(object sender, EventArgs e)
        {
            _context.Screen.LoadNewGladiatorSelectionScene();
        }

        public void OnClickStartButton(object sender, EventArgs args)
        {
            RunCoroutine(StartGameAsync());
        }

        private IEnumerator StartGameAsync()
        {
            Curtain curtain = default;
            yield return DropCurtainAsync("PendingWebResponseCurtain", (ret) => { curtain = ret; });

            Debug.Assert(!(_context.WorldModel.MainGladiator.Value == default && _window.MainGladiator == default));

            var isLoginSuccess = false;
            yield return LoginAtFrontDeskAsync(_window.MainGladiator, (ret) => { isLoginSuccess = ret; });

            if (!isLoginSuccess)
            {
                OpenMessageBox(_context.TextService.GetText("FAILED_TO_LOGIN_AT_FRONT_DESK"));
                yield return LiftCurtainAsync(curtain);
                yield break;
            }

            yield return LiftCurtainAsync(curtain);

            _context.Screen.LoadInnScene();
        }

        protected IEnumerator LoginAtFrontDeskAsync(mw.Unit unit, Action<bool> callback)
        {
            var isLogInFinished = false;
            var isLoginSuccess = false;
            _context.FrontDesk.LogIn(unit, (ret) => { isLogInFinished = true; isLoginSuccess = ret; });
            yield return new WaitUntil(() => isLogInFinished);

            callback(isLoginSuccess);
        }
    }
}
