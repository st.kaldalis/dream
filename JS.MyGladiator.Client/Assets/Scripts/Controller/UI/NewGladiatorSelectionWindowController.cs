﻿using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using res = JS.MyGladiator.Server.Common.Protocol.Response;
using mw = JS.MyGladiator.Common.Model.World;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class NewGladiatorSelectionWindowController : WindowController<NewGladiatorSelectionWindow>
    {
        private ObservableValue<List<mw.Unit>> _newGladiators;

        public override void Init(Context context, NewGladiatorSelectionWindow window)
        {
            base.Init(context, window);

            _newGladiators = _context.WorldModel.NewGladiators;
            _newGladiators.SetValueEvent += OnSetNewGladiatorsEvent;

            _window.BackButtonClickEvent += OnClickBackButtonEvent;
            _window.StartButtonClickEvent += OnClickStartButtonEvent;
            _window.GladiatorNameInputFieldValueChangedEvent += OnValueChangedGladiatorNameInputFieldEvent;
            _window.GliadatorCellClickEvent += OnClickGladiatorCell;
        }

        public override void CleanUp()
        {
            base.CleanUp();

            if (_newGladiators != default)
            {
                _newGladiators.SetValueEvent -= OnSetNewGladiatorsEvent;
            }
            _newGladiators = default;
        }

        private void OnSetNewGladiatorsEvent(object sender, ObservableValue<List<mw.Unit>>.SetValueEventArgs e)
        {
            _window.SetNewGladiators(e.Value);
        }

        private void OnClickGladiatorCell(object sender, NewGladiatorSelectionWindow.GladiatorCellClickEventArgs e)
        {
            var cell = e.Cell;
            _window.SetGladiator(cell.GetUnitModel());
        }

        private void OnValueChangedGladiatorNameInputFieldEvent(object sender, NewGladiatorSelectionWindow.GladiatorNameInputFieldValuChangedEventArgs e)
        {
            _window.SetGladiatorName(e.Value);
        }

        private void OnClickStartButtonEvent(object sender, EventArgs e)
        {
            RunCoroutine(StartAsync());
        }

        private void OnClickBackButtonEvent(object sender, EventArgs e)
        {
            _context.Screen.LoadGladiatorSelectionScene();
        }

        private IEnumerator StartAsync()
        {
            Curtain curtain = default;
            yield return DropCurtainAsync("PendingWebResponseCurtain", (ret) => { curtain = ret; });

            var isRegisterSuccess = false;
            yield return RegisterGladiatorAsync(_window.Gladiator, _window.GladiatorName, (ret) => { isRegisterSuccess = ret; });

            if (!isRegisterSuccess)
            {
                OpenMessageBox(_context.TextService.GetText("FAILED_TO_REGISTER_GLADIATOR"));
                yield return LiftCurtainAsync(curtain);
                yield break;
            }

            var isLoginSuccess = false;
            yield return LoginAtFrontDeskAsync(_window.Gladiator, (ret) => { isLoginSuccess = ret; });

            if (!isLoginSuccess)
            {
                OpenMessageBox(_context.TextService.GetText("FAILED_TO_LOGIN_AT_FRONT_DESK"));
                yield return LiftCurtainAsync(curtain);
                yield break;
            }

            yield return LiftCurtainAsync(curtain);

            _context.Screen.LoadInnScene();
        }

        protected IEnumerator LoginAtFrontDeskAsync(mw.Unit unit, Action<bool> callback)
        {
            var isLogInFinished = false;
            var isLoginSuccess = false;
            _context.FrontDesk.LogIn(unit, (ret) => { isLogInFinished = true; isLoginSuccess = ret; });
            yield return new WaitUntil(() => isLogInFinished);

            callback(isLoginSuccess);
        }

        protected IEnumerator RegisterGladiatorAsync(mw.Unit gladiator, string gladiatorName, Action<bool> callback)
        {
            res.RegisterGladiator response = default;
            _context.ServerApi.RegisterGladiator(new req.RegisterGladiator
            {
                UnitKey = gladiator.GetUnitUserData().Key,
                Name = gladiatorName,
                SessionID = _context.ServerModel.Session.SessionID,
                WritePermissionCode = _context.ServerModel.WritePermissionCode.Code
            }).ContinueWith((ret) =>
            {
                if (ret.IsCompleted && !ret.IsFaulted) { response = ret.Result; }
                else { response = new res.RegisterGladiator { ResponseCode = res.ResponseCode.UndefinedFailure }; }
            });
            yield return new WaitUntil(() => response != default);

            switch (response.ResponseCode)
            {
                case res.ResponseCode.Success:
                    _context.ServerModel.WritePermissionCode.Code = response.WritePermissionCode;

                    var newGladiators = _newGladiators.Value;
                    newGladiators.RemoveAll(unit => unit.GetUnitUserData().Key == gladiator.GetUnitUserData().Key);

                    _newGladiators.Value = newGladiators;

                    callback(true);
                    break;
                default:
                    _context.Logger.Info($"Failed to register gladiator. Response Code : {response.ResponseCode}");

                    callback(false);
                    break;
            }
        }
    }
}
