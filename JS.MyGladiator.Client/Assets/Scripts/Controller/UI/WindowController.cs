﻿using System;
using System.Collections;
using JS.MyGladiator.Client.Controller.Event;
using JS.MyGladiator.Client.View.UI;
using UnityEngine;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class WindowController<T> : BaseObject where T : Window
    {
        protected T _window;

        public override void CleanUp()
        {
            base.CleanUp();

            if (_window != default)
            {
                _window = default;
            }
        }

        public virtual void Init(Context context, T window)
        {
            _context = context;

            _window = window;

            void handler(object sender, EventArgs args)
            {
                _window.CleanUpEvent -= handler;
                CleanUp();
            }
            _window.CleanUpEvent += handler;
        }

        public T GetWindow()
        {
            return _window;
        }

        protected void OpenMessageBox(string message)
        {
            OpenMessageBox((box) =>
            {
                box.Init(_context);
                box.Message.text = message;
            });
        }

        protected void OpenMessageBox(Action<MessageBox> callback)
        {
            RunCoroutine(OpenMessageBoxAsync(callback));
        }

        private IEnumerator OpenMessageBoxAsync(Action<MessageBox> callback)
        {
            yield return _context.Pool.GetAsync("MessageBox", (bo) =>
            {
                var msgBox = bo as MessageBox;

                callback(msgBox);

                msgBox.OKButtonClickEvent += (sender, args) => { msgBox.Terminate(); };

                _context.Screen.Scene.MainWindow.ModalWindowStack.Add(msgBox);
            });
        }

        protected IEnumerator DropCurtainAsync(string objectType, Action<Curtain> callback)
        {
            Curtain curtain = default;
            _context.CurtainRail.DropCurtain(objectType, (ret) => { curtain = ret; });
            yield return new WaitUntil(() => curtain != default && curtain.GetState() == Curtain.State.Dropped);

            callback(curtain);
        }

        protected IEnumerator LiftCurtainAsync(Curtain curtain)
        {
            var isLiftCurtainFinished = false;
            void handler(object sender, LiftCurtainFinishedEventArgs args)
            {
                isLiftCurtainFinished = true;
                curtain.LiftCurtainFinishedEvent -= handler;
            }
            curtain.LiftCurtainFinishedEvent += handler;

            curtain.Lift();

            yield return new WaitUntil(() => isLiftCurtainFinished);
        }
    }
}
