﻿using JS.MyGladiator.Client.View.UI;
using System;

namespace JS.MyGladiator.Client.Controller.UI
{
    public class SceneNavigationPanelController : BaseObject
    {
        private SceneNavigationPanel _sceneNavigationPanel;

        public void Init(Context context, SceneNavigationPanel panel)
        {
            Init(context);

            _sceneNavigationPanel = panel;

            panel.InnButtonClickEvent += OnInnButtonClickEvent;
            panel.GladiatorUnionButtonClickEvent += OnGladiatorUnionButtonClickEvent;
            panel.BlacksmithButtonClickEvent += OnBlacksmithButtonClickEvent;
            panel.MagicianHouseButtonClickEvent += OnMagicianHouseButtonClickEvent;
            panel.AlchemistHouseButtonClickEvent += OnAlchemistHouseButtonClickEvent;
            panel.WeirdStoreButtonClickEvent += OnWeirdStoreButtonClickButton;
        }

        private void OnWeirdStoreButtonClickButton(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnMagicianHouseButtonClickEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnBlacksmithButtonClickEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnGladiatorUnionButtonClickEvent(object sender, EventArgs e)
        {
            _context.Screen.LoadGladiatorUnionScene();
        }

        private void OnInnButtonClickEvent(object sender, EventArgs e)
        {
            _context.Screen.LoadInnScene();
        }

        private void OnAlchemistHouseButtonClickEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public override void CleanUp()
        {
            base.CleanUp();

            if (_sceneNavigationPanel != default)
            {
                _sceneNavigationPanel.InnButtonClickEvent -= OnInnButtonClickEvent;
                _sceneNavigationPanel.GladiatorUnionButtonClickEvent -= OnGladiatorUnionButtonClickEvent;
                _sceneNavigationPanel.BlacksmithButtonClickEvent -= OnBlacksmithButtonClickEvent;
                _sceneNavigationPanel.MagicianHouseButtonClickEvent -= OnMagicianHouseButtonClickEvent;
                _sceneNavigationPanel.AlchemistHouseButtonClickEvent -= OnAlchemistHouseButtonClickEvent;
                _sceneNavigationPanel.WeirdStoreButtonClickEvent -= OnWeirdStoreButtonClickButton;
            }
        }
    }
}
