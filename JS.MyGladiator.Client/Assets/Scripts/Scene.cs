﻿using JS.MyGladiator.Client.View.UI;
using System;

namespace JS.MyGladiator.Client
{
    public abstract class Scene : BaseObject
    {
        public BaseMonoBehaviour MainStage;

        public MainWindow MainWindow;

        public event EventHandler LoadFinishedEvent;

        protected Scene()
        {
            LoadFinishedEvent = default;
            LoadFinishedEvent += (s, e) => { };
        }

        protected void TriggerLoadFinishedEvent(object sender, EventArgs args)
        {
            LoadFinishedEvent(sender, args);
        }

        /// <summary>
        /// 씬에서 사용할 리소스를 로드 한다. 
        /// 로드과정에는 풀을 준비시키는 것이 포함될 수 있다. 
        /// 로드 과정에서 문제가 발생 했을 시 로그를 남기고 앱을 중단 한다.
        /// </summary>
        public abstract void Load();

        /// <summary>
        /// 씬에서 사용한 리소스를 언로드 한다. 
        /// </summary>
        public abstract void Unload();
    }
}
