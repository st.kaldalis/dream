﻿using System;
using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    [DisallowMultipleComponent]
    public abstract class BaseMonoBehaviour : MonoBehaviour
    {
        [SerializeField]
        private string _objecType;

        private Guid _guid;

        protected Context _context;

        public event EventHandler CleanUpEvent;

        public delegate void TerminateFunc();
        public TerminateFunc Terminate;

        public string GetObjectType() { return _objecType; }

        public Guid GetGuid()
        {
            return _guid;
        }

        public virtual void Init(Context context)
        {
            _guid = Guid.NewGuid();

            _context = context;
        }

        protected void RunCoroutine(IEnumerator routine)
        {
            _context.CoroutineRunner.Run(routine);
        }

        public virtual void CleanUp()
        {
            if (CleanUpEvent != default)
            {
                CleanUpEvent(this, EventArgs.Empty);
            }

            _guid = Guid.Empty;

            _context = default;

            CleanUpEvent = default;
            CleanUpEvent += (sender, args) => { };
        }
    }
}