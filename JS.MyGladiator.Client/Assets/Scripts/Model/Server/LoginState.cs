﻿namespace JS.MyGladiator.Client.Model.Server
{
    public enum LoginState : int
    {
        INIT,
        IDLE,
        TRANSMISSION,
        SUCCESS,
        FAILURE
    }
}
