﻿using JS.MyGladiator.Net;
using System.Net.Sockets;

namespace Model.ChattingServer
{
    public class ChattingServerModel
    {
        public IPacketService PacketService;

        public IPool<SocketAsyncEventArgs> SaePool;

        public IPool<Token> TokenPool;

        public Session Session;
    }
}
