﻿using System;
using System.Collections;
using System.Collections.Generic;
using JS.MyGladiator.Common.Model.World;
using UnityEngine;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using res = JS.MyGladiator.Server.Common.Protocol.Response;

namespace JS.MyGladiator.Client.Model.World
{
    public class WorldModel : BaseObject
    {
        public ObservableValue<List<Unit>> AllGladiator;

        public ObservableValue<Unit> MainGladiator;

        public ObservableValue<List<Unit>> NewGladiators;

        public WorldModel()
        {
            AllGladiator = new ObservableValue<List<Unit>>
            {
                Value = new List<Unit>()
            };

            NewGladiators = new ObservableValue<List<Unit>>
            {
                Value = new List<Unit>()
            };

            MainGladiator = new ObservableValue<Unit>();
        }

        public void Load(Action<bool> callback)
        {
            RunCoroutine(LoadAsync(callback));
        }

        protected IEnumerator LoadAsync(Action<bool> callback)
        {
            var isSuccess = true;

            yield return LoadMainGladiatorAsync((ret) => { isSuccess = ret; });
            if (!isSuccess) { callback(isSuccess); yield break; }

            yield return LoadGladiatorsAsync((ret) => { isSuccess = ret; });
            if (!isSuccess) { callback(isSuccess); yield break; }

            yield return LoadNewGladiatorsAsync((ret) => { isSuccess = ret; });
            if (!isSuccess) { callback(isSuccess); yield break; }

            callback(isSuccess);
        }

        public IEnumerator LoadMainGladiatorAsync(Action<bool> callback)
        {
            var session = _context.ServerModel.Session;

            res.GetMainGladiator response = default;

            _context.ServerApi.GetMainGladiator(new req.GetMainGladiator { SessionID = session.SessionID }).ContinueWith((ret) =>
            {
                if (ret.IsCompleted && !ret.IsFaulted) { response = ret.Result; }
                else { response = new res.GetMainGladiator { ResponseCode = res.ResponseCode.UndefinedFailure }; }
            });

            yield return new WaitUntil(() => response != default);

            if (response.ResponseCode != res.ResponseCode.Success)
            {
                _context.Logger.Info($"Failed to load MainGladiator. Response Code : {response.ResponseCode}");

                callback(false);
                yield break;
            }

            var ssUnit = response.Gladiator;

            if (ssUnit == null)
            {
                MainGladiator.Value = null;
            }
            else
            {
                var gdUnit = _context.GameDataService.GetUnit((item) => item.ID == ssUnit.UnitID);

                var mwUnit = new Unit();
                mwUnit.Init(gdUnit, ssUnit);

                MainGladiator.Value = mwUnit;
            }

            callback(true);
        }

        public IEnumerator LoadGladiatorsAsync(Action<bool> callback)
        {
            var session = _context.ServerModel.Session;

            res.GetAllGladiator response = default;

            _context.ServerApi.GetAllGladiator(new req.GetAllGladiator { SessionID = session.SessionID }).ContinueWith((ret) =>
            {
                if (ret.IsCompleted && !ret.IsFaulted) { response = ret.Result; }
                else { response = new res.GetAllGladiator { ResponseCode = res.ResponseCode.UndefinedFailure }; }
            });

            yield return new WaitUntil(() => response != default);

            if (response.ResponseCode != res.ResponseCode.Success)
            {
                _context.Logger.Info($"Failed to load Gladiators. Response Code : {response.ResponseCode}");

                callback(false);
                yield break;
            }

            var mwAllUnit = new List<Unit>();
            foreach (var ssUnit in response.Gladiators)
            {
                var gdUnit = _context.GameDataService.GetUnit((item) => item.ID == ssUnit.UnitID);

                var mwUnit = new Unit();
                mwUnit.Init(gdUnit, ssUnit);

                mwAllUnit.Add(mwUnit);
            }

            AllGladiator.Value = mwAllUnit;

            callback(true);
        }

        public IEnumerator LoadNewGladiatorsAsync(Action<bool> callback)
        {
            var session = _context.ServerModel.Session;

            res.GetAllNewGladiator response = default;
            _context.ServerApi.GetAllNewGladiator(new req.GetAllNewGladiator { SessionID = session.SessionID }).ContinueWith((ret) =>
            {
                if (ret.IsCompleted && !ret.IsFaulted) { response = ret.Result; }
                else { response = new res.GetAllNewGladiator { ResponseCode = res.ResponseCode.UndefinedFailure }; }
            });

            yield return new WaitUntil(() => response != default);

            if (response.ResponseCode != res.ResponseCode.Success)
            {
                _context.Logger.Info($"Failed to load NewGladiators. Response Code : {response.ResponseCode}");

                callback(false);
                yield break;
            }

            var mwAllUnit = new List<Unit>();
            foreach (var ssUnit in response.NewGladiators)
            {
                var gdUnit = _context.GameDataService.GetUnit((item) => item.ID == ssUnit.UnitID);

                var mwUnit = new Unit();
                mwUnit.Init(gdUnit, ssUnit);

                mwAllUnit.Add(mwUnit);
            }
            NewGladiators.Value = mwAllUnit;

            callback(true);
        }
    }
}
