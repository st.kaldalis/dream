﻿using JS.MyGladiator.Client.Controller.Event;
using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;

namespace JS.MyGladiator.Client
{
    public class CurtainRail : BaseMonoBehaviour
    {
        public WindowStack WindowStack;

        public void DropCurtain(string objectType, Action<Curtain> callback)
        {
            RunCoroutine(DropCurtainAsync(objectType, callback));
        }

        private IEnumerator DropCurtainAsync(string objectType, Action<Curtain> callback)
        {
            var window = WindowStack.Find((item) => { return item.GetObjectType().Equals(objectType); });
            if (window != default)
            {
                callback(window as Curtain);
                yield break;
            }

            yield return _context.Pool.GetAsync(objectType, (bo) =>
            {
                var curtain = bo as Curtain;
                callback(curtain);

                curtain.Init(_context);

                WindowStack.Add(curtain);

                curtain.Drop();

                curtain.LiftCurtainFinishedEvent += (sender, args) => { WindowStack.Remove(args.Curtain.GetGuid()); };
            });
        }
    }
}
