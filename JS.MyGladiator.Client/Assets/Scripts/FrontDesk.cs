﻿using JS.MyGladiator.Client.ChattingUtility;
using System;
using System.Collections;
using System.Runtime.Remoting.Channels;
using UnityEngine;
using mw = JS.MyGladiator.Common.Model.World;

namespace JS.MyGladiator.Client
{
    // 검투사를 로그인 로그 아웃 합니다
    public class FrontDesk : BaseObject
    {
        /// <summary>
        /// 로그인 합니다. 
        /// 로그인에는 주 검투사 등록, 검투사를 서버에 연결 시켜주는 작업이 포함됩니다. 
        /// 이미 프론트에 로그인된 검투사 가 있을 경우 이 시도는 실패합니다.
        /// 주 검투사가 이미 등록 된 상태에서는 주 검투사 등록을 하지 않습니다.
        /// </summary>
        /// <param name="unit"></param>
        public void LogIn(mw.Unit unit, Action<bool> callback)
        {
            Debug.Assert(unit != default);

            RunCoroutine(LogInAsync(unit, callback));
        }

        protected IEnumerator LogInAsync(mw.Unit unit, Action<bool> callback)
        {
            if (_context.WorldModel.MainGladiator.Value == default || !unit.GetUnitUserData().Key.Equals(_context.WorldModel.MainGladiator.Value.GetUnitUserData().Key))
            {
                Server.Common.Protocol.Response.SetMainGladiator response = default;
                _context.ServerApi.SetMainGladiator(new Server.Common.Protocol.Request.SetMainGladiator()
                {
                    SessionID = _context.ServerModel.Session.SessionID,
                    UnitKey = unit.GetUnitUserData().Key,
                    WritePermissionCode = _context.ServerModel.WritePermissionCode.Code
                }).ContinueWith((ret) =>
                {
                    if (ret.IsCompleted && !ret.IsFaulted) { response = ret.Result; }
                    else { response = new Server.Common.Protocol.Response.SetMainGladiator { ResponseCode = Server.Common.Protocol.Response.ResponseCode.UndefinedFailure }; }
                });

                yield return new WaitUntil(() => response != default);

                switch (response.ResponseCode)
                {
                    case Server.Common.Protocol.Response.ResponseCode.Success:
                        _context.ServerModel.WritePermissionCode.Code = response.WritePermissionCode;
                        _context.WorldModel.MainGladiator.Value = unit;
                        break;
                    default:
                        _context.Logger.Info($"Set main gladiator request has failed. response code is {response.ResponseCode}");
                        callback(false);
                        yield break;
                }
            }


            var chattingConsole = _context.ChattingConsole;

            var isLoginResultReceived = false;
            void OnLoginResultReceivedEvent(object sender, LoginResultReceivedEventArgs args)
            {
                isLoginResultReceived = true;

                chattingConsole.LoginResultReceivedEvent -= OnLoginResultReceivedEvent;
            }
            chattingConsole.LoginResultReceivedEvent += OnLoginResultReceivedEvent;

            chattingConsole.Login("127.0.0.1", 11000, _context.ServerModel.Session.SessionID, 3.0f);

            yield return new WaitUntil(() => isLoginResultReceived);

            callback(true);
        }

        public void LogOut(mw.Unit unit) { }

    }
}
