﻿using JS.MyGladiator.Client.Controller.UI;
using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class NewGladiatorSelectionScene : Scene
    {
        public NewGladiatorSelectionScene() : base()
        {
        }

        public override void Load()
        {
            RunCoroutine(LoadAsync());
        }

        private IEnumerator LoadAsync()
        {
            Debug.Assert(MainStage == default);
            Debug.Assert(MainWindow == default);

            Debug.Assert(_context.MainUICanvas.GetWindow() == default);

            NewGladiatorSelectionWindow win = default;
            yield return _context.Pool.GetAsync("NewGladiatorSelectionWindow", (bo) =>
            {
                MainWindow = win = bo as NewGladiatorSelectionWindow;
            });

            var winCtrl = new NewGladiatorSelectionWindowController();

            win.Init(_context, _context.WorldModel.NewGladiators.Value);
            
            winCtrl.Init(_context, win);

            win.GliadatorCellClickEvent += (sender, args) =>
            {
            };

            MainWindow = win;

            _context.MainUICanvas.SetWindow(win);
            
            TriggerLoadFinishedEvent(this, EventArgs.Empty);
        }

        public override void Unload()
        {
            Debug.Assert(MainStage != default);
            Debug.Assert(MainWindow != default);

            Debug.Assert(_context.MainUICanvas.GetWindow() != default);

            Debug.Assert(_context.MainUICanvas.GetWindow().GetGuid() == MainWindow.GetGuid());
            
            MainWindow.Terminate();
            MainWindow = default;

            MainStage.Terminate();
            MainStage = default;

            _context.MainUICanvas.UnsetWindow();
        }
    }
}
