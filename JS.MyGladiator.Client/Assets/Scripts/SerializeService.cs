﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using req = JS.MyGladiator.Server.Common.Protocol.Request;

namespace JS.MyGladiator.Client
{
    [DataContract]
    [KnownType(typeof(req.SetMainGladiator))]
    [KnownType(typeof(req.RegisterGladiator))]
    public class SerializeService
    {
        public T FromJson<T>(string value)
        {
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(value));
            var ser = new DataContractJsonSerializer(typeof(T));
            var ret = (T)ser.ReadObject(ms);
            ms.Close();

            return ret;
        }

        public string ToJson<T>(T value)
        {
            var ms = new MemoryStream();
            var ser = new DataContractJsonSerializer(typeof(T));
            ser.WriteObject(ms, value);
            ms.Position = 0;
            StreamReader sr = new StreamReader(ms);
            var ret = sr.ReadToEnd();
            ms.Close();

            return ret;
        }
    }
}
