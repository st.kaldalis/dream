﻿using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class Screen : BaseObject
    {
        public Scene Scene { protected set; get; }

        public void LoadLogoScene()
        {
            if (Scene != default) { UnloadScene(); }

            Scene = new LogoScene();
            Scene.Init(_context);
            Scene.Load();
        }

        public void LoadTitleScene()
        {
            RunCoroutine(LoadSceneAsync(new TitleScene()));
        }

        public void LoadGladiatorSelectionScene()
        {
            RunCoroutine(LoadSceneAsync(new GladiatorSelectionScene()));
        }

        public void LoadDungeonScene()
        {
            throw new NotImplementedException();
        }

        public void LoadNewGladiatorSelectionScene()
        {
            RunCoroutine(LoadSceneAsync(new NewGladiatorSelectionScene()));
        }

        public void LoadInnScene()
        {
            RunCoroutine(LoadSceneAsync(new InnScene()));
        }

        public void LoadGladiatorUnionScene()
        {
            RunCoroutine(LoadSceneAsync(new GladiatorUnionScene()));
        }

        /// <summary>
        /// 커튼을 치고 난 후 기존 씬이 있다면 해당씬을 언로드 하고 새로 로드 될 씬을 로드 한다. 새로 로드 될 씬의 로드가 끝나면 커튼을 걷어 준다.
        /// 기존에 쳐저 있는 커튼이 있을 경우 씬 전환 이후에 그것이 보이면서 좋지 않은 경험을 주기 때문에 없는 것이 보장되어야 한다.
        /// </summary>
        /// <param name="scene"> 새로 로드될 씬 </param>
        /// <returns></returns>
        protected IEnumerator LoadSceneAsync(Scene scene)
        {
            Debug.Assert(_context.CurtainRail.WindowStack.Count() == 0);

            Curtain curtain = default;
            _context.CurtainRail.DropCurtain("CutawayCurtain", (ret) => { curtain = ret; });
            yield return new WaitUntil(() => curtain != default && curtain.GetState() == Curtain.State.Dropped);

            if (Scene != default) { UnloadScene(); }

            Scene = scene;
            Scene.Init(_context);
            Scene.Load();

            void OnLoadFinishedEvent(object sender, EventArgs args)
            {
                Scene.LoadFinishedEvent -= OnLoadFinishedEvent;
                curtain.Lift();
            }
            Scene.LoadFinishedEvent += OnLoadFinishedEvent;
        }

        protected void UnloadScene()
        {
            Debug.Assert(Scene != default);

            Scene.Unload();
            Scene = default;
        }
    }
}
