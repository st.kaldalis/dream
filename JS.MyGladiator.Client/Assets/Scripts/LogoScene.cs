﻿using JS.MyGladiator.Client.Controller.UI;
using JS.MyGladiator.Client.View.UI;
using System;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class LogoScene : Scene
    {
        public LogoScene() : base()
        {
        }

        public override void Load()
        {
            Debug.Assert(MainWindow == default);
            Debug.Assert(_context.MainUICanvas.GetWindow() == default);

            var original = Resources.Load<GameObject>("LogoWindow");
            var obj = Instantiate(original);
            var win = obj.GetComponent<LogoWindow>();

            var winCtrl = new LogoWindowController();

            win.Init(_context);
            win.Terminate = () => { Destroy(win.gameObject); };

            winCtrl.Init(_context, win);

            MainWindow = win;

            _context.MainUICanvas.SetWindow(win);

            TriggerLoadFinishedEvent(this, EventArgs.Empty);
        }

        public override void Unload()
        {
            Debug.Assert(MainWindow != default);
            Debug.Assert(_context.MainUICanvas.GetWindow() != default);
            Debug.Assert(_context.MainUICanvas.GetWindow().GetGuid() == MainWindow.GetGuid());

            MainWindow.Terminate();
            MainWindow = default;

            _context.MainUICanvas.UnsetWindow();
        }
    }
}
