﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class DummyAssetBundleManager : MonoBehaviour, IAssetBundleManager
    {
        public string AssetBundleRootPath;

        public JS.Common.ILogger Logger;

        public T Load<T>(string bundleName, string path) where T : UnityEngine.Object
        {
            T asset = AssetDatabase.LoadAssetAtPath<T>(AssetBundleRootPath + path);
            if (asset == null)
            {
                Logger.Fatal($"Failed to load asset. Bundle name : {bundleName}, Path : {AssetBundleRootPath + path}");
            }

            return asset;
        }

        public IEnumerator LoadAsync<T>(string bundleName, string path, Action<T> callback) where T : UnityEngine.Object
        {
            callback(Load<T>(bundleName, path));

            yield return null;
        }
    }
}
