﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class Pool : MonoBehaviour
    {
        [Serializable]
        public struct PooledObjectMetaData
        {
            public string ObjectType;

            public string BundleName;
            public string Path;

            public double ExpireTime;
        }

        public double CleaningCycle;

        protected double _nextCleaningTime;

        protected Context _context;

        protected Dictionary<string, Stack<BaseMonoBehaviour>> _pool;

        protected Dictionary<string, double> _lastPulledTime;

        public void Init()
        {
            _nextCleaningTime = CleaningCycle;

            if (_pool != default)
            {
                foreach (var pool in _pool)
                {
                    while (pool.Value.Count > 0)
                    {
                        Destroy(pool.Value.Pop());
                    }
                }
            }

            _pool = new Dictionary<string, Stack<BaseMonoBehaviour>>();
            _lastPulledTime = new Dictionary<string, double>();
        }

        public void Init(Context context)
        {
            Init();

            _context = context;
        }

        public IEnumerator GetAsync(string objectType, Action<BaseMonoBehaviour> callback)
        {
            if (GetPooledObjectInfo(objectType) == default)
            {
                _context.Logger.Fatal($"{objectType} cannot find in pooled object info.");
                callback(default);
                yield break;
            }

            _lastPulledTime[objectType] = GetNow();

            if (!_pool.ContainsKey(objectType))
            {
                _pool[objectType] = new Stack<BaseMonoBehaviour>();
            }

            BaseMonoBehaviour bo = default;

            var pool = _pool[objectType];
            if (pool.Count > 0)
            {
                bo = pool.Pop();
                bo.gameObject.SetActive(true);
            }
            else
            {
                var prefabInfo = _context.GameDataService.GetPrefabInfo(item => item.ObjectType == objectType);
                yield return _context.AssetBundleManager.LoadAsync<GameObject>(prefabInfo.BundleName, prefabInfo.Path, (pf) =>
                {
                    bo = Instantiate(objectType, pf);
                });
            }

            if (bo == default)
            {
                yield break;
            }

            bo.Terminate = () =>
            {
                _context.Pool.Return(bo);
                bo.Terminate = () => { };
            };
            callback(bo);
        }

        private BaseMonoBehaviour Instantiate(string objectType, GameObject pf)
        {
            Debug.Assert(pf != null, $"Failed to laod prefab for object type : {objectType}");

            var go = Instantiate(pf, transform, false);

            var bo = go.GetComponent<BaseMonoBehaviour>();

            Debug.Assert(bo != null, $"{objectType} does not have BaseObject.");

            Debug.Assert(bo.GetObjectType().Equals(objectType), $"Object type mismatch. Expected object type was {objectType}. But loaded object type is {bo.GetObjectType()}");

            bo.gameObject.SetActive(true);
            bo.CleanUp();

            return bo;
        }

        private double GetNow()
        {
            return DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public void Return(BaseMonoBehaviour bo)
        {
            var objecType = bo.GetObjectType();

            Debug.Assert(_pool.ContainsKey(objecType), $"{objecType} cannot find in pooled object meta data list.");

            bo.transform.SetParent(transform, false);

            bo.CleanUp();
            bo.gameObject.SetActive(false);

            _pool[objecType].Push(bo);
        }

        private Common.GameData.PooledObjectInfo GetPooledObjectInfo(string objectType)
        {
            return _context.GameDataService.GetPooledObjectInfo(item => item.ObjectType == objectType);
        }

        private void Update()
        {
            if (_nextCleaningTime > 0.0f)
            {
                _nextCleaningTime -= Time.deltaTime;
                return;
            }
            _nextCleaningTime = CleaningCycle;

            var now = GetNow();
            var gameObjects = new List<GameObject>();

            foreach (var item in _lastPulledTime)
            {
                var objectType = item.Key;
                var lastPulledTime = item.Value;
                var pooledObjectInfo = GetPooledObjectInfo(objectType);
                if (lastPulledTime + pooledObjectInfo.ExpireTime < now && _pool[objectType].Count > 0)
                {
                    var bo = _pool[objectType].Pop();
                    gameObjects.Add(bo.gameObject);
                }
            }

            StartCoroutine(Destroy(gameObjects));
        }

        private IEnumerator Destroy(List<GameObject> gameObjects)
        {
            foreach (var go in gameObjects)
            {
                Destroy(go);
                yield return null;
            }
        }
    }
}