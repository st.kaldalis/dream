﻿using JS.MyGladiator.Client.Controller.UI;
using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using UnityEngine;

namespace JS.MyGladiator.Client
{
    public class GladiatorSelectionScene : Scene
    {
        public GladiatorSelectionScene() : base()
        {
        }

        public override void Load()
        {
            RunCoroutine(LoadAsync());
        }

        private IEnumerator LoadAsync()
        {
            Debug.Assert(MainStage == default);
            Debug.Assert(MainWindow == default);

            Debug.Assert(_context.MainUICanvas.GetWindow() == default);

            GladiatorSelectionWindow win = default;
            yield return _context.Pool.GetAsync("GladiatorSelectionWindow", (bo) =>
            {
                MainWindow = win = bo as GladiatorSelectionWindow;
            });

            var winCtrl = new GladiatorSelectionWindowController();

            win.Init(_context, _context.WorldModel.AllGladiator.Value, _context.WorldModel.MainGladiator.Value);
            
            winCtrl.Init(_context, win);

            win.GliadatorCellClickEvent += (sender, args) =>
            {
            };

            MainWindow = win;

            _context.MainUICanvas.SetWindow(win);
            
            TriggerLoadFinishedEvent(this, EventArgs.Empty);
        }

        public override void Unload()
        {
            Debug.Assert(MainStage != default);
            Debug.Assert(MainWindow != default);

            Debug.Assert(_context.MainUICanvas.GetWindow() != default);

            Debug.Assert(_context.MainUICanvas.GetWindow().GetGuid() == MainWindow.GetGuid());

            MainWindow.Terminate();
            MainWindow = default;

            MainStage.Terminate();
            MainStage = default;

            _context.MainUICanvas.UnsetWindow();
        }
    }
}
