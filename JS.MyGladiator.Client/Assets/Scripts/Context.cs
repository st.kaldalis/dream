﻿using JS.MyGladiator.Client.ChattingUtility;
using JS.Common;
using JS.Lair.Common;
using JS.MyGladiator.Client.GameDataService;
using JS.MyGladiator.Client.Model.Lair;
using JS.MyGladiator.Client.Model.Server;
using JS.MyGladiator.Client.Model.World;
using JS.MyGladiator.Client.View.UI;
using JS.MyGladiator.Server.Common;
using Model.ChattingServer;
using JS.MyGladiator.Client.View;

namespace JS.MyGladiator.Client
{
    public class Context
    {
        public MainUICanvas MainUICanvas;

        public World World;

        public Screen Screen;

        public CurtainRail CurtainRail;

        public ILogger Logger;

        public IAssetBundleManager AssetBundleManager;

        public FrontDesk FrontDesk;

        public FileService FileService;

        public SerializeService SerializeService;

        public Common.GameData.GameDataService GameDataService;

        public TextService TextService;

        public CoroutineRunner CoroutineRunner;

        public ChattingConsole ChattingConsole;

        public Pool Pool;

        public ServerApi ServerApi;

        public LairApi LairApi;

        public LairModel LairModel;

        public WorldModel WorldModel;

        public ServerModel ServerModel;

        public ChattingServerModel ChattingServerModel;
    }
}