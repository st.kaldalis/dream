﻿using JS.Common;
using JS.MyGladiator.Common.GameData;

namespace JS.MyGladiator.Client.GameDataService
{
    public class TextService
    {
        public LANGUAGE_CODE LanguageCode;

        public ILogger Logger;

        private Common.GameData.GameDataService _gameDataService;

        public TextService(Common.GameData.GameDataService gameDataService)
        {
            _gameDataService = gameDataService;
        }

        public string GetText(string id)
        {
            if (_gameDataService.HasText(id, LanguageCode))
            {
                return _gameDataService.GetText(id, LanguageCode);
            }

            Logger.Warn($"Can`t find text for text id - {id}");

            return string.Empty;
        }
    }
}
