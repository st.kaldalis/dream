﻿using JS.MyGladiator.Client.Controller.UI;
using JS.MyGladiator.Client.View.UI;
using System;
using System.Collections;
using System.Diagnostics;

namespace JS.MyGladiator.Client
{
    public class DungeonScene : Scene
    {
        public DungeonScene() : base()
        {
        }

        public override void Load()
        {
            RunCoroutine(LoadAsync());
        }

        private IEnumerator LoadAsync()
        {
            Debug.Assert(MainStage == default);
            Debug.Assert(MainWindow == default);

            Debug.Assert(_context.MainUICanvas.GetWindow() == default);

            DungeonWindow win = default;
            yield return _context.Pool.GetAsync("DungeonWindow", (bo) =>
            {
                MainWindow = win = bo as DungeonWindow;
            });

            var winCtrl = new DungeonWindowController();

            win.Init(_context);
            
            winCtrl.Init(_context, win);

            MainWindow = win;

            _context.MainUICanvas.SetWindow(win);
            
            TriggerLoadFinishedEvent(this, EventArgs.Empty);
        }

        public override void Unload()
        {
            Debug.Assert(MainStage != default);
            Debug.Assert(MainWindow != default);

            Debug.Assert(_context.MainUICanvas.GetWindow() != default);

            Debug.Assert(_context.MainUICanvas.GetWindow().GetGuid() == MainWindow.GetGuid());

            MainWindow.Terminate();
            MainWindow = default;

            MainStage.Terminate();
            MainStage = default;

            _context.MainUICanvas.UnsetWindow();
        }
    }
}
