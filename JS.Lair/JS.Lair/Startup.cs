﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.Datastore.V1;
using Grpc.Auth;
using Grpc.Core;
using JS.Common;
using JS.Lair.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace JS.Lair
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            DatastoreDb db = CreateDatastoreDB();
            services.AddSingleton<IAccountStore>(new DatastoreAccountStore(db));
            services.AddSingleton<ISessionStore>(new DatastoreSessionStore(db));
            services.AddSingleton<ISerializer>(new Serializer());
        }

        private static DatastoreDb CreateDatastoreDB()
        {
            GoogleCredential cred = GoogleCredential.FromFile("/Users/stkal/Documents/keys/Lair-ff8237bd668c.json");
            if (cred.IsCreateScopedRequired)
            {
                cred = cred.CreateScoped(DatastoreClient.DefaultScopes);
            }

            var channel = new Channel(DatastoreClient.DefaultEndpoint.Host, DatastoreClient.DefaultEndpoint.Port, cred.ToChannelCredentials());
            DatastoreClient client = DatastoreClient.Create(channel);

            var db = DatastoreDb.Create("lair-225912", "", client);

            return db;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
