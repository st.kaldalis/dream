﻿using System.Net;
using JS.Common;
using JS.Lair.Models;
using Microsoft.AspNetCore.Mvc;
using req = JS.Lair.Common.Protocol.Request;
using res = JS.Lair.Common.Protocol.Response;

namespace JS.Lair.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetAccountKeyController : ControllerBase
    {
        private readonly ISessionStore _sessionStore;

        private ISerializer _serializer;

        public GetAccountKeyController(ISessionStore sessionStore, ISerializer serializer)
        {
            _serializer = serializer;
            _sessionStore = sessionStore;
        }

        [HttpPost]
        public ActionResult Post(req.GetAccountKey getAccountKey)
        {
            var session = _sessionStore.Find(getAccountKey.SessionID);

            return StatusCode((int)HttpStatusCode.OK,
                _serializer.ToJson(new res.GetAccountKey
                {
                    ResponseCode = res.ResponseCode.Success,
                    AccountKey = session.AccountKey
                }));
        }
    }
}
