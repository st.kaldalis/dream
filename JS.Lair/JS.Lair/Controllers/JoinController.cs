﻿using JS.Common;
using JS.Lair.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using req = JS.Lair.Common.Protocol.Request;
using res = JS.Lair.Common.Protocol.Response;

namespace JS.Lair.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JoinController : ControllerBase
    {
        private readonly IAccountStore _accountStore;

        private ISerializer _serializer;

        public JoinController(IAccountStore accountStore, ISerializer serializer)
        {
            _serializer = serializer;
            _accountStore = accountStore;
        }

        [HttpPost]
        public ActionResult Post(req.Join join)
        {
            var account = new Models.Account
            {
                ID = join.ID,
                Password = join.Password,
                Email = join.Email
            };

            try
            {
                _accountStore.Create(account);

                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.Join
                    {
                        ResponseCode = res.ResponseCode.Success
                    }));
            }
            catch (ArgumentException e)
            {
                if (e.ParamName == "ID")
                {
                    return StatusCode((int)HttpStatusCode.OK,
                        _serializer.ToJson(new res.Join
                        {
                            ResponseCode = res.ResponseCode.AlreadyExistID
                        }));
                }
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError,
                    _serializer.ToJson(new res.Join
                    {
                        ResponseCode = res.ResponseCode.UndefinedFailure
                    }));
            }

            // TODO 알수 없는 에러, 로깅 조치 해야 됨

            return StatusCode((int)HttpStatusCode.InternalServerError,
                _serializer.ToJson(new res.Join
                {
                    ResponseCode = res.ResponseCode.UndefinedFailure
                }));
        }
    }
}