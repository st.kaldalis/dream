﻿using System.Linq;
using System.Net;
using JS.Common;
using JS.Lair.Models;
using Microsoft.AspNetCore.Mvc;
using req = JS.Lair.Common.Protocol.Request;
using res = JS.Lair.Common.Protocol.Response;

namespace JS.Lair.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IAccountStore _accountStore;

        private readonly ISessionStore _sessionStore;

        private readonly ISerializer _serializer;

        public LoginController(IAccountStore accountStore, ISessionStore sessionStore, ISerializer serializer)
        {
            _accountStore = accountStore;
            _sessionStore = sessionStore;
            _serializer = serializer;
        }

        [HttpPost]
        public ActionResult Post(req.Login login)
        {
            var account = _accountStore.Find(login.ID);
            if (account == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.Login
                    {
                        ResponseCode = res.ResponseCode.InvalidID
                    }));
            }

            if (account.Password != login.Password)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.Login
                    {
                        ResponseCode = res.ResponseCode.InvalidPassword
                    }));
            }

            var session = _sessionStore.Find(Models.Account.ToEntity(account).Key.Path.First().Id);

            return StatusCode((int)HttpStatusCode.OK,
                _serializer.ToJson(new res.Login
                {
                    ResponseCode = res.ResponseCode.Success,
                    SessionID = session.ID
                }));
        }
    }
}