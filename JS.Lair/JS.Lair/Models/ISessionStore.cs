﻿using Google.Cloud.Datastore.V1;

namespace JS.Lair.Models
{
    public interface ISessionStore
    {
        Key Create(Session session);

        Session Find(long accountKey);

        Session Find(string id);
    }
}
