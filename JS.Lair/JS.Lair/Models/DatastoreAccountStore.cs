﻿using Google.Cloud.Datastore.V1;
using System;

namespace JS.Lair.Models
{
    public class DatastoreAccountStore : IAccountStore
    {
        private readonly DatastoreDb _db;

        private readonly KeyFactory _accountkeyFactory;
        private readonly KeyFactory _accountIDKeyFactory;

        public DatastoreAccountStore(DatastoreDb db)
        {
            _db = db;

            _accountkeyFactory = _db.CreateKeyFactory("Account");
            _accountIDKeyFactory = _db.CreateKeyFactory("AccountID");
        }

        public void Create(Account account)
        {
            var accountE = Account.ToEntity(account);
            accountE.Key = _accountkeyFactory.CreateIncompleteKey();

            var accountIDE = new Entity()
            {
                Key = _accountIDKeyFactory.CreateKey(account.ID)
            };

            using (var transaction = _db.BeginTransaction())
            {
                if (transaction.Lookup(accountIDE.Key) != null)
                {
                    throw new ArgumentException("ID already in used.", "ID");
                }

                transaction.Insert(accountIDE);
                transaction.Insert(accountE);

                transaction.Commit();
            }
        }

        public Account Find(string ID)
        {
            GqlQuery gqlQuery = new GqlQuery
            {
                QueryString = "SELECT * FROM Account WHERE ID = @ID",
                NamedBindings = { { "ID", ID } }
            };

            DatastoreQueryResults results = _db.RunQuery(gqlQuery);
            foreach (Entity entity in results.Entities)
            {
                return Account.FromEntity(entity);
            }

            return null;
        }
    }
}
