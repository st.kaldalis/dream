﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.Lair.Models
{
    public class Account
    {
        public long Key { get; set; }

        public string ID { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public static Entity ToEntity(Account account) => new Entity()
        {
            Key = new Key().WithElement("Account", account.Key),
            ["ID"] = account.ID,
            ["Password"] = account.Password,
            ["Email"] = account.Email
        };

        public static Account FromEntity(Entity entity) => new Account()
        {
            Key = entity.Key.Path.First().Id,
            ID = (string)entity["ID"],
            Password = (string)entity["Password"],
            Email = (string)entity["Email"]
        };
    }
}
