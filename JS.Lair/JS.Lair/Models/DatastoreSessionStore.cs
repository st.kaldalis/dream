﻿using Google.Cloud.Datastore.V1;
using System;
using System.Linq;

namespace JS.Lair.Models
{
    public class DatastoreSessionStore : ISessionStore
    {
        private readonly DatastoreDb _db;

        private readonly KeyFactory _keyFactory;

        public DatastoreSessionStore(DatastoreDb db)
        {
            _db = db;
            _keyFactory = _db.CreateKeyFactory("Session");
        }

        public Key Create(Session session)
        {
            var entity = Session.ToEntity(session);
            entity.Key = _keyFactory.CreateIncompleteKey();

            return _db.Insert(entity);
        }

        public Session Find(long accountKey)
        {
            // TODO 참고 하고 재 처리 할 것 https://cloud.google.com/datastore/docs/concepts/transactions

            var newID = Guid.NewGuid();

            var transaction = _db.BeginTransaction();

            // Check guid already exists.
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM Session WHERE ID = @ID",
                    NamedBindings = { { "ID", newID.ToString() } }
                };

                DatastoreQueryResults results = _db.RunQuery(gqlQuery);
                foreach (Entity entity in results.Entities)
                {
                    throw new Exception("Generate id has failed.");
                }
            }

            // Update session. If not exist than insert new one.
            {
                Session session = null;

                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM Session WHERE AccountKey = @AccountKey",
                    NamedBindings = { { "AccountKey", accountKey } }
                };

                DatastoreQueryResults results = _db.RunQuery(gqlQuery);
                foreach (Entity entity in results.Entities)
                {
                    session = Session.FromEntity(entity);
                    break;
                }

                if (session == null)
                {
                    var newKey = _keyFactory.CreateIncompleteKey();
                    session = new Session
                    {
                        Key = 0,
                        AccountKey = accountKey,
                        ID = newID.ToString()
                    };

                    var entity = Session.ToEntity(session);
                    entity.Key = _keyFactory.CreateIncompleteKey();
                    _db.Insert(entity);
                }
                else
                {
                    session.ID = newID.ToString();

                    _db.Update(Session.ToEntity(session));
                }

                transaction.Commit();

                return session;
            }
        }

        public Session Find(string id)
        {
            GqlQuery gqlQuery = new GqlQuery
            {
                QueryString = "SELECT * FROM Session WHERE ID = @ID",
                NamedBindings = { { "ID", id } }
            };

            DatastoreQueryResults results = _db.RunQuery(gqlQuery);
            foreach (Entity entity in results.Entities)
            {
                return Session.FromEntity(entity);
            }

            return null;
        }
    }
}
