﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.Lair.Models
{
    public class Session
    {
        public long Key { get; set; }

        public long AccountKey { get; set; }

        public string ID { get; set; }

        public static Entity ToEntity(Session session) => new Entity()
        {
            Key = new Key().WithElement("Session", session.Key),
            ["AccountKey"] = session.AccountKey,
            ["ID"] = session.ID
        };

        public static Session FromEntity(Entity entity) => new Session()
        {
            Key = entity.Key.Path.First().Id,
            AccountKey = (long)entity["AccountKey"],
            ID = (string)entity["ID"]
        };
    }
}
