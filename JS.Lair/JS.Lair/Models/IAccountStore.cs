﻿namespace JS.Lair.Models
{
    public interface IAccountStore
    {
        void Create(Account account);

        Account Find(string ID);
    }
}
