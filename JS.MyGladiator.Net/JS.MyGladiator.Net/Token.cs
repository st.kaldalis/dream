﻿using System;
using System.Net.Sockets;

namespace JS.MyGladiator.Net
{
    public class Token
    {
        public Session Session;

        public const int BufferSize = 256;

        public byte[] Buffer = null;

        public byte[] Stream = null;

        public EventHandler<SocketAsyncEventArgs> Completed = null;
    }
}
