﻿using System.Collections.Generic;

namespace JS.MyGladiator.Net
{
    public class TokenFactory : IFactory<Token>
    {
        public Token Create()
        {
            return new Token();
        }

        public IEnumerable<Token> Create(int count)
        {
            for (var i = 0; i < count; i++)
            {
                yield return Create();
            }
        }
    }
}
