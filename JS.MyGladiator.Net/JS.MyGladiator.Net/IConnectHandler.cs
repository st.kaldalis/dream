﻿namespace JS.MyGladiator.Net
{
    public interface IConnectHandler
    {
        void OnConnect(Session session);
    }
}
