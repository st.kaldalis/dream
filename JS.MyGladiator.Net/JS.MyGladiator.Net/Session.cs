﻿using JS.Common;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace JS.MyGladiator.Net
{
    public class Session
    {
        private enum ASYNC_REQ_RES
        {
            PENDING,
            COMPLETED,
            EXCEPTION_OCCURRED
        }

        public Socket Socket { get; protected set; }

        public Guid Guid { get; protected set; }

        protected IAcceptHandler _acceptHandler;

        protected IConnectHandler _connectHandler;

        protected IDisconnectHandler _disconnectHandler;

        protected IReceiveHandler _receiveHandler;

        protected ILogger _logger;

        protected IPacketService _packetService;

        protected IPool<SocketAsyncEventArgs> _saePool;

        protected IPool<Token> _tokenPool;

        private delegate bool AsyncReq(SocketAsyncEventArgs e);

        protected Session(ILogger logger, IPacketService packetService, IPool<SocketAsyncEventArgs> saePool, IPool<Token> tokenPool, Socket socket)
        {
            Guid = Guid.NewGuid();

            _logger = logger;
            _packetService = packetService;
            _saePool = saePool;
            _tokenPool = tokenPool;

            Socket = socket;
        }

        public Session(ILogger logger, IPacketService packetService, IPool<SocketAsyncEventArgs> saePool, IPool<Token> tokenPool, Socket socket,
            IReceiveHandler receiveHandler, IConnectHandler connectHandler, IDisconnectHandler disconnectHandler) : this(logger, packetService, saePool, tokenPool, socket)
        {
            _receiveHandler = receiveHandler;
            _connectHandler = connectHandler;
            _disconnectHandler = disconnectHandler;
        }

        public Session(ILogger logger, IPacketService packetService, IPool<SocketAsyncEventArgs> saePool, IPool<Token> tokenPool, Socket socket,
            IReceiveHandler receiveHandler, IDisconnectHandler disconnectHandler) : this(logger, packetService, saePool, tokenPool, socket)
        {
            _receiveHandler = receiveHandler;
            _disconnectHandler = disconnectHandler;
        }

        public Session(ILogger logger, IPacketService packetService, IPool<SocketAsyncEventArgs> saePool, IPool<Token> tokenPool, Socket socket,
            IAcceptHandler acceptHandler) : this(logger, packetService, saePool, tokenPool, socket)
        {
            _acceptHandler = acceptHandler;
        }

        private ASYNC_REQ_RES TryAsyncReq(AsyncReq func, SocketAsyncEventArgs sae)
        {
            var token = sae.UserToken as Token;

            try
            {
                if (func(sae))
                {
                    return ASYNC_REQ_RES.PENDING;
                }
                else
                {
                    return ASYNC_REQ_RES.COMPLETED;
                }
            }
            catch (Exception e)
            {
                sae.Completed -= token.Completed;

                _saePool.Return(sae);
                _tokenPool.Return(token);

                _logger.Fatal(e.ToString());
            }

            return ASYNC_REQ_RES.EXCEPTION_OCCURRED;
        }

        public void BeginAccept()
        {
            var token = _tokenPool.Get();
            token.Session = this;
            token.Completed = AcceptCallback;

            var sae = _saePool.Get();
            sae.UserToken = token;
            sae.Completed += token.Completed;

            if (TryAsyncReq(Socket.AcceptAsync, sae) == ASYNC_REQ_RES.COMPLETED)
            {
                AcceptCallback(Socket, sae);
            }
        }

        private void AcceptCallback(object sender, SocketAsyncEventArgs sae)
        {
            while (true)
            {
                _acceptHandler.OnAccept(sae.AcceptSocket);

                sae.AcceptSocket = null;

                var token = sae.UserToken as Token;
                if (TryAsyncReq(token.Session.Socket.AcceptAsync, sae) != ASYNC_REQ_RES.COMPLETED)
                {
                    return;
                }
            }
        }

        public void BeginConnect(IPEndPoint remoteEP)
        {
            var token = _tokenPool.Get();
            token.Session = this;
            token.Completed = ConnectCallback;

            var sae = _saePool.Get();
            sae.UserToken = token;
            sae.Completed += token.Completed;
            sae.RemoteEndPoint = remoteEP;

            if (TryAsyncReq(Socket.ConnectAsync, sae) == ASYNC_REQ_RES.COMPLETED)
            {
                ConnectCallback(Socket, sae);
            }
        }

        private void ConnectCallback(object sender, SocketAsyncEventArgs sae)
        {
            var token = sae.UserToken as Token;

            _connectHandler.OnConnect(token.Session);

            sae.Completed -= token.Completed;
            sae.RemoteEndPoint = null;

            _saePool.Return(sae);
            _tokenPool.Return(token);
        }

        public void BeginReceive()
        {
            var token = _tokenPool.Get();
            token.Session = this;
            token.Buffer = new byte[Token.BufferSize];
            token.Completed = ReceiveCallback;

            var sae = _saePool.Get();
            sae.UserToken = token;
            sae.SetBuffer(token.Buffer, 0, token.Buffer.Length);
            sae.Completed += token.Completed;

            if (TryAsyncReq(Socket.ReceiveAsync, sae) == ASYNC_REQ_RES.COMPLETED)
            {
                ReceiveCallback(Socket, sae);
            }
        }

        public void ReceiveCallback(object sender, SocketAsyncEventArgs sae)
        {
            while (true)
            {
                var token = sae.UserToken as Token;

                int bytesRead = sae.BytesTransferred;
                if (bytesRead <= 0)
                {
                    _disconnectHandler.OnDisconnect(token.Session);

                    sae.Completed -= token.Completed;

                    _saePool.Return(sae);
                    _tokenPool.Return(token);

                    return;
                }

                byte[] stream = ReceiveStream(token, bytesRead);

                stream = ProcessStream(token, stream);

                token.Stream = stream;

                if (TryAsyncReq(token.Session.Socket.ReceiveAsync, sae) != ASYNC_REQ_RES.COMPLETED)
                {
                    return;
                }
            }
        }

        private byte[] ReceiveStream(Token token, int bytesRead)
        {
            byte[] stream = null;

            if (token.Stream == null)
            {
                stream = new byte[bytesRead];

                Buffer.BlockCopy(token.Buffer, 0, stream, 0, bytesRead);
            }
            else
            {
                var streamLength = token.Stream.Length;

                stream = new byte[streamLength + bytesRead];

                Buffer.BlockCopy(token.Stream, 0, stream, 0, streamLength);
                Buffer.BlockCopy(token.Buffer, 0, stream, streamLength, bytesRead);
            }

            return stream;
        }

        private byte[] ProcessStream(Token token, byte[] stream)
        {
            while (true)
            {
                if (stream == null)
                {
                    break;
                }

                var data = _packetService.Unpack(ref stream);
                if (data == null)
                {
                    break;
                }

                _receiveHandler.OnReceive(token.Session, data);
            }

            return stream;
        }

        public void Send(byte[] dataStream)
        {
            var token = _tokenPool.Get();
            token.Session = this;
            token.Buffer = _packetService.Pack(dataStream);
            token.Completed = SendCallback;

            var sae = _saePool.Get();
            sae.UserToken = token;
            sae.SetBuffer(token.Buffer, 0, token.Buffer.Length);
            sae.Completed += token.Completed;

            if (TryAsyncReq(Socket.SendAsync, sae) == ASYNC_REQ_RES.COMPLETED)
            {
                token.Completed(Socket, sae);
            }
        }

        public void Send(byte[] dataStream, Action callback)
        {
            var token = _tokenPool.Get();
            token.Session = this;
            token.Buffer = _packetService.Pack(dataStream);
            token.Completed = (sender, args) =>
            {
                callback();
                SendCallback(sender, args);
            };

            var sae = _saePool.Get();
            sae.UserToken = token;
            sae.SetBuffer(token.Buffer, 0, token.Buffer.Length);
            sae.Completed += token.Completed;

            if (TryAsyncReq(Socket.SendAsync, sae) == ASYNC_REQ_RES.COMPLETED)
            {
                token.Completed(Socket, sae);
            }
        }

        private void SendCallback(object sender, SocketAsyncEventArgs sae)
        {
            var token = sae.UserToken as Token;

            sae.Completed -= token.Completed;

            _saePool.Return(sae);
            _tokenPool.Return(token);
        }

        public void Close()
        {
            if (!Socket.Connected)
            {
                return;
            }

            Socket.Shutdown(SocketShutdown.Both);
            Socket.Disconnect(false);
            Socket.Close();
        }
    }
}
