﻿using System.Collections.Generic;

namespace JS.MyGladiator.Net
{
    public interface IFactory<T>
    {
        T Create();

        IEnumerable<T> Create(int count);
    }
}
