﻿using System.Collections.Generic;
using System.Net.Sockets;

namespace JS.MyGladiator.Net
{
    public class SaeFactory : IFactory<SocketAsyncEventArgs>
    {
        public SocketAsyncEventArgs Create()
        {
            return new SocketAsyncEventArgs();
        }

        public IEnumerable<SocketAsyncEventArgs> Create(int count)
        {
            for (var i = 0; i < count; i++)
            {
                yield return Create();
            }
        }
    }

}
