﻿namespace JS.MyGladiator.Net
{
    public interface IDisconnectHandler
    {
        void OnDisconnect(Session session);
    }
}
