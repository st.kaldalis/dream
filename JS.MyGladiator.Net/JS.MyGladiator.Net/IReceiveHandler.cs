﻿namespace JS.MyGladiator.Net
{
    public interface IReceiveHandler
    {
        void OnReceive(Session session, byte[] data);
    }
}
