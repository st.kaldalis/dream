﻿namespace JS.MyGladiator.Net
{
    public interface IPool<T>
    {
        T Get();

        void Return(T item);
    }
}
