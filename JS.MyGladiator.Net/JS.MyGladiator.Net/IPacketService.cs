﻿namespace JS.MyGladiator.Net
{
    public interface IPacketService
    {
        byte[] Pack(byte[] data);

        byte[] Unpack(ref byte[] stream);
    }
}
