﻿using System;

namespace JS.MyGladiator.Net
{
    public class PacketService : IPacketService
    {
        public byte[] Pack(byte[] data)
        {
            // 6 = stx(1) + packet length(4) + etx(1)

            int packetLength = data.Length + 6;

            byte[] packet = new byte[packetLength];

            packet[0] = Flag.STX;

            packet[packetLength - 1] = Flag.ETX;

            byte[] number = BitConverter.GetBytes(packetLength);

            Buffer.BlockCopy(number, 0, packet, 1, 4);

            Buffer.BlockCopy(data, 0, packet, 5, data.Length);

            return packet;
        }

        public byte[] Unpack(ref byte[] stream)
        {
            var streamLength = stream.Length;
            for (int offset = 0; offset < streamLength; offset++)
            {
                if (stream[offset].Equals(Flag.STX))
                {
                    var number = new byte[4];

                    Buffer.BlockCopy(stream, offset + 1, number, 0, 4);

                    // TODO number의 값이 외부의 공격 등으로 인해 음수로 넘어 갈 경우 예외를 일으켜 주어야 한다.

                    var packetLength = BitConverter.ToInt32(number, 0);

                    var etxIndex = offset + packetLength - 1;

                    if(streamLength <= etxIndex)
                    {
                        return null;
                    }

                    if (stream[etxIndex].Equals(Flag.ETX))
                    {
                        // 6 = stx(1) + packet length(4) + etx(1)

                        var protocalLength = packetLength - 6;

                        var data = new byte[protocalLength];

                        // 5 = stx(1) + packet length(4)

                        Buffer.BlockCopy(stream, offset + 5, data, 0, protocalLength);

                        if (offset + packetLength == stream.Length)
                        {
                            stream = null;
                        }
                        else
                        {
                            var remainStreamLength = stream.Length - (offset + packetLength);

                            var remainStream = new byte[remainStreamLength];

                            Buffer.BlockCopy(stream, offset + packetLength, remainStream, 0, remainStreamLength);

                            stream = remainStream;
                        }

                        return data;
                    }
                }
            }

            return null;
        }
    }
}
