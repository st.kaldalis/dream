﻿using System.Net.Sockets;

namespace JS.MyGladiator.Net
{
    public class SaePool : IPool<SocketAsyncEventArgs>
    {
        private Pool<SocketAsyncEventArgs> pool = new Pool<SocketAsyncEventArgs>(new SaeFactory());

        public SocketAsyncEventArgs Get()
        {
            return pool.Get();
        }

        public void Return(SocketAsyncEventArgs socketAsyncEventArgs)
        {
            socketAsyncEventArgs.UserToken = null;
            socketAsyncEventArgs.SetBuffer(null, 0, 0);
            socketAsyncEventArgs.RemoteEndPoint = null;

            pool.Return(socketAsyncEventArgs);
        }
    }
}
