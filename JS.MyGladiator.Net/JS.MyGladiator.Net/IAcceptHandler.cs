﻿using System.Net.Sockets;

namespace JS.MyGladiator.Net
{
    public interface IAcceptHandler
    {
        void OnAccept(Socket socket);
    }
}
