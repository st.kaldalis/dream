﻿using System.Collections.Generic;

namespace JS.MyGladiator.Net
{
    public class Pool<T> : IPool<T>
    {
        private Stack<T> _items;

        private IFactory<T> _factory;

        public Pool(IFactory<T> factory)
        {
            _factory = factory;

            _items = new Stack<T>();
        }

        public T Get()
        {
            lock (_items)
            {
                if (_items.Count == 0)
                {
                    _items.Push(_factory.Create());
                }

                var item = _items.Pop();

                return item;
            }
        }

        public void Return(T item)
        {
            lock (_items)
            {
                _items.Push(item);
            }
        }
    }
}
