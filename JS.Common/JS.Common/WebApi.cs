﻿using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JS.Common
{
    public class WebApi
    {
        protected string _url;

        protected Serializer _serializer;

        public WebApi(string url, Serializer serializer)
        {
            _url = url;
            _serializer = serializer;
        }

        protected async Task<TRes> SendRequest<TReq, TRes>(string url, TReq req)
        {
            var webRequest = WebRequest.Create(url);

            webRequest.Method = "POST";

            webRequest.ContentType = "application/json";

            var byteArray = Encoding.UTF8.GetBytes(_serializer.ToJson(req));
            webRequest.ContentLength = byteArray.Length;
            var requestStream = webRequest.GetRequestStream();
            requestStream.Write(byteArray, 0, byteArray.Length);
            requestStream.Close();

            webRequest.Timeout = 1500;

            TRes res = default;
            using (var webResponse = await webRequest.GetResponseAsync())
            {
                using (var responseStream = webResponse.GetResponseStream())
                {
                    var memoryStream = new MemoryStream();
                    await responseStream.CopyToAsync(memoryStream);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    var reader = new StreamReader(memoryStream);
                    res = _serializer.FromJson<TRes>(reader.ReadToEnd());
                }
            }

            return res;
        }
    }
}
