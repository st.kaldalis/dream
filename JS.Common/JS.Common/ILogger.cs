﻿using System.Runtime.CompilerServices;

namespace JS.Common
{
    public interface ILogger
    {
        void Fatal(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0);
        void Info(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0);
        void Warn(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0);
    }
}
