﻿using System;

namespace JS.Common
{
    public class Time
    {
        public int GetCurrentTime()
        {
            return (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}
