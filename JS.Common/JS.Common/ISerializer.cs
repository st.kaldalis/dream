﻿namespace JS.Common
{
    public interface ISerializer
    {
        T FromJson<T>(string value);

        string ToJson<T>(T value);
    }
}
