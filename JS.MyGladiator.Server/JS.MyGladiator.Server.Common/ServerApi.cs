﻿using res = JS.MyGladiator.Server.Common.Protocol.Response;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using JS.Common;
using System.Threading.Tasks;

namespace JS.MyGladiator.Server.Common
{
    public class ServerApi : WebApi
    {
        public ServerApi(string url, Serializer serializer) : base(url, serializer)
        {
        }

        public async Task<res.Join> Join(req.Join req)
        {
            return await SendRequest<req.Join, res.Join>($"{_url}/api/join", req);
        }

        public async Task<res.Login> Login(req.Login req)
        {
            return await SendRequest<req.Login, res.Login>($"{_url}/api/login", req);
        }

        public async Task<res.GetMainGladiator> GetMainGladiator(req.GetMainGladiator req)
        {
            return await SendRequest<req.GetMainGladiator, res.GetMainGladiator>($"{_url}/api/GetMainGladiator", req);
        }

        public async Task<res.GetAllNewGladiator> GetAllNewGladiator(req.GetAllNewGladiator req)
        {
            return await SendRequest<req.GetAllNewGladiator, res.GetAllNewGladiator>($"{_url}/api/GetAllNewGladiator", req);
        }

        public async Task<res.GetAllGladiator> GetAllGladiator(req.GetAllGladiator req)
        {
            return await SendRequest<req.GetAllGladiator, res.GetAllGladiator>($"{_url}/api/GetAllGladiator", req);
        }

        public async Task<res.RegisterGladiator> RegisterGladiator(req.RegisterGladiator req)
        {
            return await SendRequest<req.RegisterGladiator, res.RegisterGladiator>($"{_url}/api/RegisterGladiator", req);
        }

        public async Task<res.SetMainGladiator> SetMainGladiator(req.SetMainGladiator req)
        {
            return await SendRequest<req.SetMainGladiator, res.SetMainGladiator>($"{_url}/api/SetMainGladiator", req);
        }
    }
}
