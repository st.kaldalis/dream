﻿namespace JS.MyGladiator.Server.Common.Protocol.Response
{
    public class GetMainGladiator : Response
    {
        public GetMainGladiator() : base()
        {
            Gladiator = default;
        }

        public MyGladiator.Common.UserData.Unit Gladiator;
    }
}
