﻿namespace JS.MyGladiator.Server.Common.Protocol.Response
{
    public enum ResponseCode
    {
        Success,

        UndefinedFailure,

        InvalidLairSessionID,
        InvalidServerSessionID,
        InvalidWritePermissionCode,
        InvalidUnitKey,

        NotFoundAccount,
        NotFoundSession,
        NotFoundWritePermissionCode,

        AlreadyExistAccount,
        AlreadyExistServerSessionID,
        AlreadyRegisteredGladiator,
        AlreadyUsedName
    }
}
