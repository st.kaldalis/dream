﻿using System.Collections.Generic;

namespace JS.MyGladiator.Server.Common.Protocol.Response
{
    public class GetAllGladiator : Response
    {
        public List<MyGladiator.Common.UserData.Unit> Gladiators;
    }
}
