﻿using JS.MyGladiator.Common.UserData;
using System.Collections.Generic;

namespace JS.MyGladiator.Server.Common.Protocol.Response
{
    public class GetAllNewGladiator : Response
    {
        public List<Unit> NewGladiators;
    }
}
