﻿using System.Runtime.Serialization;

namespace JS.MyGladiator.Server.Common.Protocol.Response
{
    public class WriteResponse : Response
    {
        [DataMember]
        public string WritePermissionCode;
    }
}
