﻿using System.Runtime.Serialization;

namespace JS.MyGladiator.Server.Common.Protocol.Request
{
    [DataContract]
    public class RegisterGladiator : WriteRequest
    {
        [DataMember]
        public long UnitKey;

        [DataMember]
        public string Name;
    }
}
