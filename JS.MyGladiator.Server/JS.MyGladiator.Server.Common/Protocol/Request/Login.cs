﻿using System.Runtime.Serialization;

namespace JS.MyGladiator.Server.Common.Protocol.Request
{
    [DataContract]
    public class Login
    {
        [DataMember]
        public string SessionID;
    }
}
