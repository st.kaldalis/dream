﻿using System.Runtime.Serialization;

namespace JS.MyGladiator.Server.Common.Protocol.Request
{
    [DataContract]
    [KnownType(typeof(GetAllGladiator))]
    [KnownType(typeof(GetAllNewGladiator))]
    [KnownType(typeof(GetMainGladiator))]
    public class Request
    {
        [DataMember]
        public string SessionID;
    }
}
