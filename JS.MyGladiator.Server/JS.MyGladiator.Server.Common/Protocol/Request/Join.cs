﻿using System.Runtime.Serialization;

namespace JS.MyGladiator.Server.Common.Protocol.Request
{
    [DataContract]
    public class Join
    {
        [DataMember]
        public string SessionID;
    }
}
