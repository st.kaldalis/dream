﻿using System.Runtime.Serialization;

namespace JS.MyGladiator.Server.Common.Protocol.Request
{
    [DataContract]
    public class SetMainGladiator : WriteRequest
    {
        [DataMember]
        public long UnitKey;
    }
}
