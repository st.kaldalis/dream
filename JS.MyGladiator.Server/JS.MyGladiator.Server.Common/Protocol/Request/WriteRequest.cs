﻿using System.Runtime.Serialization;

namespace JS.MyGladiator.Server.Common.Protocol.Request
{
    [DataContract]
    [KnownType(typeof(Join))]
    [KnownType(typeof(Login))]
    [KnownType(typeof(RegisterGladiator))]
    [KnownType(typeof(SetMainGladiator))]
    public class WriteRequest : Request
    {
        [DataMember]
        public string WritePermissionCode;
    }
}
