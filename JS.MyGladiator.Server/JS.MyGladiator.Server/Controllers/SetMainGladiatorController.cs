﻿using Microsoft.AspNetCore.Mvc;
using JS.MyGladiator.Server.Database;
using System;
using System.Net;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using res = JS.MyGladiator.Server.Common.Protocol.Response;
using JS.Common;

namespace JS.MyGladiator.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SetMainGladiatorController : ControllerBase
    {
        private readonly ICommandPool _databaseCommandPool;

        private readonly ISerializer _serializer;

        private readonly TransactionedOperationBase _transactionedOperation;

        public SetMainGladiatorController(ILogger logger, ICommandPool databaseCommandPool, ISerializer serializer)
        {
            _databaseCommandPool = databaseCommandPool;

            _serializer = serializer;

            _transactionedOperation = new GeneralTransactionedOperation(logger, (args) => { TransactionedOperation(args as TransactionedOperationArgs); });
        }

        private class TransactionedOperationArgs : GeneralTransactionedOperationArgs
        {
            public long AccountKey;

            public long UnitKey;
        }

        private void TransactionedOperation(TransactionedOperationArgs args)
        {
            var dbCmd = args.Command;
            var sessionID = args.SessionID;

            var accountKey = args.AccountKey;
            var newUnitKey = args.UnitKey;

            var mainGladiator = dbCmd.MainGladiator.Find(accountKey);
            if (mainGladiator == null)
            {
                dbCmd.MainGladiator.Create(new Models.MainGladiator()
                {
                    AccountKey = accountKey,
                    UnitKey = newUnitKey
                });
            }
            else
            {
                if (mainGladiator.UnitKey == newUnitKey)
                {
                    return;
                }

                mainGladiator.UnitKey = newUnitKey;
                dbCmd.MainGladiator.Update(mainGladiator);
            }
        }

        [HttpPost]
        public ActionResult Post(req.SetMainGladiator setMainGladiator)
        {
            var dbCmd = _databaseCommandPool.Get();

            var session = dbCmd.Session.Find(setMainGladiator.SessionID);
            if (session == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.SetMainGladiator
                    {
                        ResponseCode = res.ResponseCode.InvalidServerSessionID
                    }));
            }

            var newWritePermissionCode = Guid.NewGuid().ToString();

            try
            {
                _transactionedOperation.Execute(new TransactionedOperationArgs
                {
                    Command = dbCmd,
                    SessionID = setMainGladiator.SessionID,
                    WritePermissionCode = setMainGladiator.WritePermissionCode,
                    NewWritePermissionCode = newWritePermissionCode,

                    AccountKey = session.AccountKey,
                    UnitKey = setMainGladiator.UnitKey
                });
            }
            catch (Exception e)
            {
                if (e is ApiException)
                {
                    var apiException = e as ApiException;
                    return StatusCode((int)HttpStatusCode.OK,
                        _serializer.ToJson(new res.SetMainGladiator
                        {
                            ResponseCode = apiException.ResponseCode
                        }));
                }

                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.SetMainGladiator
                    {
                        ResponseCode = res.ResponseCode.UndefinedFailure
                    }));
            }
            finally
            {
                _databaseCommandPool.Return(dbCmd);
            }

            return StatusCode((int)HttpStatusCode.OK,
                _serializer.ToJson(new res.SetMainGladiator
                {
                    ResponseCode = res.ResponseCode.Success,
                    WritePermissionCode = newWritePermissionCode
                }));
        }
    }
}