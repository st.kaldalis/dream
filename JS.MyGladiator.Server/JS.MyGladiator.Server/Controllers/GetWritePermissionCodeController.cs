﻿using Microsoft.AspNetCore.Mvc;
using JS.MyGladiator.Server.Database;
using System.Net;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using res = JS.MyGladiator.Server.Common.Protocol.Response;
using JS.Common;

namespace JS.MyGladiator.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetWritePermissionCodeController : ControllerBase
    {
        private readonly ICommandPool _databaseCommandPool;

        private readonly ISerializer _serializer;

        private readonly TransactionedOperationBase _transactionedOperation;

        public GetWritePermissionCodeController(ILogger logger, ICommandPool databaseCommandPool, ISerializer serializer)
        {
            _databaseCommandPool = databaseCommandPool;

            _serializer = serializer;

            _transactionedOperation = new GeneralTransactionedOperation(logger);
        }

        [HttpPost]
        public ActionResult Post(req.GetWritePermissionCode getWritePermissionCode)
        {
            var dbCmd = _databaseCommandPool.Get();

            var session = dbCmd.Session.Find(getWritePermissionCode.SessionID);
            if (session == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.GetWritePermissionCode
                    {
                        ResponseCode = res.ResponseCode.InvalidServerSessionID
                    }));
            }

            var code = dbCmd.WritePermissionCode.Find(session.AccountKey);
            if (code == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.GetWritePermissionCode
                    {
                        ResponseCode = res.ResponseCode.NotFoundWritePermissionCode
                    }));
            }

            return StatusCode((int)HttpStatusCode.OK,
                _serializer.ToJson(new res.GetWritePermissionCode
                {
                    ResponseCode = res.ResponseCode.Success,
                    Code = code.Code
                }));
        }
    }
}