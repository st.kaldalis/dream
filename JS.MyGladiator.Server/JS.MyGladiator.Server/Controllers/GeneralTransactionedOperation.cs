﻿using JS.MyGladiator.Server.Database;
using System;
using JS.MyGladiator.Server.Common.Protocol.Response;
using JS.Common;

namespace JS.MyGladiator.Server.Controllers
{
    public class GeneralTransactionedOperation : TransactionedOperationBase
    {
        protected Action<GeneralTransactionedOperationArgs> _action;

        public GeneralTransactionedOperation(ILogger logger) : base(logger)
        {
            _action = (args) => { };
        }

        public GeneralTransactionedOperation(ILogger logger, Action<GeneralTransactionedOperationArgs> action) : base(logger)
        {
            _action = action;
        }

        protected override void Operation(TransactionedOperationArgs args)
        {
            Operation(args as GeneralTransactionedOperationArgs);
        }

        private void Operation(GeneralTransactionedOperationArgs args)
        {
            _action(args);

            var command = args.Command;
            var sessionID = args.SessionID;

            var session = command.Session.Find(sessionID);
            if (session == null)
            {
                throw new ApiException(ResponseCode.InvalidServerSessionID);
            }

            var code = args.WritePermissionCode;
            var writePermissionCode = command.WritePermissionCode.Find(session.AccountKey);
            if (writePermissionCode == null || writePermissionCode.Code != code)
            {
                throw new ApiException(ResponseCode.InvalidWritePermissionCode);
            }
            writePermissionCode.Code = args.NewWritePermissionCode;

            command.WritePermissionCode.Update(writePermissionCode);
        }
    }
}
