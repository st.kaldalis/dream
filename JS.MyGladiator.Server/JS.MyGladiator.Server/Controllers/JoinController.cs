﻿using Microsoft.AspNetCore.Mvc;
using JS.MyGladiator.Server.Common.Protocol.Response;
using JS.MyGladiator.Server.Database;
using JS.MyGladiator.Server.Models;
using System;
using System.Collections.Generic;
using System.Net;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using JS.Common;
using JS.Lair.Common;
using System.Threading.Tasks;

namespace JS.MyGladiator.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JoinController : ControllerBase
    {
        private readonly ILogger _logger;

        private readonly ICommandPool _commandPool;

        private readonly LairApi _lairApi;

        private readonly ISerializer _serializer;

        private readonly TransactionedOperationBase _transactionedOperation;

        public JoinController(ILogger logger, ICommandPool commandPool, LairApi lairApi, ISerializer serializer)
        {
            _logger = logger;
            _commandPool = commandPool;
            _lairApi = lairApi;
            _serializer = serializer;
            _transactionedOperation = new TransactionedOperation(logger, (args) => { TransactionedOperation(args as TransactionedOperationArgs); });
        }

        private class TransactionedOperationArgs : Database.TransactionedOperationArgs
        {
            public Models.Account Account;

            public WritePermissionCode WritePermissionCode;

            public Session Session;

            public List<Unit> Units;

            public List<UnitStatus> UnitStatuses;

            public Dictionary<int, List<UnitAttachment>> UnitAttachmentMap;

            public List<NewGladiator> NewGladiators;
        }

        private void TransactionedOperation(TransactionedOperationArgs args)
        {
            var dbCmd = args.Command;
            var account = args.Account;
            var writePermissionCode = args.WritePermissionCode;
            var session = args.Session;
            var units = args.Units;
            var unitStatuses = args.UnitStatuses;
            var unitAttachmentMap = args.UnitAttachmentMap;
            var newGladiators = args.NewGladiators;

            if (dbCmd.Account.Find(account.LairAccountKey) != null)
            {
                throw new ApiException(ResponseCode.AlreadyExistAccount);
            }

            var accountKey = dbCmd.Account.Create(account);

            session.AccountKey = accountKey;
            dbCmd.Session.Create(session);

            writePermissionCode.AccountKey = accountKey;
            dbCmd.WritePermissionCode.Create(writePermissionCode);

            var unitCount = units.Count;

            var unitKeys = new Dictionary<int, long>();
            for (int i = 0; i < unitCount; i++)
            {
                unitKeys[i] = dbCmd.Unit.Create(units[i]);
            }

            for (int i = 0; i < unitCount; i++)
            {
                var unitStatus = unitStatuses[i];
                unitStatus.UnitKey = unitKeys[i];

                dbCmd.UnitStatus.Create(unitStatus);
            }

            for (int i = 0; i < unitCount; i++)
            {
                var unitKey = unitKeys[i];
                var unitAttachments = unitAttachmentMap[i];
                foreach (var unitAttachment in unitAttachments)
                {
                    unitAttachment.UnitKey = unitKey;

                    dbCmd.UnitAttachment.Create(unitAttachment);
                }
            }

            for (int i = 0; i < unitCount; i++)
            {
                var newGladiator = newGladiators[i];
                newGladiator.AccountKey = accountKey;
                newGladiator.UnitKey = unitKeys[i];

                dbCmd.NewGladiator.Create(newGladiator);
            }
        }

        [HttpPost]
        public async Task<ActionResult> PostAsync(req.Join join)
        {
            var getAccountKey = await _lairApi.GetAccountKey(new Lair.Common.Protocol.Request.GetAccountKey { SessionID = join.SessionID });
            if (getAccountKey.ResponseCode != Lair.Common.Protocol.Response.ResponseCode.Success)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new Join
                    {
                        ResponseCode = ResponseCode.InvalidLairSessionID
                    }));
            }
            var accountKey = getAccountKey.AccountKey;

            var account = new Models.Account
            {
                LairAccountKey = accountKey
            };

            var writePermissionCode = new WritePermissionCode
            {
                Code = Guid.NewGuid().ToString()
            };

            var session = new Session
            {
                ID = string.Empty,
                ExpireTime = 0
            };

            var units = new List<Unit>();
            for (var i = 0; i < 3; i++)
            {
                var unit = new Unit()
                {
                    ID = "GLADIATOR_00",
                    Level = 1
                };
                units.Add(unit);
            }

            var random = new Random();

            var unitStatuses = new List<UnitStatus>();
            for (var i = 0; i < 3; i++)
            {
                var status = new int[4] { 0, 0, 0, 0 };
                var totalStatus = 10;
                for (var j = 0; j < totalStatus; j++)
                {
                    status[random.Next(0, 4)]++;
                }

                var unitStatus = new UnitStatus()
                {
                    Strength = status[0],
                    Dexterity = status[1],
                    Vitality = status[2],
                    Luck = status[3]
                };
                unitStatuses.Add(unitStatus);
            }

            var unitAttachmentMap = new Dictionary<int, List<UnitAttachment>>();
            for (var i = 0; i < 3; i++)
            {
                var unitAttachments = new List<UnitAttachment>();
                unitAttachmentMap[i] = unitAttachments;

                // TODO 이하는 테스트 용이니까 대체 되어야 함
                if (random.Next(0, 99) < 50)
                {
                    var unitAttachment = new UnitAttachment()
                    {
                        Type = "visor",
                        ID = random.Next(0, 99) < 50 ? "visor_000" : "visor_001"
                    };

                    unitAttachments.Add(unitAttachment);
                }

                if (random.Next(0, 99) < 50)
                {
                    var unitAttachmentE = new UnitAttachment()
                    {
                        Type = "gun",
                        ID = random.Next(0, 99) < 50 ? "gun_000" : "gun_001"
                    };
                    unitAttachments.Add(unitAttachmentE);
                }
            }

            var newGladiators = new List<NewGladiator>();
            for (var i = 0; i < 3; i++)
            {
                var newGladiator = new NewGladiator() { };
                newGladiators.Add(newGladiator);
            }

            var dbCmd = _commandPool.Get();

            try
            {
                _transactionedOperation.Execute(new TransactionedOperationArgs
                {
                    Command = dbCmd,
                    Account = account,
                    WritePermissionCode = writePermissionCode,
                    Session = session,
                    Units = units,
                    UnitStatuses = unitStatuses,
                    UnitAttachmentMap = unitAttachmentMap,
                    NewGladiators = newGladiators
                });
            }
            catch (Exception e)
            {
                if (e is ApiException)
                {
                    var apiException = e as ApiException;
                    return StatusCode((int)HttpStatusCode.OK,
                        _serializer.ToJson(new Join
                        {
                            ResponseCode = apiException.ResponseCode
                        }));
                }

                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new Join
                    {
                        ResponseCode = ResponseCode.UndefinedFailure
                    }));
            }
            finally
            {
                _commandPool.Return(dbCmd);
            }

            return StatusCode((int)HttpStatusCode.OK,
                _serializer.ToJson(new Join
                {
                    ResponseCode = ResponseCode.Success
                }));
        }
    }
}