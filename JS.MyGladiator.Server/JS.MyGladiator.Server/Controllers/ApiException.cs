﻿using JS.MyGladiator.Server.Common.Protocol.Response;
using System;
using System.Net;

namespace JS.MyGladiator.Server.Controllers
{
    public class ApiException : Exception
    {
        public ApiException(ResponseCode code)
        {
            ResponseCode = code;
        }

        public ResponseCode ResponseCode { protected set; get; }
    }
}
