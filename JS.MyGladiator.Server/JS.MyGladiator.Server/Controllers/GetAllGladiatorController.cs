﻿using Microsoft.AspNetCore.Mvc;
using JS.MyGladiator.Server.Database;
using System.Collections.Generic;
using System.Net;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using res = JS.MyGladiator.Server.Common.Protocol.Response;
using UserData = JS.MyGladiator.Common.UserData;
using JS.Common;

namespace JS.MyGladiator.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetAllGladiatorController : ControllerBase
    {
        private readonly ICommandPool _databaseCommandPool;

        private readonly ISerializer _serializer;

        private readonly TransactionedOperationBase _transactionedOperation;

        public GetAllGladiatorController(ILogger logger, ICommandPool databaseCommandPool, ISerializer serializer)
        {
            _databaseCommandPool = databaseCommandPool;

            _serializer = serializer;

            _transactionedOperation = new GeneralTransactionedOperation(logger);
        }

        [HttpPost]
        public ActionResult Post(req.GetAllGladiator getAllGladiator)
        {
            var dbCmd = _databaseCommandPool.Get();

            var session = dbCmd.Session.Find(getAllGladiator.SessionID);

            var gladiators = dbCmd.Gladiator.FindAll(session.AccountKey);

            var sGladiators = new List<UserData.Unit>();
            foreach (var newGladiator in gladiators)
            {
                var unit = dbCmd.Unit.Find(newGladiator.UnitKey);
                var unitStatus = dbCmd.UnitStatus.Find(unit.Key);
                var unitAttachments = dbCmd.UnitAttachment.FindAll(unit.Key);

                var sGladiator = new UserData.Unit
                {
                    Key = unit.Key,
                    UnitID = unit.ID,
                    Name = unit.Name,
                    Level = unit.Level,
                    Status = null,
                    Attachments = null
                };

                if (unitStatus != null)
                {
                    var status = new UserData.Status()
                    {
                        Dexterity = unitStatus.Dexterity,
                        Vitality = unitStatus.Vitality,
                        Luck = unitStatus.Luck,
                        Strength = unitStatus.Strength
                    };

                    sGladiator.Status = status;
                }

                if (unitAttachments != null)
                {
                    var attachments = new List<UserData.Attachment>();
                    foreach (var newUnitAttachment in unitAttachments)
                    {
                        attachments.Add(new UserData.Attachment()
                        {
                            Type = newUnitAttachment.Type,
                            ID = newUnitAttachment.ID
                        });
                    }

                    sGladiator.Attachments = attachments;
                }

                sGladiators.Add(sGladiator);
            }

            return StatusCode((int)HttpStatusCode.OK,
                _serializer.ToJson(new res.GetAllGladiator
                {
                    ResponseCode = res.ResponseCode.Success,
                    Gladiators = sGladiators
                }));
        }
    }
}