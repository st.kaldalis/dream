﻿using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using JS.MyGladiator.Server.Database;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using res = JS.MyGladiator.Server.Common.Protocol.Response;
using UserData = JS.MyGladiator.Common.UserData;
using JS.Common;

namespace JS.MyGladiator.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetMainGladiatorController : ControllerBase
    {
        private readonly ICommandPool _databaseCommandPool;

        private readonly ISerializer _serializer;

        private readonly TransactionedOperationBase _transactionedOperation;

        public GetMainGladiatorController(ILogger logger, ICommandPool databaseCommandPool, ISerializer serializer)
        {
            _databaseCommandPool = databaseCommandPool;

            _serializer = serializer;

            _transactionedOperation = new GeneralTransactionedOperation(logger);
        }

        [HttpPost]
        public ActionResult Post(req.GetMainGladiator getAllGladiator)
        {
            var dbCmd = _databaseCommandPool.Get();

            var session = dbCmd.Session.Find(getAllGladiator.SessionID);
            if (session == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.GetMainGladiator
                    {
                        ResponseCode = res.ResponseCode.InvalidServerSessionID
                    }));
            }

            var mainGladiator = dbCmd.MainGladiator.Find(session.AccountKey);
            if (mainGladiator == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.GetMainGladiator
                    {
                        ResponseCode = res.ResponseCode.Success,
                        Gladiator = null
                    }));
            }

            var unit = dbCmd.Unit.Find(mainGladiator.UnitKey);
            var unitStatus = dbCmd.UnitStatus.Find(unit.Key);
            var unitAttachments = dbCmd.UnitAttachment.FindAll(unit.Key);

            var sGladiator = new UserData.Unit
            {
                UnitID = unit.ID,
                Name = unit.Name,
                Level = unit.Level,
                Status = null,
                Attachments = null
            };

            if (unitStatus != null)
            {
                var status = new UserData.Status()
                {
                    Dexterity = unitStatus.Dexterity,
                    Vitality = unitStatus.Vitality,
                    Luck = unitStatus.Luck,
                    Strength = unitStatus.Strength
                };

                sGladiator.Status = status;
            }

            if (unitAttachments != null)
            {
                var attachments = new List<UserData.Attachment>();
                foreach (var newUnitAttachment in unitAttachments)
                {
                    attachments.Add(new UserData.Attachment()
                    {
                        Type = newUnitAttachment.Type,
                        ID = newUnitAttachment.ID
                    });
                }

                sGladiator.Attachments = attachments;
            }

            return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.GetMainGladiator
                    {
                        ResponseCode = res.ResponseCode.Success,
                        Gladiator = sGladiator
                    }));
        }
    }
}