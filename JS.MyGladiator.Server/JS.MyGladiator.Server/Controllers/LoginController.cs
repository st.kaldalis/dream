﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using JS.MyGladiator.Server.Database;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using res = JS.MyGladiator.Server.Common.Protocol.Response;
using JS.Lair.Common;
using JS.Common;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JS.MyGladiator.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly int SESSION_DURATION;

        private readonly ICommandPool _commandPool;

        private readonly LairApi _lairApi;

        private readonly ISerializer _serializer;

        private readonly Time _time;

        private ILogger _logger;

        private readonly TransactionedOperationBase _transactionedOperation;

        public LoginController(ILogger logger, ICommandPool commandPool, LairApi lairApi, ISerializer serializer, Time time)
        {
            SESSION_DURATION = 24 * 60 * 60;

            _commandPool = commandPool;
            _lairApi = lairApi;
            _serializer = serializer;
            _time = time;
            _logger = logger;
            _transactionedOperation = new TransactionedOperation(logger, (args) => { TransactionedOperation(args as TransactionedOperationArgs); });
        }

        private class TransactionedOperationArgs : Database.TransactionedOperationArgs
        {
            public Models.Session Session;
        }

        private void TransactionedOperation(TransactionedOperationArgs args)
        {
            var dbCmd = args.Command;
            var session = args.Session;

            if (dbCmd.Session.Find(session.ID) != null)
            {
                _logger.Warn("Try to use already exist session id.");
                throw new ApiException(res.ResponseCode.AlreadyExistServerSessionID);
            }

            dbCmd.Session.Update(session);
        }

        [HttpPost]
        public async Task<ActionResult> PostAsync(req.Login login)
        {
            var getAccountKey = await _lairApi.GetAccountKey(new Lair.Common.Protocol.Request.GetAccountKey { SessionID = login.SessionID });
            if (getAccountKey.ResponseCode != Lair.Common.Protocol.Response.ResponseCode.Success)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.Login
                    {
                        ResponseCode = res.ResponseCode.InvalidLairSessionID
                    }));
            }
            var accountKey = getAccountKey.AccountKey;

            var dbCmd = _commandPool.Get();

            var account = dbCmd.Account.Find(accountKey);
            if (account == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.Login
                    {
                        ResponseCode = res.ResponseCode.NotFoundAccount
                    }));
            }

            var session = dbCmd.Session.Find(account.Key);
            if (session == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.Login
                    {
                        ResponseCode = res.ResponseCode.NotFoundSession
                    }));
            }

            if (session.ExpireTime < _time.GetCurrentTime())
            {
                session.ExpireTime = _time.GetCurrentTime() + SESSION_DURATION;
                session.ID = Guid.NewGuid().ToString();

                try
                {
                    _transactionedOperation.Execute(new TransactionedOperationArgs { Command = dbCmd, Session = session });
                }
                catch (Exception e)
                {
                    if (e is ApiException)
                    {
                        var apiException = e as ApiException;
                        return StatusCode((int)HttpStatusCode.OK,
                            _serializer.ToJson(new res.Login
                            {
                                ResponseCode = apiException.ResponseCode
                            }));
                    }

                    return StatusCode((int)HttpStatusCode.OK,
                        _serializer.ToJson(new res.Login
                        {
                            ResponseCode = res.ResponseCode.UndefinedFailure
                        }));
                }
                finally
                {
                    _commandPool.Return(dbCmd);
                }
            }

            var code = dbCmd.WritePermissionCode.Find(session.AccountKey);
            if (code == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.Login
                    {
                        ResponseCode = res.ResponseCode.NotFoundWritePermissionCode
                    }));
            }

            return StatusCode((int)HttpStatusCode.OK,
                _serializer.ToJson(new res.Login
                {
                    ResponseCode = res.ResponseCode.Success,
                    SessionID = session.ID,
                    WritePermissionCode = code.Code
                }));
        }
    }
}
