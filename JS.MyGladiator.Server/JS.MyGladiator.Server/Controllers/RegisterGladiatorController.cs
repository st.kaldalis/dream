﻿using Microsoft.AspNetCore.Mvc;
using JS.MyGladiator.Server.Common.Protocol.Response;
using JS.MyGladiator.Server.Database;
using JS.MyGladiator.Server.Models;
using System;
using System.Net;
using req = JS.MyGladiator.Server.Common.Protocol.Request;
using res = JS.MyGladiator.Server.Common.Protocol.Response;
using JS.Common;

namespace JS.MyGladiator.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterGladiatorController : ControllerBase
    {
        private ILogger _logger;

        private readonly ICommandPool _databaseCommandPool;

        private readonly ISerializer _serializer;

        private readonly TransactionedOperationBase _transactionedOperation;

        public RegisterGladiatorController(ILogger logger, ICommandPool databaseCommandPool, ISerializer serializer)
        {
            _logger = logger;

            _databaseCommandPool = databaseCommandPool;

            _serializer = serializer;

            _transactionedOperation = new GeneralTransactionedOperation(logger, (args) => { TransactionedOperation(args as TransactionedOperationArgs); });
        }

        private class TransactionedOperationArgs : GeneralTransactionedOperationArgs
        {
            public long AccountKey;

            public Unit Unit;
        }

        private void TransactionedOperation(TransactionedOperationArgs args)
        {
            var dbCmd = args.Command;

            var accountKey = args.AccountKey;

            var unit = args.Unit;
            var unitKey = unit.Key;

            var newGladiators = dbCmd.NewGladiator.FindAll(accountKey);
            var newGladiator = newGladiators.Find((item) => { return item.UnitKey == unitKey; });
            if (newGladiator == null)
            {
                _logger.Warn($"Cannet find unit in new gladiator list. AccountKey : {accountKey}, UnitKey : {unitKey}");
                throw new ApiException(ResponseCode.InvalidUnitKey);
            }

            var gladiator = dbCmd.Gladiator.Find(accountKey, unitKey);
            if (gladiator != null)
            {
                _logger.Warn($"Try to register already registered gladiator. AccountKey : {accountKey}, UnitKey : {unitKey}");
                throw new ApiException(ResponseCode.AlreadyRegisteredGladiator);
            }

            if (dbCmd.GladiatorName.Find(unit.Name) != null)
            {
                throw new ApiException(ResponseCode.AlreadyUsedName);
            }

            dbCmd.GladiatorName.Create(new GladiatorName { Name = unit.Name });

            dbCmd.Unit.Update(unit);

            dbCmd.Gladiator.Create(new Gladiator
            {
                AccountKey = accountKey,
                UnitKey = unitKey
            });

            dbCmd.NewGladiator.Delete(newGladiator.Key);
        }

        [HttpPost]
        public ActionResult Post(req.RegisterGladiator registerGladiator)
        {
            var dbCmd = _databaseCommandPool.Get();

            var sessionID = registerGladiator.SessionID;
            var writePermissionCode = registerGladiator.WritePermissionCode;
            var unitKey = registerGladiator.UnitKey;
            var unitName = registerGladiator.Name;

            var session = dbCmd.Session.Find(sessionID);
            if (session == null)
            {
                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.RegisterGladiator
                    {
                        ResponseCode = res.ResponseCode.InvalidServerSessionID
                    }));
            }

            var accountKey = session.AccountKey;

            Unit unit = null;
            try
            {
                unit = dbCmd.Unit.Find(unitKey);
                if (unit == null)
                {
                    _logger.Warn($"Try to resgister gladiator with invalid unit key. AccountID : {accountKey}, UnitKey : {unitKey}");

                    _databaseCommandPool.Return(dbCmd);

                    return StatusCode((int)HttpStatusCode.OK,
                        _serializer.ToJson(new res.RegisterGladiator
                        {
                            ResponseCode = res.ResponseCode.InvalidUnitKey
                        }));
                }

                unit.Name = unitName;
            }
            catch (Exception e)
            {
                _logger.Fatal($"Message : {e.Message}\nStackTrace : {e.StackTrace}");

                _databaseCommandPool.Return(dbCmd);

                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.RegisterGladiator
                    {
                        ResponseCode = res.ResponseCode.UndefinedFailure
                    }));
            }

            var newWritePermissionCode = Guid.NewGuid().ToString();

            try
            {
                _transactionedOperation.Execute(new TransactionedOperationArgs
                {
                    SessionID = sessionID,
                    WritePermissionCode = writePermissionCode,
                    NewWritePermissionCode = newWritePermissionCode,
                    Command = dbCmd,

                    AccountKey = accountKey,
                    Unit = unit
                });
            }
            catch (Exception e)
            {
                if (e is ApiException)
                {
                    var apiException = e as ApiException;
                    return StatusCode((int)HttpStatusCode.OK,
                        _serializer.ToJson(new res.RegisterGladiator
                        {
                            ResponseCode = apiException.ResponseCode
                        }));
                }

                return StatusCode((int)HttpStatusCode.OK,
                    _serializer.ToJson(new res.RegisterGladiator
                    {
                        ResponseCode = res.ResponseCode.UndefinedFailure
                    }));
            }
            finally
            {
                _databaseCommandPool.Return(dbCmd);
            }

            return StatusCode((int)HttpStatusCode.OK,
                _serializer.ToJson(new res.RegisterGladiator
                {
                    ResponseCode = res.ResponseCode.Success,
                    WritePermissionCode = newWritePermissionCode
        }));
        }
}
}