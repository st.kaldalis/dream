﻿using JS.MyGladiator.Server.Database;

namespace JS.MyGladiator.Server.Controllers
{
    public class GeneralTransactionedOperationArgs : TransactionedOperationArgs
    {
        public string SessionID;

        public string WritePermissionCode;

        public string NewWritePermissionCode;
    }
}
