﻿using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface IAccount
        {
            long Create(Account account);

            Account Find(long lairAccountKey);
        }
    }
}
