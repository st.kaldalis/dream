﻿using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface IMainGladiator
        {
            MainGladiator Find(long accountKey);

            long Create(MainGladiator mainGladiator);

            void Update(MainGladiator mainGladiator);
        }
    }
}
