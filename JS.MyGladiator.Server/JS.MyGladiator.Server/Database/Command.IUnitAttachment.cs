﻿using JS.MyGladiator.Server.Models;
using System.Collections.Generic;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface IUnitAttachment
        {
            List<UnitAttachment> FindAll(long unitKey);

            void Create(UnitAttachment unitAttachment);
        }
    }
}
