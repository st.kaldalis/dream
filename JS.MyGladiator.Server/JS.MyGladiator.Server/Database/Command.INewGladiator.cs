﻿using JS.MyGladiator.Server.Models;
using System.Collections.Generic;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface INewGladiator
        {
            List<NewGladiator> FindAll(long accountKey);
            void Create(NewGladiator newGladiator);
            void Delete(long key);
        }
    }
}
