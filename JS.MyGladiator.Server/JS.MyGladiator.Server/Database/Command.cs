﻿namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public IAccount Account;

        public IWritePermissionCode WritePermissionCode;

        public IGladiatorName GladiatorName;

        public IGladiator Gladiator;

        public IMainGladiator MainGladiator;

        public INewGladiator NewGladiator;

        public ISession Session;

        public IUnitAttachment UnitAttachment;

        public IUnitStatus UnitStatus;

        public IUnit Unit;

        public abstract void BeginTransaction();

        public abstract void CommitTransaction();

        public abstract void RollbackTransaction();
    }
}
