﻿using JS.Common;
using System;

namespace JS.MyGladiator.Server.Database
{
    public abstract class TransactionedOperationBase
    {
        protected ILogger _logger;

        public TransactionedOperationBase(ILogger logger)
        {
            _logger = logger;
        }

        public void Execute(TransactionedOperationArgs args)
        {
            var command = args.Command;

            try
            {
                command.BeginTransaction();

                Operation(args);

                command.CommitTransaction();
            }
            catch (Exception e)
            {
                try
                {
                    command.RollbackTransaction();
                }
                catch (Exception rollbackE)
                {
                    _logger.Fatal(rollbackE.Message);
                }

                _logger.Warn(e.Message);
                throw e;
            }
        }

        protected abstract void Operation(TransactionedOperationArgs args);
    }
}
