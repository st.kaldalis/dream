﻿using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface IGladiatorName
        {
            GladiatorName Find(string name);

            void Create(GladiatorName gladiatorName);
        }
    }
}
