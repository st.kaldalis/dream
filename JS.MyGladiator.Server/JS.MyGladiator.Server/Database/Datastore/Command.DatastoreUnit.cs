﻿using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;
using System.Linq;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreUnit : DatastoreTable, IUnit
        {
            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("Unit");
            }

            public Unit Find(long unitKey)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM Unit WHERE __key__ = @key",
                    NamedBindings = { { "key", _keyFactory.CreateKey(unitKey) } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                foreach (Entity entity in results.Entities)
                {
                    return Models.Unit.FromEntity(entity);
                }

                return null;
            }

            public long Create(Unit unit)
            {
                var e = Models.Unit.ToEntity(unit);
                e.Key = _db.AllocateId(_keyFactory.CreateIncompleteKey());

                Insert(e);

                return e.Key.Path.First().Id;
            }

            public void Update(Unit unit)
            {
                Update(Models.Unit.ToEntity(unit));
            }
        }
    }
}
