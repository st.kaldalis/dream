﻿using System.Linq;
using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreAccount : DatastoreTable, IAccount
        {
            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("Account");
            }

            public long Create(Account account)
            {
                var accountE = Models.Account.ToEntity(account);
                accountE.Key = _db.AllocateId(_keyFactory.CreateIncompleteKey());

                Insert(accountE);

                return accountE.Key.Path.First().Id;
            }

            public Account Find(long lairAccountKey)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM Account WHERE LairAccountKey = @LairAccountKey",
                    NamedBindings = { { "LairAccountKey", lairAccountKey } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                foreach (Entity entity in results.Entities)
                {
                    return Models.Account.FromEntity(entity);
                }

                return null;
            }
        }
    }
}
