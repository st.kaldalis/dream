﻿using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreSession : DatastoreTable, ISession
        {
            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("Session");
            }

            public void Create(Session session)
            {
                var sessionE = Models.Session.ToEntity(session);
                sessionE.Key = _keyFactory.CreateIncompleteKey();

                Insert(sessionE);

                return;
            }

            public Session Find(long accountKey)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM Session WHERE AccountKey = @AccountKey",
                    NamedBindings = { { "AccountKey", accountKey } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                foreach (Entity entity in results.Entities)
                {
                    return Models.Session.FromEntity(entity);
                }

                return null;
            }

            public Session Find(string id)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM Session WHERE ID = @ID",
                    NamedBindings = { { "ID", id } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                foreach (Entity entity in results.Entities)
                {
                    return Models.Session.FromEntity(entity);
                }

                return null;
            }

            public bool Update(Session session)
            {
                var sessionE = Models.Session.ToEntity(session);

                if (_transaction == null)
                {
                    _db.Update(sessionE);
                }
                else
                {
                    _transaction.Update(sessionE);
                }

                return true;
            }
        }
    }
}
