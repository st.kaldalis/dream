﻿using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreGladiatorName : DatastoreTable, IGladiatorName
        {
            public void Create(GladiatorName gladiatorName)
            {
                var e = Models.GladiatorName.ToEntity(gladiatorName);
                e.Key = _keyFactory.CreateIncompleteKey();

                Insert(e);
            }

            public GladiatorName Find(string name)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM GladiatorName WHERE Name = @Name",
                    NamedBindings = { { "Name", name } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                foreach (Entity entity in results.Entities)
                {
                    return Models.GladiatorName.FromEntity(entity);
                }

                return null;
            }

            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("GladiatorName");
            }
        }
    }
}
