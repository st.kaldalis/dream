﻿using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreWritePermissionCode : DatastoreTable, IWritePermissionCode
        {
            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("WritePermissionCode");
            }

            public void Create(WritePermissionCode wpc)
            {
                var wpcE = Models.WritePermissionCode.ToEntity(wpc);
                wpcE.Key = _keyFactory.CreateIncompleteKey();

                Insert(wpcE);

                return;
            }

            public WritePermissionCode Find(long accountKey)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM WritePermissionCode WHERE AccountKey = @AccountKey",
                    NamedBindings = { { "AccountKey", accountKey } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                foreach (Entity entity in results.Entities)
                {
                    return Models.WritePermissionCode.FromEntity(entity);
                }

                return null;
            }

            public bool Update(WritePermissionCode wpc)
            {
                var macE = Models.WritePermissionCode.ToEntity(wpc);

                if (_transaction == null)
                {
                    _db.Update(macE);
                }
                else
                {
                    _transaction.Update(macE);
                }

                return true;
            }
        }
    }
}
