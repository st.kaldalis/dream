﻿using Google.Cloud.Datastore.V1;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreTable
        {
            protected DatastoreDb _db;

            protected DatastoreTransaction _transaction;

            protected KeyFactory _keyFactory;

            public virtual void Init(DatastoreDb db)
            {
                _db = db;
            }

            public void SetTransaction(DatastoreTransaction transaction)
            {
                _transaction = transaction;
            }

            public void UnsetTransaction()
            {
                _transaction = null;
            }

            public void Init()
            {
                _db = null;

                _transaction = null;

                _keyFactory = null;
            }

            protected DatastoreQueryResults RunQuery(GqlQuery gqlQuery)
            {
                if (_transaction == null)
                {
                    return _db.RunQuery(gqlQuery);
                }

                return _transaction.RunQuery(gqlQuery);
            }

            protected void Insert(Entity entity)
            {
                if (_transaction == null)
                {
                    _db.Insert(entity);
                }
                else
                {
                    _transaction.Insert(entity);
                }
            }

            protected void Update(Entity entity)
            {
                if (_transaction == null)
                {
                    _db.Update(entity);
                }
                else
                {
                    _transaction.Update(entity);
                }
            }

            protected void Delete(Key key)
            {
                if (_transaction == null)
                {
                    _db.Delete(key);
                }
                else
                {
                    _transaction.Delete(key);
                }
            }
        }
    }
}