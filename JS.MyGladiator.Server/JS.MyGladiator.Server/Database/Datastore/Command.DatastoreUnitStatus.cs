﻿using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreUnitStatus : DatastoreTable, IUnitStatus
        {
            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("UnitStatus");
            }

            public void Create(UnitStatus unitStatus)
            {
                var e = Models.UnitStatus.ToEntity(unitStatus);
                e.Key = _keyFactory.CreateIncompleteKey();

                Insert(e);
            }

            public UnitStatus Find(long unitKey)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM UnitStatus WHERE UnitKey = @UnitKey",
                    NamedBindings = { { "UnitKey", unitKey } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                foreach (Entity entity in results.Entities)
                {
                    return Models.UnitStatus.FromEntity(entity);
                }

                return null;
            }
        }
    }
}
