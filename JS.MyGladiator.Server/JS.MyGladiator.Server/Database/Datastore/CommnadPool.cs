﻿using Google.Cloud.Datastore.V1;
using System.Collections.Concurrent;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public class CommnadPool : ICommandPool
    {
        private ConcurrentStack<Command> _commands;

        private readonly DatastoreDb _db;

        public CommnadPool(DatastoreDb db)
        {
            _commands = new ConcurrentStack<Command>();

            _db = db;
        }

        public Database.Command Get()
        {
            if (_commands.TryPop(out Command cmd))
            {
                cmd.Init();
                cmd.Init(_db);

                return cmd;
            }

            cmd = new Command();
            cmd.Init(_db);

            return cmd;
        }

        public void Return(Database.Command command)
        {
            if (command is Command)
            {
                _commands.Push(command as Command);
            }
        }
    }
}
