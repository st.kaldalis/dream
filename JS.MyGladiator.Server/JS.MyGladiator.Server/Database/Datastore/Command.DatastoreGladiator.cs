﻿using System.Collections.Generic;
using System.Linq;
using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreGladiator : DatastoreTable, IGladiator
        {
            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("Gladiator");
            }

            public long Create(Gladiator gladiator)
            {
                var gladiatorE = Models.Gladiator.ToEntity(gladiator);
                gladiatorE.Key = _db.AllocateId(_keyFactory.CreateIncompleteKey());

                Insert(gladiatorE);

                return gladiatorE.Key.Path.First().Id;
            }

            public List<Gladiator> FindAll(long accountKey)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM Gladiator WHERE AccountKey = @AccountKey",
                    NamedBindings = { { "AccountKey", accountKey } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                List<Gladiator> gladiators = new List<Gladiator>();
                foreach (Entity entity in results.Entities)
                {
                    gladiators.Add(Models.Gladiator.FromEntity(entity));
                }

                return gladiators;
            }

            public Gladiator Find(long accountKey, long unitKey)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM Gladiator WHERE AccountKey = @AccountKey AND UnitKey = @UnitKey",
                    NamedBindings = { { "AccountKey", accountKey }, { "UnitKey", unitKey } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                foreach (Entity entity in results.Entities)
                {
                    return Models.Gladiator.FromEntity(entity);
                }

                return null;
            }
        }
    }
}
