﻿using System.Collections.Generic;
using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreUnitAttachment : DatastoreTable, IUnitAttachment
        {
            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("UnitAttachment");
            }

            public List<UnitAttachment> FindAll(long unitKey)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM UnitAttachment WHERE UnitKey = @UnitKey",
                    NamedBindings = { { "UnitKey", unitKey } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                List<UnitAttachment> unitAttachments = new List<UnitAttachment>();
                foreach (Entity entity in results.Entities)
                {
                    unitAttachments.Add(Models.UnitAttachment.FromEntity(entity));
                }

                return unitAttachments.Count > 0 ? unitAttachments : null;
            }

            public void Create(UnitAttachment unitAttachment)
            {
                var e = Models.UnitAttachment.ToEntity(unitAttachment);
                e.Key = _keyFactory.CreateIncompleteKey();

                Insert(e);
            }
        }
    }
}
