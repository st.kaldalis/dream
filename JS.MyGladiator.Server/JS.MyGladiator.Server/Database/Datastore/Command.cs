﻿using Google.Cloud.Datastore.V1;
using System.Collections.Generic;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        private DatastoreDb _db;

        private DatastoreTransaction _transaction;

        private List<DatastoreTable> _tables;

        public Command()
        {
            var account = new DatastoreAccount();
            var writePermissionCode = new DatastoreWritePermissionCode();
            var gladiatorName = new DatastoreGladiatorName();
            var gladiator = new DatastoreGladiator();
            var mainGladiator = new DatastoreMainGladiator();
            var newGladiator = new DatastoreNewGladiator();
            var session = new DatastoreSession();
            var unit = new DatastoreUnit();
            var unitAttachment = new DatastoreUnitAttachment();
            var unitStatus = new DatastoreUnitStatus();

            _tables = new List<DatastoreTable>
            {
                account,
                writePermissionCode,
                gladiatorName,
                gladiator,
                mainGladiator,
                newGladiator,
                session,
                unit,
                unitAttachment,
                unitStatus
            };

            Account = account;
            WritePermissionCode = writePermissionCode;
            GladiatorName = gladiatorName;
            Gladiator = gladiator;
            MainGladiator = mainGladiator;
            NewGladiator = newGladiator;
            Session = session;
            Unit = unit;
            UnitAttachment = unitAttachment;
            UnitStatus = unitStatus;
        }

        public void Init()
        {
            _db = null;

            _tables.ForEach((table) => { table.Init(); });
        }

        public void Init(DatastoreDb db)
        {
            _db = db;

            _tables.ForEach((table) => { table.Init(); table.Init(db); });
        }

        public override void BeginTransaction()
        {
            _transaction = _db.BeginTransaction();

            _tables.ForEach((table) => { table.SetTransaction(_transaction); });
        }

        public override void CommitTransaction()
        {
            try
            {
                _transaction.Commit();
            }
            finally
            {
                _tables.ForEach((table) => { table.UnsetTransaction(); });
            }
        }

        public override void RollbackTransaction()
        {
            try
            {
                _transaction.Rollback();
            }
            finally
            {
                _tables.ForEach((table) => { table.UnsetTransaction(); });
            }
        }
    }
}
