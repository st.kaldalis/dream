﻿using System.Collections.Generic;
using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreNewGladiator : DatastoreTable, INewGladiator
        {
            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("NewGladiator");
            }

            public List<NewGladiator> FindAll(long accountKey)
            {
                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM NewGladiator WHERE AccountKey = @AccountKey",
                    NamedBindings = { { "AccountKey", accountKey } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                List<NewGladiator> newGladiators = new List<NewGladiator>();
                foreach (Entity entity in results.Entities)
                {
                    newGladiators.Add(Models.NewGladiator.FromEntity(entity));
                }

                return newGladiators;
            }

            public void Create(NewGladiator newGladiator)
            {
                var newGladiatorE = Models.NewGladiator.ToEntity(newGladiator);
                newGladiatorE.Key = _keyFactory.CreateIncompleteKey();

                Insert(newGladiatorE);
            }

            public void Delete(long key)
            {
                Delete(new Key().WithElement("NewGladiator", key));
            }
        }
    }
}
