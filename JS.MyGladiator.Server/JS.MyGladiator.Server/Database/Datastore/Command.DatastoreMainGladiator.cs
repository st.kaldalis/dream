﻿using Google.Cloud.Datastore.V1;
using JS.MyGladiator.Server.Models;
using System.Linq;

namespace JS.MyGladiator.Server.Database.Datastore
{
    public partial class Command : Database.Command
    {
        protected class DatastoreMainGladiator : DatastoreTable, IMainGladiator
        {
            public override void Init(DatastoreDb db)
            {
                base.Init(db);

                _keyFactory = _db.CreateKeyFactory("MainGladiator");
            }

            public MainGladiator Find(long accountKey)
            {
                MainGladiator mainGladiator = null;

                GqlQuery gqlQuery = new GqlQuery
                {
                    QueryString = "SELECT * FROM MainGladiator WHERE AccountKey = @AccountKey",
                    NamedBindings = { { "AccountKey", accountKey } }
                };

                DatastoreQueryResults results = RunQuery(gqlQuery);

                foreach (Entity entity in results.Entities)
                {
                    mainGladiator = Models.MainGladiator.FromEntity(entity);
                    break;
                }

                return mainGladiator;
            }

            public long Create(MainGladiator mainGladiator)
            {
                var e = Models.MainGladiator.ToEntity(mainGladiator);
                e.Key = _keyFactory.CreateIncompleteKey();

                Insert(e);

                return e.Key.Path.First().Id;
            }

            public void Update(MainGladiator mainGladiator)
            {
                Update(Models.MainGladiator.ToEntity(mainGladiator));
            }
        }
    }
}
