﻿namespace JS.MyGladiator.Server.Database
{
    public interface ICommandPool
    {
        Command Get();

        void Return(Command command);
    }
}
