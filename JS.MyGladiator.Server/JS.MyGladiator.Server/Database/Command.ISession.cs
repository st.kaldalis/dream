﻿using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface ISession
        {
            void Create(Session session);

            bool Update(Session session);

            Session Find(long accountKey);

            Session Find(string id);
        }
    }
}
