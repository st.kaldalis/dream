﻿using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface IUnit
        {
            Unit Find(long unitKey);

            long Create(Unit unit);

            void Update(Unit unit);
        }
    }
}
