﻿using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface IWritePermissionCode
        {
            void Create(WritePermissionCode writeKey);

            bool Update(WritePermissionCode writeKey);

            WritePermissionCode Find(long accountKey);
        }
    }
}
