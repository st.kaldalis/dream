﻿using JS.Common;
using System;

namespace JS.MyGladiator.Server.Database
{
    public class TransactionedOperation : TransactionedOperationBase
    {
        protected Action<TransactionedOperationArgs> _action;

        public TransactionedOperation(ILogger logger) : base(logger)
        {
            _action = (args) => { };
        }

        public TransactionedOperation(ILogger logger, Action<TransactionedOperationArgs> action) : base(logger)
        {
            _action = action;
        }

        protected override void Operation(TransactionedOperationArgs args)
        {
            _action(args);
        }
    }
}
