﻿using System.Collections.Generic;
using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface IGladiator
        {
            long Create(Gladiator gladiator);

            List<Gladiator> FindAll(long accountKey);
            Gladiator Find(long accountKey, long unitKey);
        }
    }
}
