﻿using JS.MyGladiator.Server.Models;

namespace JS.MyGladiator.Server.Database
{
    public abstract partial class Command
    {
        public interface IUnitStatus
        {
            UnitStatus Find(long unitKey);

            void Create(UnitStatus unitStatus);
        }
    }
}
