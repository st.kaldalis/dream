﻿using JS.Common;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace JS.MyGladiator.Server
{
    public class ConsoleLogger : ILogger
    {
        public void Fatal(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Debug.WriteLine($"[FATAL] {msg}\nfilePath : {filePath}\nlineNumber : {lineNumber}");
        }

        public void Info(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Debug.WriteLine($"[INFO] {msg}\nfilePath : {filePath}\nlineNumber : {lineNumber}");
        }

        public void Warn(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Debug.WriteLine($"[WARN] {msg}\nfilePath : {filePath}\nlineNumber : {lineNumber}");
        }
    }
}
