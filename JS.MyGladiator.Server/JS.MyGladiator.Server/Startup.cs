﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.Datastore.V1;
using Grpc.Auth;
using Grpc.Core;
using JS.Common;
using JS.Lair.Common;
using JS.MyGladiator.Common.GameData;
using JS.MyGladiator.Server.Database;
using JS.MyGladiator.Server.Database.Datastore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace JS.MyGladiator.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            var logger = CreateLogger();
            services.AddSingleton(logger);

            DatastoreDb db = CreateDatastoreDB();
            services.AddSingleton<ICommandPool>(new CommnadPool(db));
            services.AddSingleton(new LairApi("http://localhost:61120", new Serializer()));
            services.AddSingleton<ISerializer>(new Serializer());
            services.AddSingleton(new Time());

            var gameDataService = new GameDataService();
            gameDataService.Load("/Users/stkal/Documents/git/dream/Document/GameData/dst/GameData");

            services.AddSingleton<IGameDataService>(new GameDataService());
        }

        private static ILogger CreateLogger()
        {
            /* TODO : 동작환경에 따라 적절한 로거를 지정해줘야 함 */
            return new ConsoleLogger();
        }

        private static DatastoreDb CreateDatastoreDB()
        {
            GoogleCredential cred = GoogleCredential.FromFile("/Users/stkal/Documents/keys/MyGladiator-bf8f820c7cab.json");
            if (cred.IsCreateScopedRequired)
            {
                cred = cred.CreateScoped(DatastoreClient.DefaultScopes);
            }

            var channel = new Channel(DatastoreClient.DefaultEndpoint.Host, DatastoreClient.DefaultEndpoint.Port, cred.ToChannelCredentials());
            DatastoreClient client = DatastoreClient.Create(channel);

            var db = DatastoreDb.Create("mygladiator", "", client);

            return db;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
