﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.MyGladiator.Server.Models
{
    public class NewGladiator
    {
        public long Key { get; set; }

        public long AccountKey { get; set; }

        public long UnitKey { get; set; }

        public static Entity ToEntity(NewGladiator unit) => new Entity()
        {
            Key = new Key().WithElement("Unit", unit.Key),
            ["AccountKey"] = unit.AccountKey,
            ["UnitKey"] = unit.UnitKey
        };

        public static NewGladiator FromEntity(Entity entity) => new NewGladiator()
        {
            Key = entity.Key.Path.First().Id,
            AccountKey = (long)entity["AccountKey"],
            UnitKey = (long)entity["UnitKey"]
        };
    }
}
