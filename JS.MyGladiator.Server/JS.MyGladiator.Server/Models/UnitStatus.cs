﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.MyGladiator.Server.Models
{
    public class UnitStatus
    {
        public long Key { get; set; }

        public long UnitKey { get; set; }

        public int Strength { get; set; }

        public int Dexterity { get; set; }

        public int Vitality { get; set; }

        public int Luck { get; set; }

        public static Entity ToEntity(UnitStatus status) => new Entity()
        {
            Key = new Key().WithElement("Status", status.Key),
            ["UnitKey"] = status.UnitKey,
            ["Strength"] = status.Strength,
            ["Dexterity"] = status.Dexterity,
            ["Vitality"] = status.Vitality,
            ["Luck"] = status.Luck
        };

        public static UnitStatus FromEntity(Entity entity) => new UnitStatus()
        {
            Key = entity.Key.Path.First().Id,
            UnitKey = (long)entity["UnitKey"],
            Strength = (int)entity["Strength"],
            Dexterity = (int)entity["Dexterity"],
            Vitality = (int)entity["Vitality"],
            Luck = (int)entity["Luck"]
        };
    }
}
