﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.MyGladiator.Server.Models
{
    public class UnitAttachment
    {
        public long Key { get; set; }

        public long UnitKey { get; set; }

        public string Type { get; set; }

        public string ID { get; set; }

        public static Entity ToEntity(UnitAttachment attachment) => new Entity()
        {
            Key = new Key().WithElement("Attachment", attachment.Key),
            ["UnitKey"] = attachment.UnitKey,
            ["Type"] = attachment.Type,
            ["ID"] = attachment.ID
        };

        public static UnitAttachment FromEntity(Entity entity) => new UnitAttachment()
        {
            Key = entity.Key.Path.First().Id,
            UnitKey = (long)entity["UnitKey"],
            Type = (string)entity["Type"],
            ID = (string)entity["ID"]
        };
    }
}
