﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.MyGladiator.Server.Models
{
    public class MainGladiator
    {
        public long Key { get; set; }

        public long AccountKey { get; set; }

        public long UnitKey { get; set; }

        public static Entity ToEntity(MainGladiator mainGladiator) => new Entity()
        {
            Key = new Key().WithElement("MainGladiator", mainGladiator.Key),
            ["AccountKey"] = mainGladiator.AccountKey,
            ["UnitKey"] = mainGladiator.UnitKey
        };

        public static MainGladiator FromEntity(Entity entity) => new MainGladiator()
        {
            Key = entity.Key.Path.First().Id,
            AccountKey = (long)entity["AccountKey"],
            UnitKey = (long)entity["UnitKey"]
        };
    }
}
