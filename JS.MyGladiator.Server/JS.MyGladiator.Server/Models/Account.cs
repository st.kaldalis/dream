﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.MyGladiator.Server.Models
{
    public class Account
    {
        public long Key { get; set; }

        public long LairAccountKey { get; set; }

        public static Entity ToEntity(Account account) => new Entity()
        {
            Key = new Key().WithElement("Account", account.Key),
            ["LairAccountKey"] = account.LairAccountKey
        };

        public static Account FromEntity(Entity entity) => new Account()
        {
            Key = entity.Key.Path.First().Id,
            LairAccountKey = (long)entity["LairAccountKey"]
        };
    }
}
