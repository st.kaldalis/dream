﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.MyGladiator.Server.Models
{
    public class WritePermissionCode
    {
        public long Key { get; set; }

        public long AccountKey { get; set; }

        public string Code { get; set; }

        public static Entity ToEntity(WritePermissionCode wpc) => new Entity()
        {
            Key = new Key().WithElement("WritePermissionCode", wpc.Key),
            ["AccountKey"] = wpc.AccountKey,
            ["Code"] = wpc.Code
        };

        public static WritePermissionCode FromEntity(Entity entity) => new WritePermissionCode()
        {
            Key = entity.Key.Path.First().Id,
            AccountKey = (long)entity["AccountKey"],
            Code = (string)entity["Code"]
        };
    }
}
