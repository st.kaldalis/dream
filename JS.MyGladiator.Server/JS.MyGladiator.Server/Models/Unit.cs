﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.MyGladiator.Server.Models
{
    public class Unit
    {
        public long Key { get; set; }

        public string ID { get; set; }

        public string Name { get; set; }

        public int Level { get; set; }

        public static Entity ToEntity(Unit unit) => new Entity()
        {
            Key = new Key().WithElement("Unit", unit.Key),
            ["ID"] = unit.ID,
            ["Name"] = unit.Name,
            ["Level"] = unit.Level
        };

        public static Unit FromEntity(Entity entity) => new Unit()
        {
            Key = entity.Key.Path.First().Id,
            ID = (string)entity["ID"],
            Name = (string)entity["Name"],
            Level = (int)entity["Level"]
        };
    }
}
