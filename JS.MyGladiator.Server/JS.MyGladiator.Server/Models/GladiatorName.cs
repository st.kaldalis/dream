﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.MyGladiator.Server.Models
{
    public class GladiatorName
    {
        public long Key { get; set; }

        public string Name { get; set; }

        public static Entity ToEntity(GladiatorName gladiatorName) => new Entity()
        {
            Key = new Key().WithElement("Account", gladiatorName.Key),
            ["Name"] = gladiatorName.Name
        };

        public static GladiatorName FromEntity(Entity entity) => new GladiatorName()
        {
            Key = entity.Key.Path.First().Id,
            Name = (string)entity["Name"]
        };
    }
}
