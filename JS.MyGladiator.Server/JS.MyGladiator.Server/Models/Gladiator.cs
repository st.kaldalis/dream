﻿using Google.Cloud.Datastore.V1;
using System.Linq;

namespace JS.MyGladiator.Server.Models
{
    public class Gladiator
    {
        public long Key { get; set; }

        public long AccountKey { get; set; }

        public long UnitKey { get; set; }

        public static Entity ToEntity(Gladiator unit) => new Entity()
        {
            Key = new Key().WithElement("Unit", unit.Key),
            ["AccountKey"] = unit.AccountKey,
            ["UnitKey"] = unit.UnitKey
        };

        public static Gladiator FromEntity(Entity entity) => new Gladiator()
        {
            Key = entity.Key.Path.First().Id,
            AccountKey = (long)entity["AccountKey"],
            UnitKey = (long)entity["UnitKey"]
        };
    }
}
