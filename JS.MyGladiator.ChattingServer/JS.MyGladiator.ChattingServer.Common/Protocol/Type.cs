﻿namespace JS.MyGladiator.ChattingServer.Common.Protocol
{
    public enum Type : int
    {
        REQUEST_CONNECTION,
        ALLOW_CONNECTION,
        REJECT_CONNECTION,

        SEND_MESSAGE
    }
}
