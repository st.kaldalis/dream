﻿using JS.Common;
using System;
using System.Runtime.CompilerServices;

namespace JS.MyGladiator.ChattingServer
{
    class Logger : ILogger
    {
        public void Fatal(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Console.WriteLine($"[Fatal]\nmsg : {msg}\nfilePath : {filePath}\nlineNumber : {lineNumber}");
        }

        public void Info(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Console.WriteLine($"[Info]\nmsg : {msg}\nfilePath : {filePath}\nlineNumber : {lineNumber}");
        }

        public void Warn(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Console.WriteLine($"[Warn]\nmsg : {msg}\nfilePath : {filePath}\nlineNumber : {lineNumber}");
        }
    }
}
