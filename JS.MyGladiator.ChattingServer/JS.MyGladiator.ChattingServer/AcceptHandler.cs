﻿using JS.MyGladiator.Net;
using System;
using System.Net.Sockets;

namespace JS.MyGladiator.ChattingServer
{
    public class AcceptHandler : IAcceptHandler
    {
        private Context _context;

        public AcceptHandler(Context context)
        {
            _context = context;
        }

        public void OnAccept(Socket socket)
        {
            var session = new Session(_context.Logger, _context.PacketService, _context.SaePool, _context.TokenPool,
                socket, new ReceiveHandler(_context), new DisconnectHandler(_context));

            if (_context.WatingSessionList.TryAdd(session.Guid, session))
            {
                session.BeginReceive();
            }
            else
            {
                _context.Logger.Info("Failed to add session to waiting list. Same guid already exists in waiting list.");
                session.Close();
            }
        }
    }
}
