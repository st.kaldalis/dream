﻿using FlatBuffers;
using JS.MyGladiator.ChattingServer.Common.Protocol;
using JS.MyGladiator.Net;
using System;

namespace JS.MyGladiator.ChattingServer
{
    public class ReceiveHandler : IReceiveHandler
    {
        private Context _context;

        public ReceiveHandler(Context context)
        {
            _context = context;
        }

        public void OnReceive(Net.Session session, byte[] data)
        {
            var protocolType = (Common.Protocol.Type)BitConverter.ToInt32(data, 0);
            var byteBuffer = new ByteBuffer(data, 4);
            switch (protocolType)
            {
                case Common.Protocol.Type.REQUEST_CONNECTION:
                    OnReceiveRequestConnection(session.Guid, RequestConnection.GetRootAsRequestConnection(byteBuffer));
                    break;
                case Common.Protocol.Type.SEND_MESSAGE:
                    OnReceiveRequestSend(session.Guid, RequestSendMessage.GetRootAsRequestSendMessage(byteBuffer));
                    break;
                default:
                    break;
            }
        }

        private void OnReceiveRequestConnection(Guid sessionGuid, RequestConnection protocol)
        {
            var sessionID = protocol.SessionID;

            _context.ServerApi.GetMainGladiator(new Server.Common.Protocol.Request.GetMainGladiator() { SessionID = sessionID }).ContinueWith((ret) =>
            {
                if (ret.IsCompleted && !ret.IsFaulted)
                {
                    if (_context.WatingSessionList.TryRemove(sessionGuid, out var session) && _context.SessionList.TryAdd(session.Guid, session))
                    {
                        session.MainGladiatorName = ret.Result.Gladiator.Name;

                        var builder = new FlatBufferBuilder(1);

                        session.Send(Common.Protocol.Type.ALLOW_CONNECTION, builder.DataBuffer.ToSizedArray());
                    }
                }
                else
                {
                    if (_context.WatingSessionList.TryRemove(sessionGuid, out var session))
                    {
                        var builder = new FlatBufferBuilder(1);

                        session.Send(Common.Protocol.Type.REJECT_CONNECTION, builder.DataBuffer.ToSizedArray(), () => { session.Close(); });
                    }
                }
            });
        }

        private void OnReceiveRequestSend(Guid sessionGuid, RequestSendMessage protocol)
        {
            if (!_context.SessionList.TryGetValue(sessionGuid, out var session)) { return; }

            var builder = new FlatBufferBuilder(1);

            var senderName = builder.CreateString(session.MainGladiatorName);
            var body = builder.CreateString(protocol.Body);

            RequestSendMessage.StartRequestSendMessage(builder);
            RequestSendMessage.AddSenderName(builder, senderName);
            RequestSendMessage.AddBody(builder, body);

            var requestSendMessage = RequestSendMessage.EndRequestSendMessage(builder);

            builder.Finish(requestSendMessage.Value);

            foreach (var item in _context.SessionList)
            {
                item.Value.Send(Common.Protocol.Type.SEND_MESSAGE, builder.DataBuffer.ToSizedArray());
            }
        }
    }
}
