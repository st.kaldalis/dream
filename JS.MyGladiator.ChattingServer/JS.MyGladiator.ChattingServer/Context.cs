﻿using JS.Common;
using JS.MyGladiator.Net;
using System;
using System.Collections.Concurrent;
using System.Net.Sockets;

namespace JS.MyGladiator.ChattingServer
{
    public class Context
    {
        public ILogger Logger;

        public IPacketService PacketService;

        public IPool<SocketAsyncEventArgs> SaePool;

        public IPool<Token> TokenPool;

        public ConcurrentDictionary<Guid, Session> WatingSessionList;

        public ConcurrentDictionary<Guid, Session> SessionList;

        public Server.Common.ServerApi ServerApi;
    }
}
