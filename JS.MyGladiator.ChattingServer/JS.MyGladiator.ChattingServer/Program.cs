﻿using JS.MyGladiator.Net;
using JS.MyGladiator.Server.Common;
using System;
using System.Collections.Concurrent;
using System.Net;
using System.Net.Sockets;

namespace JS.MyGladiator.ChattingServer
{
    class Program
    {
        private static Context context;

        static void Main(string[] args)
        {
            var serverApi = new ServerApi("http://localhost:61438", new JS.Common.Serializer());

            context = new Context
            {
                PacketService = new PacketService(),

                SaePool = new SaePool(),
                TokenPool = new Pool<Token>(new TokenFactory()),

                Logger = new Logger(),

                WatingSessionList = new ConcurrentDictionary<Guid, Session>(),

                SessionList = new ConcurrentDictionary<Guid, Session>(),

                ServerApi = serverApi
            };

            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);

            Socket listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(localEndPoint);
            listener.Listen(100);

            var session = new Session(context.Logger, context.PacketService, context.SaePool, context.TokenPool,
                listener, new AcceptHandler(context));

            session.BeginAccept();

            Console.WriteLine("Press 'q' to terminate.");
            while (true)
            {
                var info = Console.ReadKey();
                if (info.Key == ConsoleKey.Q)
                {
                    Console.WriteLine("Bye.");
                    break;
                }
            }
        }
    }
}
