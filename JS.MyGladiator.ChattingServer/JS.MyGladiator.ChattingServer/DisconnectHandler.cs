﻿using JS.MyGladiator.Net;
using System;

namespace JS.MyGladiator.ChattingServer
{
    class DisconnectHandler : IDisconnectHandler
    {
        private Context _context;

        public DisconnectHandler(Context context)
        {
            _context = context;
        }

        public void OnDisconnect(Net.Session session)
        {
            session.Close();

            try
            {
                _context.SessionList.TryRemove(session.Guid, out _);
            }
            catch (Exception e)
            {
                _context.Logger.Info(e.ToString());
            }
        }
    }
}
