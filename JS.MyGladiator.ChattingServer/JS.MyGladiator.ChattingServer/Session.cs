﻿using System;
using System.Net.Sockets;
using JS.Common;
using JS.MyGladiator.Net;

namespace JS.MyGladiator.ChattingServer
{
    public class Session : Net.Session
    {
        public Session(ILogger logger, IPacketService packetService, IPool<SocketAsyncEventArgs> saePool, IPool<Token> tokenPool, Socket socket, IReceiveHandler receiveHandler, IDisconnectHandler disconnectHandler) : base(logger, packetService, saePool, tokenPool, socket, receiveHandler, disconnectHandler)
        {
        }

        public Session(ILogger logger, IPacketService packetService, IPool<SocketAsyncEventArgs> saePool, IPool<Token> tokenPool, Socket socket, IAcceptHandler acceptHandler) : base(logger, packetService, saePool, tokenPool, socket, acceptHandler)
        {
        }

        public string MainGladiatorName;

        public void Send(Common.Protocol.Type protocolType, byte[] protocol)
        {
            Send(CreateDataStream(protocolType, protocol));
        }

        public void Send(Common.Protocol.Type protocolType, byte[] protocol, Action callback)
        {
            Send(CreateDataStream(protocolType, protocol), callback);
        }
        private byte[] CreateDataStream(Common.Protocol.Type protocolType, byte[] protocol)
        {
            byte[] dataStream = new byte[sizeof(int) + protocol.Length];
            Buffer.BlockCopy(BitConverter.GetBytes((int)protocolType), 0, dataStream, 0, sizeof(int));
            Buffer.BlockCopy(protocol, 0, dataStream, sizeof(int), protocol.Length);
            return dataStream;
        }
    }
}
