﻿using System.Runtime.CompilerServices;

namespace DataManager
{
    public interface ILogger
    {
        void Info(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0);

        void Warn(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0);

        void Fatal(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0);
    }
}
