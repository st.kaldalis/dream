﻿using System;
using System.Runtime.CompilerServices;

namespace DataManager
{
    public class Logger : ILogger
    {
        public void Fatal(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Console.WriteLine($"[FATAL] {msg}\nfilePath : {filePath} lineNumber : {lineNumber}");
        }

        public void Info(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Console.WriteLine($"[INFO] {msg}\nfilePath : {filePath} lineNumber : {lineNumber}");
        }

        public void Warn(string msg, [CallerFilePath] string filePath = null, [CallerLineNumber] int lineNumber = 0)
        {
            Console.WriteLine($"[WARN] {msg}\nfilePath : {filePath} lineNumber : {lineNumber}");
        }
    }
}
