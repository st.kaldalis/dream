﻿using System;
using System.Collections.Generic;
using OfficeOpenXml;
using System.IO;
using System.Text.RegularExpressions;

namespace DataManager
{
    public class Row : Dictionary<string, object>
    {
    }

    public class Table : List<Row>
    {
    }

    public class TableMap : SortedDictionary<string, Table>
    {
    }

    public class MetaRow
    {
        public string TableName;

        public string DataType;

        public string Key;

        public object DefaultValue;

        public bool IsUnique;

        public string RefTable;

        public string RefKey;

        public bool Nullable;

        public MetaRow()
        {
            DefaultValue = null;
            IsUnique = false;
            Nullable = false;
        }
    }

    public class MetaTable : Dictionary<string, MetaRow>
    {
    }

    public class MetaTableMap : Dictionary<string, MetaTable>
    {
    }

    public class TablePosition
    {
        public int Row;

        public int Col;

        public TablePosition(int row, int col)
        {
            Row = row;
            Col = col;
        }
    }

    public class Parser
    {
        private ILogger _logger;

        private readonly int _keyRowOffset;

        private readonly int _dataRowOffset;

        private readonly string _startOfTableFlag;

        private readonly string _endOfTableFlag;

        public Parser(ILogger logger)
        {
            _logger = logger;
            _keyRowOffset = 1;
            _dataRowOffset = 2;
            _startOfTableFlag = "{SOT}";
            _endOfTableFlag = "{EOT}";
        }

        public TableMap MakePrimeTableMap(string path, IEnumerable<string> fileNames)
        {
            var primeTableMap = new TableMap();

            foreach (var fileName in fileNames)
            {
                ExcelPackage package = null;
                try
                {
                    package = new ExcelPackage(new FileInfo($"{path}/{fileName}"));
                }
                catch (Exception e)
                {
                    _logger.Warn($"{path}/{fileName}\n{e.Message}");
                    continue;
                }

                foreach (var sheet in package.Workbook.Worksheets)
                {
                    if (sheet.Dimension == null)
                    {
                        continue;
                    }

                    var startCell = sheet.Dimension.Start;
                    var endCell = sheet.Dimension.End;

                    for (int nrow = startCell.Row; nrow <= endCell.Row; nrow++)
                    {
                        for (int ncol = startCell.Column; ncol <= endCell.Column; ncol++)
                        {
                            if (!IsStartOfTable(sheet, nrow, ncol))
                            {
                                continue;
                            }

                            var nTableNameRow = nrow + 1;
                            if (IsCellEmpty(sheet, nTableNameRow, ncol))
                            {
                                continue;
                            }

                            var tableName = Convert.ToString(sheet.Cells[nTableNameRow, ncol].Value);
                            var table = GetTable(sheet, new TablePosition(nTableNameRow, ncol));

                            if (primeTableMap.ContainsKey(tableName))
                                primeTableMap[tableName].AddRange(table);
                            else
                                primeTableMap[tableName] = table;
                        }
                    }
                }
            }

            return primeTableMap;
        }

        private MetaRow MakeMetaRow(Row row)
        {
            var metaRow = new MetaRow();

            string[] primeKeys = { "tableName", "dataType", "key" };
            foreach (var primeKey in primeKeys)
            {
                if (row.ContainsKey(primeKey))
                    continue;
                _logger.Warn($"{primeKey} cannot be None.");
                return null;
            }

            metaRow.TableName = Convert.ToString(row["tableName"]);

            metaRow.DataType = Convert.ToString(row["dataType"]);
            string[] validDataTypes = { "int", "float", "string", "bool", "time" };
            bool bValidType = false;
            foreach (var vdt in validDataTypes)
            {
                if (metaRow.DataType == vdt)
                {
                    bValidType = true;
                    break;
                }
            }
            if (!bValidType)
            {
                _logger.Warn($"tableName : {metaRow.TableName} / key : {metaRow.Key} / dataType : {metaRow.DataType} - invalid dataType");
                return null;
            }

            metaRow.Key = Convert.ToString(row["key"]);

            metaRow.IsUnique = row.ContainsKey($"isUnique") ? Convert.ToBoolean(row["isUnique"]) : false;
            metaRow.DefaultValue = row.ContainsKey($"defaultValue") ? row["defaultValue"] : null;
            metaRow.Nullable = row.ContainsKey($"isNullable") ? Convert.ToBoolean(row["isNullable"]) : false;

            if (row.ContainsKey($"refTable"))
            {
                if (row.ContainsKey($"refKey"))
                {
                    metaRow.RefTable = Convert.ToString(row["refTable"]);
                    if (string.IsNullOrEmpty(metaRow.RefTable))
                    {
                        metaRow.RefTable = null;
                        metaRow.RefKey = null;
                    }
                    else
                    {
                        metaRow.RefKey = Convert.ToString(row["refKey"]);
                        metaRow.RefKey = string.IsNullOrEmpty(metaRow.RefKey) ? null : metaRow.RefKey;
                    }
                }
                else
                {
                    _logger.Warn($"tableName : {metaRow.TableName} / key : {metaRow.Key}  / refTable : {metaRow.RefTable} - refKey is None. refTable will be ignored.");
                }
            }

            return metaRow;
        }

        public MetaTableMap MakeMetaTableMap(TableMap primeTableMap, string metaTableName)
        {
            if (!primeTableMap.ContainsKey(metaTableName))
            {
                _logger.Warn($"metaTableName : {metaTableName} - does not exist in prime table map.");
                return null;
            }

            var metaTableMap = new MetaTableMap();

            foreach (var row in primeTableMap[metaTableName])
            {
                var metaRow = MakeMetaRow(row);
                if (metaRow == null)
                {
                    continue;
                }

                var tableName = metaRow.TableName;
                var key = metaRow.Key;

                if (!metaTableMap.ContainsKey(tableName))
                {
                    metaTableMap[tableName] = new MetaTable();
                }

                if (metaTableMap[tableName].ContainsKey(key))
                {
                    _logger.Warn($"tableName : {tableName} / key : {key} - duplicated key exist.");
                    continue;
                }

                metaTableMap[tableName][key] = metaRow;
            }

            foreach (var metaTable in metaTableMap.Values)
            {
                foreach (var metaRow in metaTable.Values)
                {
                    if (!string.IsNullOrEmpty(metaRow.RefTable) && !metaTableMap.ContainsKey(metaRow.RefTable))
                    {
                        _logger.Warn($"tableName : {metaRow.TableName} / key : {metaRow.Key} / refTable : {metaRow.RefTable} - refTable is not exist. Reference will be ignored.");
                        metaRow.RefTable = metaRow.RefKey = null;
                        continue;
                    }

                    if (!string.IsNullOrEmpty(metaRow.RefKey) && !metaTableMap[metaRow.RefTable].ContainsKey(metaRow.RefKey))
                    {
                        _logger.Warn($"tableName : {metaRow.TableName} / key : {metaRow.Key} / refTable : {metaRow.RefTable} / refKey : {metaRow.RefKey} - refKey is not exist. Reference will be ignored.");
                        metaRow.RefTable = metaRow.RefKey = null;
                        continue;
                    }
                }
            }

            return metaTableMap;
        }

        private bool IsCellEmpty(ExcelWorksheet sheet, int nrow, int ncol)
        {
            var startCell = sheet.Dimension.Start;
            var endCell = sheet.Dimension.End;

            if (nrow < startCell.Row || nrow > endCell.Row)
                return true;

            if (ncol < startCell.Column || ncol > endCell.Column)
                return true;

            return null == sheet.Cells[nrow, ncol].Value;
        }

        private bool IsTableNameCell(ExcelWorksheet sheet, int nrow, int ncol)
        {
            return !IsCellEmpty(sheet, nrow, ncol);
        }

        private List<string> GetKeys(ExcelWorksheet sheet, TablePosition tablePosition)
        {
            List<string> keys = new List<string>();

            var nrow = tablePosition.Row + _keyRowOffset;
            var ncol = tablePosition.Col;

            if (nrow > sheet.Dimension.End.Row)
                return keys;

            while (true)
            {
                if (IsCellEmpty(sheet, nrow, ncol))
                    break;

                keys.Add(Convert.ToString(sheet.Cells[nrow, ncol].Value));

                ncol += 1;
                if (ncol > sheet.Dimension.End.Column)
                    break;
            }

            return keys;
        }

        private Table GetTable(ExcelWorksheet sheet, TablePosition tablePosition)
        {
            var table = new Table();

            var keys = GetKeys(sheet, tablePosition);

            var endCell = sheet.Dimension.End;

            var nrow = tablePosition.Row + _dataRowOffset;

            while (true)
            {
                var row = new Row();
                for (var i = 0; i < keys.Count; i++)
                {
                    var key = keys[i];
                    var ncol = tablePosition.Col + i;

                    if (IsCellEmpty(sheet, nrow, ncol))
                        row[key] = null;
                    else
                        row[key] = sheet.Cells[nrow, ncol].Value;
                }

                bool isEmptyRow = true;

                foreach (var entry in row)
                {
                    if (entry.Value != null)
                    {
                        isEmptyRow = false;
                        break;
                    }
                }

                if (!isEmptyRow)
                {
                    table.Add(row);
                }

                nrow += 1;
                if (nrow > endCell.Row)
                    break;
                if (IsEndOfTable(sheet, nrow, tablePosition.Col))
                    break;
            }

            return table;
        }

        private bool MatchTableCellValue(ExcelWorksheet sheet, int nrow, int ncol, string target)
        {
            var startCell = sheet.Dimension.Start;
            var endCell = sheet.Dimension.End;

            if (nrow < startCell.Row || nrow > endCell.Row)
                return false;

            if (ncol < startCell.Column || ncol > endCell.Column)
                return false;

            var value = sheet.Cells[nrow, ncol].Value;
            if (value is string)
            {
                return target == Convert.ToString(value);
            }
            return false;
        }

        private bool IsStartOfTable(ExcelWorksheet sheet, int nrow, int ncol)
        {
            return MatchTableCellValue(sheet, nrow, ncol, _startOfTableFlag);
        }

        private bool IsEndOfTable(ExcelWorksheet sheet, int nrow, int ncol)
        {
            return MatchTableCellValue(sheet, nrow, ncol, _endOfTableFlag);
        }

        private object GetDataTypeVerifiedValue(string dataType, object value)
        {
            if (value == null)
            {
                return null;
            }

            if (dataType == "string")
            {
                if (value is string)
                {
                    return value;
                }
            }
            else if (dataType == "bool")
            {
                if (value is bool)
                {
                    return value;
                }
                else if (value is long)
                {
                    return Convert.ToInt64(value) != 0;
                }
                else if (value is int)
                {
                    return Convert.ToInt32(value) != 0;
                }
                else if (value is short)
                {
                    return Convert.ToInt16(value) != 0;
                }
                else if (value is float)
                {
                    return Convert.ToSingle(value) != 0.0f;
                }
                else if (value is double)
                {
                    return Convert.ToDouble(value) != 0.0f;
                }
            }
            else if (dataType == "float")
            {
                if (value is double)
                {
                    return Convert.ToDouble(value);
                }
            }
            else if (dataType == "int")
            {
                if (value is double)
                {
                    return Convert.ToInt32(value);
                }
            }
            else if (dataType == "time")
            {
                if (value is string)
                {
                    var time = Convert.ToString(value);
                    Regex r = new Regex(@"^[0-2]?[0-3]:[0-5]?[0-9]:[0-5]?[0-9](.[0-9]{1,3})?$", RegexOptions.IgnoreCase);
                    Match m = r.Match(time);
                    if (m.Success)
                    {
                        string[] tokens = time.Split(':');
                        float milliseconds = 0.0f;
                        milliseconds += float.Parse(tokens[2]);
                        milliseconds += int.Parse(tokens[1]) * 60;
                        milliseconds += int.Parse(tokens[0]) * 3600;
                        return milliseconds;
                    }
                    return 0.0f;
                }
            }

            return null;
        }

        public TableMap MakeTableMap(TableMap primeTableMap, MetaTableMap metaTableMap)
        {
            var tableMap = new TableMap();

            foreach (var entry in primeTableMap)
            {
                var tableName = entry.Key;
                var primeTable = entry.Value;

                if (!metaTableMap.ContainsKey(tableName))
                {
                    continue;
                }

                var table = new Table();
                var cachedValues = new Dictionary<string, HashSet<object>>();
                var metaTable = metaTableMap[tableName];
                foreach (var primeRow in primeTable)
                {
                    var row = new Row();
                    foreach (var metaRow in metaTable.Values)
                    {
                        var key = metaRow.Key;
                        var isUnique = metaRow.IsUnique;
                        var defaultValue = metaRow.DefaultValue;
                        var nullable = metaRow.Nullable;

                        if (primeRow.ContainsKey(key))
                        {
                            var value = primeRow[key];

                            if (!nullable && (value == null && defaultValue == null))
                            {
                                _logger.Warn($"tableName : {tableName} / key : {key} / dataType : {metaRow.DataType} - not nullable but value is null, and default value is also null.");
                            }

                            if (value == null && defaultValue != null)
                            {
                                value = defaultValue;
                            }

                            if (value != null)
                            {
                                var originalValue = value;
                                value = GetDataTypeVerifiedValue(metaRow.DataType, originalValue);
                                if (value == null)
                                {
                                    _logger.Warn($"tableName : {tableName} / key : {key} / dataType : {metaRow.DataType} / value : {value} - invlind datatype.");
                                }
                            }

                            if (value != null && isUnique)
                            {
                                if (cachedValues.ContainsKey(key) && cachedValues[key].Contains(value))
                                {
                                    _logger.Warn($"tableName : {tableName} / key : {key} / value : {value} - setted unique value but duplicated value exist.");
                                }
                                else
                                {
                                    if (!cachedValues.ContainsKey(key))
                                    {
                                        cachedValues[key] = new HashSet<object>();
                                    }

                                    cachedValues[key].Add(value);
                                }
                            }

                            row[key] = value;
                        }
                        else
                        {
                            row[key] = null;
                        }
                    }
                    table.Add(row);
                }
                tableMap[tableName] = table;
            }

            {
                var cachedValues = new Dictionary<string, Dictionary<string, Dictionary<object, bool>>>();

                var mtmKeys = new List<string>(metaTableMap.Keys);
                foreach (var tableName in mtmKeys)
                {
                    var metaTable = metaTableMap[tableName];

                    if (!tableMap.ContainsKey(tableName))
                    {
                        continue;
                    }

                    var table = tableMap[tableName];
                    for (var nrow = 0; nrow < table.Count; nrow++)
                    {
                        var row = table[nrow];
                        var rowKeys = new List<string>(row.Keys);
                        foreach (var key in rowKeys)
                        {
                            var value = row[key];
                            if (value == null && !metaTable[key].Nullable)
                            {
                                _logger.Warn($"tableName : {tableName} / key : {key} / value : {value} - not nullable but value is null.");
                                continue;
                            }

                            var refTableName = metaTable[key].RefTable;
                            var refKey = metaTable[key].RefKey;
                            if (refTableName == null || refKey == null)
                            {
                                continue;
                            }

                            if (!ValidateReference(tableMap, tableName, key, value, refTableName, refKey, ref cachedValues))
                            {
                                _logger.Warn($"tableName : {tableName} / key : {key} / value : {value} - reference error.");
                            }
                        }
                    }
                }
            }

            return tableMap;
        }

        public bool ValidateReference(
            TableMap tableMap,
            string tableName, string key, object value,
            string refTableName, string refKey,
            ref Dictionary<string, Dictionary<string, Dictionary<object, bool>>> cachedValues)
        {
            if (cachedValues.ContainsKey(refTableName)
                                && cachedValues[refTableName].ContainsKey(refKey)
                                && cachedValues[refTableName][refKey].ContainsKey(value))
            {
                if (!cachedValues[refTableName][refKey][value])
                {
                    return false;
                }
            }
            else
            {
                if (!cachedValues.ContainsKey(refTableName))
                    cachedValues[refTableName] = new Dictionary<string, Dictionary<object, bool>>();
                if (!cachedValues[refTableName].ContainsKey(refKey))
                    cachedValues[refTableName][refKey] = new Dictionary<object, bool>();

                if (!tableMap.ContainsKey(refTableName))
                {
                    return false;
                }

                var valueExist = false;
                var refTable = tableMap[refTableName];
                foreach (var refRow in refTable)
                {
                    if (Object.Equals(refRow[refKey], value))
                    {
                        valueExist = true;
                        cachedValues[refTableName][refKey][value] = true;
                        break;
                    }
                }

                if (valueExist != true)
                {
                    return false;
                }
            }

            return true;
        }  
    }
}
