﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DataManager
{
    class Program
    {
        static void Main(string[] args)
        {
            var metaTableName = "Meta";
            var srcPath = "../src/GameData/";
            var dstPath = "../dst/GameData/";
            var ext = "xlsx";

            List<string> fileNames = Directory.GetFiles(srcPath, "*." + ext).Select(Path.GetFileName).Where(name => !name.StartsWith("~$")).ToList();

            var logger = new Logger();
            var parser = new Parser(logger);
            var primeTableMap = parser.MakePrimeTableMap(srcPath, fileNames);
            var metaTableMap = parser.MakeMetaTableMap(primeTableMap, metaTableName);
            var tableMap = parser.MakeTableMap(primeTableMap, metaTableMap);

            foreach (var entry in tableMap)
            {
                var tableName = entry.Key;
                var rows = entry.Value;

                if (rows.Count == 0)
                {
                    continue;
                }

                var csv = "";
                var keys = rows[0].Keys;
                for (var i = 0; i < keys.Count; i++)
                {
                    if (i != 0)
                    {
                        csv += ",";
                    }

                    csv += keys.ElementAt(i);
                }
                csv += "\n";

                for (var i = 0; i < rows.Count; i++)
                {
                    for (var j = 0; j < keys.Count; j++)
                    {
                        if (j != 0)
                        {
                            csv += ",";
                        }

                        csv += rows[i][keys.ElementAt(j)];
                    }

                    csv += "\n";
                }

                var writer = new StreamWriter($"{dstPath}/{tableName}.csv", false);
                writer.Write(csv);
                writer.Close();
            }
        }
    }
}
